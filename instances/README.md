# Problem Instances

The experimentation in [1] is performed over a thermal-aware job mapping problem, defined over a multicore CPU by Intel. The CPU was called SCC (Single-chip Cloud Computer) and it was the research prototype that later evolved into the Xeon Phi.

The proble consists in mapping a number of jobs on the platform cores (there are 48 of them). Each job is a identified by its CPI (Clocks Per Instruction) value. The job mapping affects the efficiency of the platform cores, because the cores are equipped with thermal controllers that reduce the operating frequency in case the temperature gets too high. The goal is to keep the minimal core efficiency as high as possible. For a detailed description of our target problem, the reader is referred to [1].

This folder contains the raw data employed to define our two benchmarks, which have both been obtained by:

* Starting from a full mapping of a set of 288 jobs to the 48 platform cores
* Selecting a subset of the platform cores

Then, the goal is re-mapping the jobs on the selected cores. This leads in fact to a reduced-size version of the original problem, which (not by chance) could represent also the basic move in a Large Neighborhood Search approach.

All benchmarks have been obtained from the same initial job mapping, which is provided in the "initial_workload.txt" file:

* The file contains a single line, representing the job CPIs
* The first 6 jobs are mapped on core 0, the second 6 jobs on core 1, and so on and so forth.

The subset of cores considered in the first benchmark (46 in number) can be found in the "benchmark1.txt" file, while the subsets considered in the second benchmark (200 in number) are in the "benchmark2.txt" file.

Originally, we had one more benchmark (the one employed in [2]). This is not reported in this folder, but:

* The benchmark was obtained by starting from the same initial workload
* The considered cores can be deduced from the log file names in our results

## References

[1] Lombardi, Michele, and Stefano Gualandi. "A lagrangian propagator for artificial neural networks in constraint programming." Constraints (2015): 1-28.

[2] Stefano Gualandi, Federico Malucelli: "Resource Constrained Shortest Paths with a Super Additive Objective Function". CP 2012: 299-315