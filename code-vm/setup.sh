#!/usr/bin/env sh

# Fix the local issue
update-locale LC_ALL=en_US.UTF-8

# Install or-tools dependencies
# NOTE_ some packages may be necessary only at compilation time
sudo apt-get install -y bison flex python-setuptools python-dev autoconf \
  libtool zlib1g-dev texinfo help2man gawk g++ curl cmake subversion

# Install numpy (this is needed for the Thermal Aware Workload Dispatching solver)
sudo apt-get install -y python-numpy