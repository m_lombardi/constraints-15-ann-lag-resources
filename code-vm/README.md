# Lagrangian Propagator Code

This folder contains the code for the Lagrangian Neural Network propagator. Those code is shipped in a [vagrant](https://www.vagrantup.com) Virtual Machine, which leads to one huge benefit and a (perhaps annoying) drawback

* PRO: everything is pre-configured and working. You just need to install the VM (see the instruction below) and you can have the code running in minutes.
* CON: the VM is relatively large (~500MB)

In detail, in order to run the code you will need to:

* Install vagrant: just start from the [main web site](https://www.vagrantup.com) and follow the instructions
* Boot the virtual machine
* Move to the correct folder and run a script

This will be enough to repeat _some_ (not all) the experiments from [1]. More experiments can be re-executed by adjusting the script and/or recompiling the code (because several parameters are defined at compilation time).

If you are interested in making modifications to the code or adjusting the parameters, you will need to get a better understanding of the code structure. Some brief instructions are provided in this file, but for more detailed help you should consider contacting [the code author](mailto:michele.lombardi2@unibo.it)

## Installing the VM and running the code

_NOTE: by default, the VM will require 2 cores and 2048MB RAM. You can adjust these settings in the [`Vagrantfile`](Vagrantfile) that you can find in this folder_

Here I will assume that you have already installed vagrant on you system. Once you have done that, just open a terminal on the `code-vm` folder and run:

    vagrant up

The first time that you run this command, vagrant will need to download a Ubuntu box, which may take some time. It is advisable to perform this step when a decent network connection is available.

After that is done, the VM will be automatically [provisioned](https://docs.vagrantup.com/v2/provisioning/index.html) by running the [`setup.sh`](setup.sh) script that you can find on this repository. This will install some packages into the new VM, which again will take some time. Luckily, this step will be performed only at the first machine boot.

If everything goes well, at this point you will have a fully configured VM happily running on your system.

You can halt the machine at any time with:

    vagrant halt

You can reboot it with:

    vagrant up

which will not _not_ perform the provisioning again. Unless you _destroy_ the VM with:

    vagrant destroy

Destroying the VM is a good idea if you need to free up some disk space, of if you just made a mess of something and you prefer to re-start the configuration from scratch.

Most importantly, you can log in the VM via ssh using:

     vagrant ssh

Once you've logged in, just move to the `/vagrant/lag-cst-test` folder on the VM:

     cd /vagrant/lag-cst-test

And you can run some experiments with the Lagrangian propagator by executing a script:

     ./batch_solve_lag.sh

Keep in mind that this will solve 3x200 instances, each with a time limit of 300 seconds.

You can re-execute additional experiments by adjusting the `batch_solve_lag.sh` script. In particular, I have included a set of pre-compiled versions of the propagator that differ for the values of some configuration parameters. The pre-compiled versions are in the `pre-compiled-libs` folder: brief usage instructions can be found inside the `batch_solve_lag.sh` script.

If you want to make more advanced modifications, or you just want to try some other parameter configurations, you will need to re-compile the propagator.


## Understanding the code structure

The Lagrangian ANN propagator is implemented on top of [Google or-tools](https://developers.google.com/optimization/?hl=en), and then wrapped using SWIG to expose a python API.

As a consequence, the project code is divided into three main components:

* A (now old) or-tools installation, pre-compiled for Ubuntu trusty (in the `or-tools` folder)
* The code for the propagator itself, and the SWIG specification for the wrapper (in the `emllib` folder)
* Python code to solve a Thermal Aware Worklod Dispatching problem that is employed in the experimentation in [1]. This can be found in the `lag-cst-test` folder.

The or-tools installation should be fine as it is. Actually, with the aim to reduce the size of this repository I have removed part of the source files, so it may be tricky to recompile it at this stage.

The propagator code is pre-compiled and already working, but it may be necessary/interesting to perform some recompilation. The reason is that _several of the propagator parameters from [1] are specified at compile time_. So, if you want for example to adjust the activation budget, or the number of subgradient operations, you will need to modify the propagator source.

## Re-compiling the propagator

The propagator code is in the [`emllib/src/neuron_cst.cc`](emllib/src/neuron_cst.cc) file. The parameters are specified using macros. Just search the code for the string:

```c++
//
// PARAMETERS FOR THE LAGRANGIAN PROPAGATOR
//
```

and you'll find them. You can change the parameters as you wish: they should be kind of understandable, provided that you have read reference [1].

You may notice that the code contains much more than the Lagrangian propagator. In particular, you should find alternative implementations of individual Neuron Constraints [2] and the code for the Weighted Average Constraint from [3], which was originally designed to support embedding ANNs in CP.

Once you are done, you need to re-compile the code by running:

```
make clean
make all
```

You need to do this from the `emlib` folder via the shh terminal of the VM. The full path will be  `/vagrant/emllib`.

Re-building the project will place two important files in the `emllib/lib` folder, namely:

* `pywrapcp_eml.py`, i.e. the python code for the wrapper
* `_pywrapcp_eml.so`, i.e. the C code for the wrapper (and the propagator)

### The Python Application Code

Python code to solve the Thermal Aware Workload Dispatching problem can be found in the [`lag-cst-test/dispatch_cp.py`](lag-cst-test/dispatch_cp.py).

When/before you run the script, you will need to redefine the `PYTHONPATH` variable so that it points to a folder that contains both the wrapper files (i.e. `pywrapcp_eml.py` and `_pywrapcp_eml.so`). For a quick test you can use one of the pre-compiled versions, by running:

```
PYTHONPATH=pre-compiled-libs/i1000-s5-b18 python dispatch_cp.py
```

If everything goes well, this should print a help message.

By default, the python code solves the Thermal Aware Dispatching problem described in [1], using an approach similar to Large Neighborhood Search. The approach is described in more detail in a forthcoming paper [4], which discusses in detail several techniques to embed Machine Learnign models in CP, MINLP, and SMT.

The original experimentation from [1] was obtained by forcing the solver to explore completely a fixed neighborhood. This is done via the options:

* `--explore-neighborhood {core_set}`, which forces the solver to explore a single neighborhood
* `--local-tlim {time limit}`, which configures the time limit for each neighborhood exploration
* `--global-tlim {time limit}`, which configures the time limit for the whole LNS process (and should be equal to the local time limit, in case a fixed neighborhood is explored)
* `--global-iter 1`, which stops after a single neighborhood exploration
* `--local-nsol 0`, which sets no limit on the number of improving solutions for each neighborhood
* `--restart-base 0`, which disables restarts

The single neighborhood is explored using a custom, static, search strategy, activated via the option `--search-strategy ad_hoc`. The search strategy is defined in the the `emllib` code (in particular the `tawd_search.cc`) file.

This brief discussion, together with the content of the `batch_solve_lag.sh` script, should be enough to allow the definition of custom experiments.



## References

[1] Lombardi, Michele, and Stefano Gualandi. "A lagrangian propagator for artificial neural networks in constraint programming." Constraints (2015): 1-28.

[2] Bartolini, Andrea, et al. "Neuron constraints to model complex real-world problems." Principles and Practice of Constraint Programming–CP 2011. Springer Berlin Heidelberg, 2011. 115-129.

[3] Bonfietti, Alessio, and Michele Lombardi. "The weighted average constraint." Principles and Practice of Constraint Programming. Springer Berlin Heidelberg, 2012.

[4] Lombardi, Michele, and Bartolini, Andrea, and Milano, Michela. "Empirical Decision Model Learning" Accepted for publication (subject to minor revisions) at Artificial Intelligence