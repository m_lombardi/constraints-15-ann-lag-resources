#ifndef OR_TOOLS_TAWD_SEARCH_H_
#define OR_TOOLS_TAWD_SEARCH_H_

// Copyright 2010-2011 Google
// Neuron Constraint class
// a global constraints representing an artificial neuron
//
// created: 2011-10-31
// author: Michele Lombardi <michele.lombardi2@unibo.it>

//TODO remove some of these
//#include "base/commandlineflags.h"
//#include "base/integral_types.h"
//#include "base/logging.h"
//#include "base/stringprintf.h"
#include "constraint_solver/constraint_solveri.h"
//#include "base/scoped_ptr.h"
//#include "util/string_array.h"
#include "math.h"
#include <limits>
#include <vector>

#include <algorithm>

using namespace std;

namespace operations_research{
	
DecisionBuilder* MakeTAWDSearch(Solver* solver, 
								const vector<IntVar*>& C,
			   			   		const vector<int64>& cpis);
								
DecisionBuilder* MakeTAWDSearch2(Solver* solver, 
								const vector<IntVar*>& C,
			   			   		const vector<int64>& cpis);
							
DecisionBuilder* MakeTAWDResSearch(Solver* s,
								const vector<IntVar*>& C_res,
								const vector<IntVar*>& C_otr,
			   			   		const vector<int64>& cpi_res,
								const vector<int64>& cpi_otr);

} // end of namespace operations_research

#endif