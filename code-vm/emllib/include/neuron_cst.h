#ifndef OR_TOOLS_NEURON_CONSTRAINT_H_
#define OR_TOOLS_NEURON_CONSTRAINT_H_

// Copyright 2010-2011 Google
// Neuron Constraint class
// a global constraints representing an artificial neuron
//
// created: 2011-10-31
// author: Michele Lombardi <michele.lombardi2@unibo.it>

//TODO remove some of these
//#include "base/commandlineflags.h"
//#include "base/integral_types.h"
//#include "base/logging.h"
//#include "base/stringprintf.h"
#include "constraint_solver/constraint_solveri.h"
//#include "base/scoped_ptr.h"
//#include "util/string_array.h"
#include "math.h"
#include <limits>
#include <vector>

#include <algorithm>

using namespace std;

// TODO switch from using doubles for weight to something more SWIG compliant

//TODO add some discussion about the floating point approximation we use

//------------------------------------------------------------------------------
// Hardlim neuron
//------------------------------------------------------------------------------

namespace operations_research{
	
typedef enum {ntype_purelin = 0, ntype_tansig, ntype_hardlim} neuron_type;
	
/**
 * @brief Build a neuron constraint with "hardlim" activation function
 * @param s the solver
 * @param size the number of inputs
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeHardlimNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec = 1,
							  string name = string(""));

/**
 * @brief Build a neuron constraint with "hardlim" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeHardlimNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));
							
/**
 * @brief Build a neuron constraint with "hardlim" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeHardlimNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));

//------------------------------------------------------------------------------
// Tansig neuron
//------------------------------------------------------------------------------

/**
 * @brief Build a neuron constraint with "tansig" activation function
 * @param s the solver
 * @param size the number of inputs
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeTansigNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec = 1,
							  string name = string(""));

/**
 * @brief Build a neuron constraint with "tansig" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeTansigNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));
							
/**
 * @brief Build a neuron constraint with "tansig" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeTansigNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));
							
							
//------------------------------------------------------------------------------
// Purelin neuron
//------------------------------------------------------------------------------

/**
 * @brief Build a neuron constraint with "purelin" activation function
 * @param s the solver
 * @param size the number of inputs
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakePurelinNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec = 1,
							  string name = string(""));

/**
 * @brief Build a neuron constraint with "purelin" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakePurelinNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));
							
/**
 * @brief Build a neuron constraint with "purelin" activation function
 * @param s the solver
 * @param inputs the neuron inputs (each one is a integer variable)
 * @param output the neuron output
 * @param bias the bias (weight 0 in classical neuron models)
 * @param weights weight for each input
 * @param prec fixed precision (must be > 0, default 1)
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakePurelinNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec = 1,
							  string name = string(""));
							  
// ==========================================================================
// = Alternative approach to neuron constraints: linear constrain with real =
// = valued weights + activation function constraint                        =
// ==========================================================================
							  
Constraint* MakeActFunctionCst(Solver* const s,
						  IntExpr* x,
						  IntExpr* y,
						  int32 prec,
						  neuron_type type,
						  string name = string(""));
						  

Constraint* MakeRealWgtLinearCst(Solver* const s, 
							  int size,
							  const IntVar* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec,
							  string name = string(""));

Constraint* MakeRealWgtLinearCst(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name = string(""));

// // ======================================================
// // = Two layer, Feed Forward, Neural Network constraint =
// // ======================================================
							  
// Constraint* MakeTansig2LFFPropEnhancer(Solver* const s,
// 						 int isize,
// 						 int hsize,
// 						 const IntVar* const* inputs,
// 						 const IntVar* const* hidden_activities,
// 						 IntVar* output_activity,
// 						 double const* wgt0,
// 						 double const* wgt1,
// 						 int32 prec,
// 						 string name);

// Constraint* MakeTansig2LFFPropEnhancer(Solver* s,
// 							  const std::vector<IntVar*>& inputs,
// 							  const std::vector<IntVar*>& hidden_activities,
// 							  IntVar* output_activity,
// 							  const std::vector<double>& wgt0,
// 							  const std::vector<double>& wgt1,
// 							  int32 prec,
// 							  string name);

Constraint* MakeTansig2LFFCst(Solver* const s,
				 const std::vector<IntExpr*>& x,
				 IntExpr* out,
				 const std::vector<double>& wgt0,
				 const std::vector<double>& wgt1,
				 int32 prec,
				 int64 mode);
				 
// Constraint* MakeTansig2LFFCst(Solver* const s,
// 			 const std::vector<IntVar*>& x,
// 			 IntExpr* out,
// 			 const std::vector<double>& wgt0,
// 			 const std::vector<double>& wgt1,
// 			 int32 prec,
// 			 string name);

//------------------------------------------------------------------------------
// Rounded product constraint
// An input variable x can be normalized by posting:
// ROUNDPROD(x - x_min, 1/x_max)
//------------------------------------------------------------------------------

/**
 * @brief Build a rounded product constraint
 * Build a rounded product constraint; this enforces the relation:
 * y = ROUND(x * v)
 * where "x", "y" are integer expressions and "v" is a real valued coefficient
 * @param s the solver
 * @param input the "x" expression
 * @param output the "y" expression
 * @param coeff the coefficient
 * @return a pointer to the new constraint
 */
Constraint* MakeRoundedProductConstraint(Solver* s,
                                         IntExpr* const input,
                                         IntExpr* const output,
                                         double coeff);

//------------------------------------------------------------------------------
// Rounded division constraint
// Introduced to avoid expressing a division as a product (as it may lead to
// very large bounds during propagation and overflow errors)
//------------------------------------------------------------------------------

/**
 * @brief Build a rounded division constraint
 * Build a rounded product constraint; this enforces the relation:
 * z = ROUND(x / y)
 * where "x", "y", "z" are integer expressions
 * @param s the solver
 * @param num the "x" expression
 * @param den the "y" expression
 * @param output the "z" expression
 * @return a pointer to the new constraint
 */
Constraint* MakeRoundedDivisionConstraint(Solver* s,
								IntExpr* const num,
								IntExpr* const den,
								IntExpr* const output);

//------------------------------------------------------------------------------
// Regret constraint
// This constraint allows to compute the minimum and the second minimum of an
// array simultaneously
//------------------------------------------------------------------------------
								
Constraint* MakeRegretConstraint(Solver* const s,
							  int size,
							  const IntExpr* const* inputs,
                              IntExpr* const extr,
                              IntExpr* const second_extr);
							  
Constraint* MakeRegretConstraint(Solver* s,
                           const std::vector<IntExpr*>& inputs,
                           IntExpr* const extr,
						   IntExpr* const second_extr);
						   
Constraint* MakeRegretConstraint(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntExpr* const extr,
						   IntExpr* const second_extr);
                                        
//------------------------------------------------------------------------------
// Average constraint
//------------------------------------------------------------------------------                                        
                                         
/**
 * @brief Build an "average" constraint
 * Build an average constraint; this enforces the relation:
 * y = ROUND( (\sum_i w_i * x_i) / (\sum_i x_i) )
 * Where "x_" and "y" are integer expressions and "x_i" must be non-negative.
 * Terms "w_i" are real valued weights.
 * @param s the solver
 * @param size number of elments in the average
 * @param inputs the "x_i" expressions
 * @param output the "y" expression
 * @param weights the weights
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */ 
Constraint* MakeAverageCst(Solver* const s, 
                           int size,
                           const IntVar* const* inputs,
                           IntVar* const output,
                           int64 const* weights,
                           string name = string(""));

/**
 * @brief Build an "average" constraint
 * Build an average constraint; this enforces the relation:
 * y = ROUND( (\sum_i w_i * x_i) / (\sum_i x_i) )
 * Where "x_" and "y" are integer expressions and "x_i" must be non-negative.
 * Terms "w_i" are real valued weights.
 * @param s the solver
 * @param inputs the "x_i" expressions
 * @param output the "y" expression
 * @param weights the weights
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeAverageCst(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntVar* const output,
                           const std::vector<int64>& weights,
                           string name = string(""));

/**
 * @brief Build an "average" constraint
 * Build an average constraint; this enforces the relation:
 * y = ROUND( (\sum_i w_i * x_i) / (\sum_i x_i) )
 * Where "x_" and "y" are integer expressions and "x_i" must be non-negative.
 * Terms "w_i" are real valued weights.
 * @param s the solver
 * @param size number of elments in the average
 * @param inputs the "x_i" expressions
 * @param output the "y" expression
 * @param weights the weight variables
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */ 
Constraint* MakeAverageCst(Solver* const s, 
                           int size,
                           const IntVar* const* inputs,
                           IntVar* const output,
                           const IntVar* const* weights,
                           string name = string(""));

/**
 * @brief Build an "average" constraint
 * Build an average constraint; this enforces the relation:
 * y = ROUND( (\sum_i w_i * x_i) / (\sum_i x_i) )
 * Where "x_" and "y" are integer expressions and "x_i" must be non-negative.
 * Terms "w_i" are real valued weights.
 * @param s the solver
 * @param inputs the "x_i" expressions
 * @param output the "y" expression
 * @param weights the weight variables
 * @param name optional constraint name
 * @return a pointer to the new constraint
 */
Constraint* MakeAverageCst(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntVar* const output,
                           const std::vector<IntVar*>& weights,
                           string name = string(""));

// //
// // FIXME what is this?
// //
// Decision* MakeMyFixValues(Solver* s, const std::vector<IntVar*>& vars,
// 													const std::vector<int64>& vals);


} // end of namespace operations_research

#endif