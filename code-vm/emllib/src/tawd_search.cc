#include <tawd_search.h>

#include <iostream>

using namespace std;

namespace operations_research {

// my usual debug macros
#define DEBUG_ON true
#ifndef NDEBUG
#define DBG(X) if (DEBUG_ON) { X }
#else
#define DBG(X)
#endif
	
#ifndef NDEBUG
template <class T> static void print_array(T* v, int size) {
	cerr << "[";
	for (int i = 0; i < size; ++i)
		cerr << (i ? "," : "") << v[i];
	cerr << "]";
}
#endif

// ==========================================
// = Customized search for the TAWD problem =
// ==========================================
	
class TAWDSearch : public DecisionBuilder {
private:
	const vector<IntVar*> C_; // task assignment variables
	const vector<int64> cpi_; // weight of each task
	int64 maxcpi_; 
	int64 ncores_;
	
public:
	TAWDSearch(const vector<IntVar*>& C,
			   const vector<int64>& cpis)
		: C_(C), cpi_(cpis) {
		// find the maximum cpi value and the number of cores
		bool first = true;
		for (int i = 0; i < cpi_.size(); ++i) {
			if (first || cpi_[i] > maxcpi_)
				maxcpi_ = cpi_[i];
			if (first || C_[i]->Max()+1 > ncores_)
				ncores_ = C_[i]->Max()+1; 
			first = false;
		}
	}
	
	virtual ~TAWDSearch() {}
	
	virtual Decision* Next(Solver* const solver) {
		CHECK(solver == C_[0]->solver()) << "Non-matching solvers for the model and the search strategy";
		
		// choose the (unassigned) task with the lowest CPI and compute the
		// total workload for each core. As a workload measure for each task
		// we use "maxcpi_ - cpi_[i]", which is always >= 0 and it is higher for
		// computation intensive tasks
		int sel_idx = -1;
		vector<int64> wld(ncores_);
		for (int i = 0; i < C_.size(); ++i) {
			if (!C_[i]->Bound()) {
				if (sel_idx < 0 || cpi_[i] < cpi_[sel_idx])
						sel_idx = i;				
			}
			else {
				wld[C_[i]->Value()] += (maxcpi_ - cpi_[i]);
			}		
		}
				
		// if all variables are bound, the search is over
		if (sel_idx < 0) return NULL;
		
		// cerr << "task " << sel_idx << " with cpi " << cpi_[sel_idx] << endl;
		
		// othewise, place the selected task on the core with the lowest
		// total workload
		IntVarIterator* it = C_[sel_idx]->MakeDomainIterator(false);
		int64 sel_k = -1;
		for (it->Init(); it->Ok(); it->Next()) {
			int64 k = it->Value();
			if (sel_k < 0 || wld[k] < wld[sel_k])
				sel_k = k;
		}
		delete it;
		
		// cerr << "mapped on core " << sel_k << " with wld " << wld[sel_k] << endl;
		
		// return the branching decision
		return solver->MakeAssignVariableValue(C_[sel_idx], sel_k);
	}
	
    virtual string DebugString() const {
      return "TAWDSearch";
    }
};

DecisionBuilder* MakeTAWDSearch(Solver* s,
								const vector<IntVar*>& C,
			   			   		const vector<int64>& cpis) {
	return s->RevAlloc(new TAWDSearch(C, cpis));
}

// ===============================================
// = Customized search for the TAWD problem (V2) =
// ===============================================
	
class TAWDSearch2 : public DecisionBuilder {
private:
	const vector<IntVar*> C_; // task assignment variables
	const vector<int64> cpi_; // weight of each task
	int64 maxcpi_; 
	int64 ncores_;
	
public:
	TAWDSearch2(const vector<IntVar*>& C,
			   const vector<int64>& cpis)
		: C_(C), cpi_(cpis) {
		// find the maximum cpi value and the number of cores
		bool first = true;
		for (int i = 0; i < cpi_.size(); ++i) {
			if (first || cpi_[i] > maxcpi_)
				maxcpi_ = cpi_[i];
			if (first || C_[i]->Max()+1 > ncores_)
				ncores_ = C_[i]->Max()+1; 
			first = false;
		}
	}
	
	virtual ~TAWDSearch2() {}
	
	virtual Decision* Next(Solver* const solver) {
		CHECK(solver == C_[0]->solver()) << "Non-matching solvers for the model and the search strategy";
		
		// choose the (unassigned) task with the smallest domain and compute the
		// total workload for each core. As a workload measure for each task
		// we use "maxcpi_ - cpi_[i]", which is always >= 0 and it is higher for
		// computation intensive tasks
		int sel_idx = -1;
		vector<int64> wld(ncores_);
		for (int i = 0; i < C_.size(); ++i) {
			if (!C_[i]->Bound()) {
				if (sel_idx < 0 || C_[i]->Size() < C_[sel_idx]->Size())
						sel_idx = i;				
			}
			else {
				wld[C_[i]->Value()] += (maxcpi_ - cpi_[i]);
			}		
		}
				
		// if all variables are bound, the search is over
		if (sel_idx < 0) return NULL;
		
		// cerr << "task " << sel_idx << " with cpi " << cpi_[sel_idx] << endl;
		
		// othewise, place the selected task on the core with the lowest
		// total workload
		IntVarIterator* it = C_[sel_idx]->MakeDomainIterator(false);
		int64 sel_k = -1;
		for (it->Init(); it->Ok(); it->Next()) {
			int64 k = it->Value();
			if (sel_k < 0 || wld[k] < wld[sel_k])
				sel_k = k;
		}
		delete it;
		
		// cerr << "mapped on core " << sel_k << " with wld " << wld[sel_k] << endl;
		
		// return the branching decision
		return solver->MakeAssignVariableValue(C_[sel_idx], sel_k);
	}
	
    virtual string DebugString() const {
      return "TAWDSearch2";
    }
};

DecisionBuilder* MakeTAWDSearch2(Solver* s,
								const vector<IntVar*>& C,
			   			   		const vector<int64>& cpis) {
	return s->RevAlloc(new TAWDSearch2(C, cpis));
}

// ============================================================================
// = Second customized search for the TAWD problem (reserved tasks supported) =
// ============================================================================

class TAWDResSearch : public DecisionBuilder {
private:
	const vector<IntVar*> C_res_; // task assignment variables (reserved eff.)
	const vector<IntVar*> C_otr_; // task assignment variables (others)
	const vector<int64> cpi_res_; // weight of each task (reserved eff.)
	const vector<int64> cpi_otr_; // weight of each task (others)
	int64 maxcpi_; 
	int64 ncores_;
	
public:
	TAWDResSearch(const vector<IntVar*>& C_res,
				  const vector<IntVar*>& C_otr,
			   	  const vector<int64>& cpi_res,
				  const vector<int64>& cpi_otr)
		: C_res_(C_res), C_otr_(C_otr), cpi_res_(cpi_res), cpi_otr_(cpi_otr) {
		// find the maximum cpi value and the number of cores
		bool first = true;
		for (int i = 0; i < cpi_otr_.size(); ++i) {
			if (first || cpi_otr_[i] > maxcpi_)
				maxcpi_ = cpi_otr_[i];
			if (first || C_otr_[i]->Max()+1 > ncores_)
				ncores_ = C_otr_[i]->Max()+1; 
			first = false;
		}
		for (int i = 0; i < cpi_res_.size(); ++i) {
			if (cpi_res_[i] > maxcpi_)
				maxcpi_ = cpi_res_[i];
			if (C_res_[i]->Max()+1 > ncores_)
				ncores_ = C_res_[i]->Max()+1; 
		}
	}
	
	virtual ~TAWDResSearch() {}
	
	virtual Decision* Next(Solver* const solver) {
		CHECK(solver == C_otr_[0]->solver()) << "Non-matching solvers for the model and the search strategy";
		CHECK(solver == C_res_[0]->solver()) << "Non-matching solvers for the model and the search strategy";
		
		// choose the (unassigned) NON-RESERVED tasks with the lowest CPI and
		// compute the total workload for each core. As a workload measure for
		// each task we use "maxcpi_ - cpi_[i]", which is always >= 0 and it is
		// higher for computation intensive tasks
		int sel_idx = -1;
		vector<int64> wld(ncores_);
		for (int i = 0; i < C_otr_.size(); ++i) {
			if (!C_otr_[i]->Bound()) {
				if (sel_idx < 0 || cpi_otr_[i] < cpi_otr_[sel_idx])
						sel_idx = i;				
			}
			else {
				wld[C_otr_[i]->Value()] += (maxcpi_ - cpi_otr_[i]);
			}		
		}
		
		// if all tasks without reservations where bound, let's consider the
		// tasks with a quality guarantee
		
		for (int i = 0; i < C_res_.size(); ++i) {
			if (!C_res_[i]->Bound()) {
				if (sel_idx < 0 || cpi_res_[i] < cpi_res_[sel_idx])
						sel_idx = C_otr_.size() + i;				
			}
			else {
				wld[C_res_[i]->Value()] += (maxcpi_ - cpi_res_[i]);
			}		
		}
		
		// if all variables are bound, the search is over
		if (sel_idx < 0) return NULL;
		
		// cerr << "task " << sel_idx << " with cpi " << cpi_[sel_idx] << endl;
		
		// prepare a pointer to the selected variables
		IntVar* C_sel_ = (sel_idx >= C_otr_.size()
						  ? C_res_[sel_idx-C_otr_.size()]
						  : C_otr_[sel_idx]);
		
		// othewise, place the selected task on the core with the lowest
		// total workload
		IntVarIterator* it = C_sel_->MakeDomainIterator(false);
		int64 sel_k = -1;
		for (it->Init(); it->Ok(); it->Next()) {
			int64 k = it->Value();
			if (sel_k < 0 || wld[k] < wld[sel_k])
				sel_k = k;
		}
		delete it;
		
		// cerr << "mapped on core " << sel_k << " with wld " << wld[sel_k] << endl;
		
		// return the branching decision
		return solver->MakeAssignVariableValue(C_sel_, sel_k);
	}
	
    virtual string DebugString() const {
      return "TAWDSearch";
    }
};

DecisionBuilder* MakeTAWDResSearch(Solver* s,
								const vector<IntVar*>& C_res,
								const vector<IntVar*>& C_otr,
			   			   		const vector<int64>& cpi_res,
								const vector<int64>& cpi_otr) {
	return s->RevAlloc(new TAWDResSearch(C_res, C_otr, cpi_res, cpi_otr));
}

} // end of operations_research namespace