#include <neuron_cst.h>

using namespace std;

namespace operations_research {

#define NN_INFINITY numeric_limits<double>::infinity()
#define NN_EPSILON 10e-9
	
#define DOUBLE_MAX numeric_limits<double>::max()
#define DOUBLE_MIN (-numeric_limits<double>::max())
	
// #define INT64_MAX numeric_limits<int64>::max()
// #define INT64_MIN numeric_limits<int64>::min()

// my usual debug macros
#define DEBUG_ON true
#ifndef NDEBUG
#define DBG(X) if (DEBUG_ON) { X }
#else
#define DBG(X)
#endif
	
#ifndef NDEBUG
template <class T> static void print_array(T* v, int size) {
	cerr << "[";
	for (int i = 0; i < size; ++i)
		cerr << (i ? "," : "") << v[i];
	cerr << "]";
}
#endif
    
// A simple rounding method
#define ROUND(X) (((X) > 0.0) ? floor((X) + 0.5) : ceil((X) - 0.5))
#define DE_ROUND_UP(X) (((X) >= 0.0) ? ((X) + 0.5 - NN_EPSILON) : ((X) + 0.5))
#define DE_ROUND_DOWN(X) (((X) > 0.0) ? ((X) - 0.5) : ((X) - 0.5 + NN_EPSILON))
	
	
// DE-COMMENT THIS TO DISABLE PROPAGATION
// #define DISABLE_NN_PROPAGATION

//==============================================================================
// The basic constraint implementation class: this handles the internal
// activity variable and the propagation. The activation function is left
// unspecified, but it is assumed to be monotonic non-decreasing
//==============================================================================
	
class BaseNeuronConstraint : public Constraint
{
private:
	// internal copy of the weight array
	double* weights_;
	// internal pointers to the input variables (auto-initialized)
	IntExpr** inputs_;
	// internal pointer output variable (auto-initialized)
	IntVar* const output_;
	// number of inputs
	int size_;
	// precision (stored as a double, but this is in fact an integer)
	double prec_;
	// bias
	double bias_;
	// constraint name
	string name_;
	
public:
	/**
	 * @brief Constructor to build the basic structure of a Neuron Constraint
	 * Constructor to build the basic structure of a Neuron Constraint; note
	 * this is a pure virtual class
	 * @param s the solver
	 * @param inputs neuron inputs (variables)
	 * @param weights input weights
	 * @param size number of neuron inputs
	 * @param output neuron output variable
	 * @param name constraint name (optional)
	 */
	BaseNeuronConstraint(Solver* const s,
						 int size,
						 const IntExpr* const* inputs,
						 IntVar* const output,
						 double bias,
						 double const* weights,
						 int32 prec,
						 string name)
	: Constraint(s), size_(size), output_(output), name_(name),
	  prec_(prec), bias_(bias)
	{
		/* if size is 0, perform no intialization */
		if (size_ > 0) {
			// init array of input variables
			inputs_ = new IntExpr*[size_];
			memcpy(inputs_, inputs, size_ * sizeof(*inputs));
			// init array of weights
			weights_ = new double[size_];
			memcpy(weights_, weights, size_ * sizeof(*weights));
		}
	}

	virtual ~BaseNeuronConstraint() {
		delete[] inputs_;
		delete[] weights_;
	}
	

	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
		// the InitialPropagate method should be sufficient for this constraint
		Demon* d = MakeConstraintDemon0(solver(), this,
						&BaseNeuronConstraint::PropagateINOUT, "inout");
		// post on all neuron inputs
		for(int i = 0; i < size_; ++i)
			inputs_[i]->WhenRange(d);
		// post on neuron output
		output_->WhenRange(d);
	}
	
#undef DEBUG_ON
#define DEBUG_ON false
    void PropagateINOUT() {
		#ifdef DISABLE_NN_PROPAGATION
		for (int i = 0; i < size_; ++i)
			if (!inputs_[i]->Bound()) return;
		#endif
		
		/* propagate min from inputs to output */
		DBG(cout << endl << "*** PropagateINOUT ***" << endl;)
		DBG(cout << this << endl;)
		DBG(cout << "activity min to output min" << endl;)
		double act_min = bias_ * prec_; // this is the minimum possible activity
		for (int i = 0; i < size_; ++i)
			if (weights_[i] >= 0) act_min += weights_[i] * inputs_[i]->Min();
            else act_min += weights_[i] * inputs_[i]->Max();
		act_min /= prec_; // scale down to actual value
//		int64 out_lb = (int64) ceil(prec_ * output_lb(act_min));
        int64 out_lb = (int64) ROUND(prec_ * output_lb(act_min));
        DBG(cout << "activity min: " << act_min << endl;)
        DBG(cout << "output lb: " << output_lb(act_min) << endl;)
		output_->SetMin(out_lb);
		DBG(cout << this << endl;)
		
		/* propagate max from inputs to output */
		DBG(cout << "activity max to output max" << endl;)
		double act_max = bias_ * prec_; // this is the minimum possible activity
		for (int i = 0; i < size_; ++i)
			if (weights_[i] >= 0) act_max += weights_[i] * inputs_[i]->Max();
            else act_max += weights_[i] * inputs_[i]->Min();
		act_max /= prec_; // scale down to actual value
//		int64 out_ub = (int64) floor(prec_ * output_ub(act_max));
        int64 out_ub = (int64) ROUND(prec_ * output_ub(act_max));
        DBG(cout << "activity max: " << act_max << endl;)
        DBG(cout << "output ub: " << output_ub(act_max) << endl;)
		output_->SetMax(out_ub);
		DBG(cout << this << endl;)
		
		/* propagate max from output to inputs */
		DBG(cout << "output max to activity max" << endl;)
		double y_max = output_->Max();
		if (!is_int() && y_max < prec_*output_self_ub())
			y_max = DE_ROUND_UP(y_max); // apply reverse rouding fix
		double act_ub = prec_ * activity_ub(y_max/prec_); // unscaled
		if (act_ub != NN_INFINITY)
			for (int i = 0; i < size_; ++i) {
				// actions and bounds depend on the weight sign
				if (weights_[i] >= 0) {
					// weight of current input in the act_min value (unscaled)
					double in_wgt = inputs_[i]->Min()*weights_[i];
					double act_res = prec_ * act_min - in_wgt;
					int64 in_ub = (int64) floor((act_ub - act_res)/weights_[i]);
					inputs_[i]->SetMax(in_ub);
				}
				else {
					// weight of current input in the act_min value (unscaled)
					double in_wgt = inputs_[i]->Max()*weights_[i];
					double act_res = prec_ * act_min - in_wgt;
					int64 in_ub = (int64) ceil((act_ub - act_res)/weights_[i]);
					inputs_[i]->SetMin(in_ub);
				}
			}
		DBG(cout << this << endl;)
		
		/* propagate min from output to inputs */
		DBG(cout << "output min to activity min" << endl;)
		double y_min = output_->Min();
		if (!is_int() && y_min > prec_ * output_self_lb())
		    y_min = DE_ROUND_DOWN(y_min); // apply reverse rouding fix        
		double act_lb = prec_ * activity_lb(y_min/prec_); // unscaled
		if (act_lb != -NN_INFINITY)
			for (int i = 0; i < size_; ++i) {
		                // actions and bounds depend on the weight sign
		                if (weights_[i] >= 0) {
		                    // weight of current input in the act_max value (unscaled)
		                    double in_wgt = inputs_[i]->Max()*weights_[i];
		                    double act_res = prec_ * act_max - in_wgt;
		                    int64 in_lb = (int64) ceil((act_lb - act_res)/weights_[i]);
		                    inputs_[i]->SetMin(in_lb);
		                }
		                else {
		                    // weight of current input in the act_max value (unscaled)
		                    double in_wgt = inputs_[i]->Min()*weights_[i];
		                    double act_res = prec_ * act_max - in_wgt;
		                    int64 in_lb = (int64) floor((act_lb - act_res)/weights_[i]);
		                    inputs_[i]->SetMax(in_lb);                    
		                }
			}
		DBG(cout << this << endl;)   
    }
	
	/**
	 * @brief Propagation method
	 */
	void InitialPropagate() {
		DBG(cout << endl << "*** InitialPropagate ***" << endl;)
      DBG(cout << this << endl;)
		/* output self propagation */
		DBG(cout << "performing self propagation" << endl;)
		double self_ub = output_self_ub();
		if (self_ub != NN_INFINITY) {
			int64 self_out_ub = ROUND(prec_ * self_ub);
			output_->SetMax(self_out_ub);
		}
		double self_lb = output_self_lb();
		if (self_lb != -NN_INFINITY) {
			int64 self_out_lb = ROUND(prec_ * self_lb);
			output_->SetMin(self_out_lb);
		}
		if (is_int())
		    output_->RemoveInterval(output_->Min()+1, output_->Max()-1);
		DBG(cout << this << endl;)
		
		// then, perform the base neuron constraint propagation
		PropagateINOUT();
	}
	
#undef DEBUG_ON
#define DEBUG_ON true	
	
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		
		// append output
		out.append(output_->DebugString());
		out.append(" = ");
		
		// append type
		out.append(get_type());
		
		// append arguments
		out.append("(");
		StringAppendF(&out, "%g", bias_ * prec_);
		for (int i = 0; i < size_; ++i) {
			out.append(" + ");
			StringAppendF(&out, "%g", weights_[i]);
			out.append("*");
			out.append(inputs_[i]->DebugString());
		}
		
		// close string and return
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(get_type(), this);
        for(int i = 0; i < size_; ++i)
            visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												   inputs_[0]);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												output_);
		visitor->EndVisitConstraint(get_type(), this);
	}
	
protected:
	//--------------------------------------------------------------------------
	// Method to specify the behavior of the activation function
	//--------------------------------------------------------------------------

	// the largest/smalles possible output bound corresponding to the specified 
	// activity value; in general, those are both "act_function(act_val)"
	virtual double output_ub(double act_val) const = 0;
	virtual double output_lb(double act_val) const = 0;
	
	// the largest/smalles possible activity value corresponding to the
	// specified  output value; in principle, those should both be
	// "act_function^-1(out_val)". However, this may not be the case due to
	// machine dependent precision issues
	virtual double activity_ub(double out_val) const = 0;
	virtual double activity_lb(double out_val) const = 0;
	
	// methods to restrict output domain
	virtual double output_self_ub() const = 0;
	virtual double output_self_lb() const = 0;
	
	// get the constraint type (used to support ModelVisitor)
	virtual const char* get_type() const = 0;
    
	// tells whether the neuron has integer of real valued output
	virtual bool is_int() const = 0;

}; // end of BaseNeuronConstraint class

//==============================================================================
// Classes for specific neuron constraints
//==============================================================================
	
//------------------------------------------------------------------------------
// Neuron Constraint with Hardlim activation function (step, output in {0,+1})
//------------------------------------------------------------------------------

/**
 * @class HardlimNeuron
 * @author Michele Lombardi
 * @date 11/02/2011
 * @file neuron_cst.cc
 * @brief Neuron Constraint with Hardlim activation function (step, output in {0,+1})
 */
class HardlimNeuron : public BaseNeuronConstraint {
public:
	HardlimNeuron(Solver* const s,
				  int size,
				  const IntExpr* const* inputs,
				  IntVar* const output,
				  double bias,
				  double const* weights,
				  int32 prec,
				  string name) :
				  BaseNeuronConstraint(s, size, inputs, output,
									   bias, weights, prec, name) {}
									   
	virtual ~HardlimNeuron() {}
									   
private:
	// bounds on the output variable, based on the current output bounds
	double output_self_ub() const {
		return 1L;
	}
	double output_self_lb() const {
		return 0L;
	}
	
	// definition of the hardlim function; the inverse hardlim function
	// is not defined (it lacks injectivity)
	#define HARDLIM(X) ((X) < 0 ? 0 : 1)

	// bounds on the output variable, based on activity values
	double output_ub(double act_val) const {
		return HARDLIM(act_val);
	}
	double output_lb(double act_val) const {
		return HARDLIM(act_val);
	}
	
	// bounds on the activity variable, based on output values
	double activity_ub(double out_val) const {
		CHECK(out_val == 0 || out_val == 1) << "invalid neuron output";
		return out_val == 1 ? NN_INFINITY : 0L-NN_EPSILON;
	}
	double activity_lb(double out_val) const {
		CHECK(out_val == 0 || out_val == 1) << "invalid neuron output";
		return out_val == 1 ? 0L : -NN_INFINITY;
	}
	
	// get the constraint type (used to support ModelVisitor)
	const char* get_type() const {
		return "Hardlim";
	}
    
    // the hardlim neuron has integer output
    bool is_int() const {
        return true;
    }
};

//
// Functions to build a neuron constraint
//

Constraint* MakeHardlimNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec,
							  string name) {
  CHECK(inputs != NULL) << "NULL neuron input array, maybe a bad cast";
  CHECK(weights != NULL) << "NULL neuron weight array, maybe a bad cast";
  CHECK(output != NULL) << "NULL neuron output, maybe a bad cast";
  CHECK_EQ(s, output->solver());
  CHECK(prec > 0) << "invalid neuron precision";
  for (int i = 0; i < size; ++i) CHECK_EQ(s, inputs[i]->solver());
  return s->RevAlloc(new HardlimNeuron(s, size, inputs, output,
									   bias, weights, prec, name));
}


Constraint* MakeHardlimNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakeHardlimNeuron(s, inputs.size(), inputs.data(), output,
							  bias, weights.data(), prec, name);
}

Constraint* MakeHardlimNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakeHardlimNeuron(s, inputs.size(), (IntExpr**) inputs.data(), output,
							  bias, weights.data(), prec, name);
}


//------------------------------------------------------------------------------
// Neuron Constraint with Tansig activation function (output in [0,+1])
//------------------------------------------------------------------------------

/**
 * @class TansigNeuron
 * @author Michele Lombardi
 * @date 11/02/2011
 * @file neuron_cst.cc
 * @brief Neuron Constraint with Tansig activation function (cont, output in [-1,+1])
 */
class TansigNeuron : public BaseNeuronConstraint {
public:
	TansigNeuron(Solver* const s,
				  int size,
				  const IntExpr* const* inputs,
				  IntVar* const output,
				  double bias,
				  double const* weights,
				  int32 prec,
				  string name) :
				  BaseNeuronConstraint(s, size, inputs, output,
									   bias, weights, prec, name) {}
									   
	virtual ~TansigNeuron() {}
									   
private:
	// bounds on the output variable, based on the current output bounds
	double output_self_ub() const {
		return 1L;
	}
	double output_self_lb() const {
		return -1L;
	}
	
	// definition of the tansig function
	#define TANSIG(X) (2/(1 + exp(-2 *(X)) ) - 1)

	// in principle tansig can be inverted in the open interval ]-1,1[
	// due to precision issues, this is not true in practice and the boudaries
	// of the invertibility interval are system dependent. Within the interval
	// inverse tansig is
	#define INVTANSIG(X) (-0.5*log( (1-(X))/(1+(X)) ) )
	
	// upper and lower bound of the invertibility interval (those are system
	// wide values, which should be re-computed whenver NN_EPSILON is changed
	#define INV_UB 18.36840028874576
	#define INV_LB -19.061547461897135

	// bounds on the output variable, based on activity values
	double output_ub(double act_val) const {
		return TANSIG(act_val);
	}
	double output_lb(double act_val) const {
		return TANSIG(act_val);
	}
	
	// bounds on the activity variable, based on output values
	double activity_ub(double out_val) const {
//		CHECK(out_val >= -1 || out_val <= 1) << "invalid neuron output";
		return out_val >= 1 ? NN_INFINITY :
			   out_val <= -1 ? INV_LB : INVTANSIG(out_val);
	}
	double activity_lb(double out_val) const {
//		CHECK(out_val >= -1 || out_val <= 1) << "invalid neuron output";
		return out_val >= 1 ? INV_UB :
			   out_val <= -1 ? -NN_INFINITY : INVTANSIG(out_val);
	}
	
	// get the constraint type (used to support ModelVisitor)
	const char* get_type() const {
		return "Tansig";
	}
    
    // the tansig neuron has real valued output
    bool is_int() const {
        return false;
    }
	
};

// TODO add methods to access the neuron activity in BaseNeuronConstraint
// TODO altertively, add a function to access the activity

//
// Functions to build a neuron constraint
//

Constraint* MakeTansigNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec,
							  string name) {
  CHECK(inputs != NULL) << "NULL neuron input array, maybe a bad cast";
  CHECK(weights != NULL) << "NULL neuron weight array, maybe a bad cast";
  CHECK(output != NULL) << "NULL neuron output, maybe a bad cast";
  CHECK_EQ(s, output->solver());
  CHECK(prec > 0) << "invalid neuron precision";
  for (int i = 0; i < size; ++i) CHECK_EQ(s, inputs[i]->solver());
  return s->RevAlloc(new TansigNeuron(s, size, inputs, output,
											bias, weights, prec, name));
}


Constraint* MakeTansigNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakeTansigNeuron(s, inputs.size(), inputs.data(), output,
							  bias, weights.data(), prec, name);
}

Constraint* MakeTansigNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakeTansigNeuron(s, inputs.size(), (IntExpr**) inputs.data(), output,
							  bias, weights.data(), prec, name);
}

//------------------------------------------------------------------------------
// Neuron Constraint with linear activation function (output in ]-infty,+intfy[)
//------------------------------------------------------------------------------

/**
 * @class PurelinNeuron
 * @author Michele Lombardi
 * @date 11/02/2011
 * @file neuron_cst.cc
 * @brief Neuron Constraint with linear activation function (output in ]-infty,+intfy[)
 */
class PurelinNeuron : public BaseNeuronConstraint {
public:
	PurelinNeuron(Solver* const s,
				  int size,
				  const IntExpr* const* inputs,
				  IntVar* const output,
				  double bias,
				  double const* weights,
				  int32 prec,
				  string name) :
				  BaseNeuronConstraint(s, size, inputs, output,
									   bias, weights, prec, name) {}
									   
	virtual ~PurelinNeuron() {}
									   
private:
	// bounds on the output variable, based on the current output bounds
	double output_self_ub() const {
		return +NN_INFINITY;
	}
	double output_self_lb() const {
		return -NN_INFINITY;
	}
	
	// bounds on the output variable, based on activity values
	double output_ub(double act_val) const {
		return act_val;
	}
	double output_lb(double act_val) const {
		return act_val;
	}
	
	// bounds on the activity variable, based on output values
	double activity_ub(double out_val) const {
		return out_val;
	}
	double activity_lb(double out_val) const {
		return out_val;
	}
	
	// get the constraint type (used to support ModelVisitor)
	const char* get_type() const {
		return "Purelin";
	}
    
    // the purelin neuron has real valued output
    bool is_int() const {
        return false;
    }
	
};

//
// Functions to build a neuron constraint
//

Constraint* MakePurelinNeuron(Solver* const s, 
							  int size,
							  const IntExpr* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec,
							  string name) {
  CHECK(inputs != NULL) << "NULL neuron input array, maybe a bad cast";
  CHECK(weights != NULL) << "NULL neuron weight array, maybe a bad cast";
  CHECK(output != NULL) << "NULL neuron output, maybe a bad cast";
  CHECK_EQ(s, output->solver());
  CHECK(prec > 0) << "invalid neuron precision";
  for (int i = 0; i < size; ++i) CHECK_EQ(s, inputs[i]->solver());
  return s->RevAlloc(new PurelinNeuron(s, size, inputs, output,
											bias, weights, prec, name));
}


Constraint* MakePurelinNeuron(Solver* s,
							  const std::vector<IntExpr*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakePurelinNeuron(s, inputs.size(), inputs.data(), output,
							  bias, weights.data(), prec, name);
}

Constraint* MakePurelinNeuron(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakePurelinNeuron(s, inputs.size(), (IntExpr**) inputs.data(), output,
							  bias, weights.data(), prec, name);
}

//==============================================================================
// Alterantive approach to neuron constraints: linear constraint + activation
// function constraint
//==============================================================================


#undef DEBUG_ON
#define DEBUG_ON false
class ActFunctionCst : public Constraint {
	
private:
	// internal pointer to the input variable
	IntExpr* x_;
	// internal pointer to the output variable
	IntExpr* y_;
	// precision (stored as a double, but this is in fact an integer)
	double prec_;
	// activation function type
	neuron_type type_;
	// constraint name
	string name_;
	
public:
	ActFunctionCst(Solver* const s,
						 IntExpr* x,
						 IntExpr* y,
						 int32 prec,
						 neuron_type type,
						 string name)
	: Constraint(s), prec_(prec), name_(name), x_(x), y_(y), type_(type)
	{}
	
	virtual ~ActFunctionCst() {}
	
	/**
	 * @brief Post method; the constraint perform forward propagation only
	 */
	void Post() {
		DBG(cerr << "ActFunctionCst::Post" << endl;);

		// post on the input variable
		x_->WhenRange(MakeConstraintDemon0(
								solver(), this,
								&ActFunctionCst::PropagateIn2Out,
								"PropagateIn2Out"));

		// post on the input variable
		y_->WhenRange(MakeConstraintDemon0(
								solver(), this,
								&ActFunctionCst::PropagateOut2In,
								"PropagateOut2In"));
	}		
	
	// ===============================
	// = Propagation related methods =
	// ===============================
	
	double activation_function(neuron_type type, double v) {
		switch (type) {
			case ntype_purelin:
				return v;
			case ntype_tansig:
				return TANSIG(v);
			case ntype_hardlim:
				return v >= 0 ? -1 : 1;
		}
	}
	
	double inv_activation_function_ub(neuron_type type, double v) {
		switch (type) {
			case ntype_purelin:
				return v;
			case ntype_tansig:
				assert(v >= -1 && v <= 1);
				return v == 1 ? NN_INFINITY :
								v == -1 ? INV_LB : INVTANSIG(v);
			case ntype_hardlim:
				assert(v == -1 && v == 1);
				return v == 1 ? NN_INFINITY : 0-NN_EPSILON;
		}
	}
	
	double inv_activation_function_lb(neuron_type type, double v) {
		switch (type) {
			case ntype_purelin:
				return v;
			case ntype_tansig:
				assert(v >= -1 && v <= 1);
				return v == 1 ? INV_UB :
								v == -1 ? -NN_INFINITY : INVTANSIG(v);
			case ntype_hardlim:
				assert(v == -1 && v == 1);
				return v == 1 ? 0 : -NN_INFINITY;
		}
	}
	
	
	// ==============================
	// = Actual propagation methods =
	// ==============================
		
	// method for forward propagation
	void PropagateIn2Out() {
		DBG(cerr << "ActFunctionCst::PropagateIn2Out" << endl;);
		DBG(cerr << ">> before: " << this << endl;);
			
		double ub = activation_function(type_, x_->Max() / prec_);
		double lb = activation_function(type_, x_->Min() / prec_);
				
		// prune the output variable
		y_->SetMax(ROUND(ub * prec_));
		y_->SetMin(ROUND(lb * prec_));
		
		DBG(cerr << "<< after: " << this << endl;);
	}
	
	void PropagateOut2In() {
		DBG(cerr << "ActFunctionCst::PropagateOut2In" << endl;);
		DBG(cerr << ">> before: " << this << endl;);
		
		// de-round the output variable
		double ymax = DE_ROUND_UP(y_->Max());		
		double ymin = DE_ROUND_DOWN(y_->Min());
		
		DBG(cerr << "uncapped ymax: " << ymax << endl;);
		DBG(cerr << "uncapped ymin: " << ymin << endl;);
		
		// capping (for tansig and hardlim)
		if (type_ == ntype_tansig || type_ == ntype_hardlim) {
			ymax = ymax > prec_ ? prec_ : ymax;
			ymin = ymin < -prec_ ? -prec_ : ymin;
		}
		
		DBG(cerr << "ymax: " << ymax << endl;);
		DBG(cerr << "ymin: " << ymin << endl;);

		// compute (scaled) bounds
		double ub = inv_activation_function_ub(type_, ymax / prec_);
		double lb = inv_activation_function_lb(type_, ymin / prec_);

		DBG(cerr << "ub: " << ub << endl;);
		DBG(cerr << "lb: " << lb << endl;);
		
		// apply the bounds
		if (ub == NN_INFINITY); // do nothing
		else if (ub == -NN_INFINITY) solver()->Fail();
		else x_->SetMax(ceil(ub * prec_)); // scale down and prune

		if (lb == -NN_INFINITY); // do nothing
		else if (-lb == NN_INFINITY) solver()->Fail();
		else x_->SetMin(floor(lb * prec_)); // scale down and prune
	
		DBG(cerr << "<< after: " << this << endl;);
	}
	
	// Initial propagation method
	void InitialPropagate() {
		DBG(cerr << "ActFunctionCst::InitialPropagate" << endl;);
		PropagateIn2Out();
		PropagateOut2In();
	}
	
	const char* get_ntype_string(neuron_type t) const {
		switch(t) {
			case ntype_purelin:
				return "purelin";
			case ntype_tansig:
				return "tansig";
			case ntype_hardlim:
				return "hardlim";
			default:
				cerr << "ERROR: unknown neuron type" << endl;
				exit(1);
		}
	}
	
	// 
	string DebugString() const {
		string out;
			
		// add the description of the output variable
		out.append(y_->DebugString());
		out.append(" = ");
		// bias of the output neuron activity
		out.append(get_ntype_string(type_));
		out.append("(");
		out.append(x_->DebugString());
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint("ActFunctionCst", this);
		visitor->VisitIntegerExpressionArgument(
			ModelVisitor::kExpressionArgument, y_);
		visitor->VisitIntegerExpressionArgument(
			ModelVisitor::kExpressionArgument, x_);
		visitor->EndVisitConstraint("ActFunctionCst", this);
	}

};
#undef DEBUG_ON
#define DEBUG_ON true

Constraint* MakeActFunctionCst(Solver* const s,
							  IntExpr* x,
							  IntExpr* y,
							  int32 prec,
							  neuron_type type,
							  string name) {
  CHECK(x != NULL) << "NULL input variable, maybe a bad cast";
  CHECK(y != NULL) << "NULL output variable, maybe a bad cast";
  CHECK_EQ(s, x->solver());
  CHECK_EQ(s, y->solver());
  CHECK(prec > 0) << "invalid precision";
  CHECK(type == ntype_hardlim ||
	    type == ntype_tansig ||
		type == ntype_purelin) << "invalid activation function type";
  return s->RevAlloc(new ActFunctionCst(s, x, y, prec, type, name));
}

class RealWgtLinearCst : public Constraint {
	
private:
	// bias
	double bias_;
	// internal copy of the input weight array
	double* wgt_;
	// internal pointers to the input variables
	IntVar** x_;
	// internal pointers to the output variable
	IntVar* y_;
	// number of inputs
	int n_;
	// precision (stored as a double, but this is in fact an integer)
	double prec_;
	// constraint name
	string name_;
	
	// the last computed output bounds
	// NumericalRev<double> y_lb;
	// NumericalRev<double> y_ub;
	NumericalRev<double> y_lb;
	NumericalRev<double> y_ub;

public:
	RealWgtLinearCst(Solver* const s,
						 int n,
						 const IntVar* const* x,
						 IntVar* y,
						 double bias,
						 double const* wgt,
						 int32 prec,
						 string name)
	: Constraint(s), n_(n), prec_(prec), name_(name), y_(y), bias_(bias),
	  y_lb(0), y_ub(0)
	{
		assert(n > 0);
		// init array of input variables
		x_ = new IntVar*[n_];
		memcpy(x_, x, n_ * sizeof(*x));
		// init weight array
		wgt_ = new double[n_];
		memcpy(wgt_, wgt, n_ * sizeof(*wgt));
	}
	
	virtual ~RealWgtLinearCst() {
		delete[] x_;
		delete[] wgt_;
	}
	
	/**
	 * @brief Post method; the constraint perform forward propagation only
	 */
#undef DEBUG_ON
#define DEBUG_ON false
	void Post() {
		DBG(cerr << "RealWgtLinearCst::Post" << endl;);
		// Prepare the daemons to run the actual propagation routines
		Demon* out2in = MakeDelayedConstraintDemon0(solver(), this,
			&RealWgtLinearCst::PropagateOut2In, "PropagateOut2In");

		// post on all input variables
		for(int i = 0; i < n_; ++i) {
			// post the forward propagation daemon (constant time complexity,
			// immediate firing)
			x_[i]->WhenRange(MakeConstraintDemon1(
									solver(), this,
									&RealWgtLinearCst::PropagateIn2Out,
									"PropagateIn2Out", i));
			// post the backward propagation daemon (this is only needed if
			// the min/max of the y variable is not the same as the last
			// computed bounds: this condition is checked in PropagateIn2Out)
			x_[i]->WhenRange(out2in);
		}
		// post on the output variable
		y_->WhenRange(out2in);
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// ===============================
	// = Propagation related methods =
	// ===============================
	
	// method for forward propagation
#undef DEBUG_ON
#define DEBUG_ON true
	void PropagateIn2Out(int i) {
		DBG(cerr << "RealWgtLinearCst::PropagateIn2Out" << endl;);
		DBG(cerr << ">> before: " << this << endl;);
		
		DBG(cerr << "x_[" << i << "] = " 
				 << "[" << x_[i]->OldMin() << "," << x_[i]->OldMax() << "] "
				 << "--> [" << x_[i]->Min() << "," << x_[i]->Max() << "]"
				 << endl;);
		
		// update bounds
		DBG(cerr << "old y_ub = " << y_ub.Value() << endl;);
		DBG(cerr << "old y_lb = " << y_lb.Value() << endl;);

		// // DBG(cerr << "wgt_[" << i << "] = " << wgt_[i] << endl;);
		// if (wgt_[i] >= 0) {			
		// 	y_ub.Add(solver(), wgt_[i] * (x_[i]->Max() - x_[i]->OldMax()));
		// 	y_lb.Add(solver(), wgt_[i] * (x_[i]->Min() - x_[i]->OldMin()));
		// }
		// else {
		// 	y_ub.Add(solver(), wgt_[i] * (x_[i]->Min() - x_[i]->OldMin()));
		// 	y_lb.Add(solver(), wgt_[i] * (x_[i]->Max() - x_[i]->OldMax()));
		// }
		
		// FIXME for debug purpose
		// check value correctness
		double lb2 = bias_ * prec_, ub2 = bias_ * prec_;
		bool fixed_value = true;
		for (int i = 0; i < n_; ++i) {
			if (!x_[i]->Bound())
				fixed_value = false;
			// add the best case contribution of each variables (this depends
			// on the variable weight)
			if (wgt_[i] >= 0) {
				ub2 += wgt_[i] * x_[i]->Max();
				lb2 += wgt_[i] * x_[i]->Min();
			}
			else {
				ub2 += wgt_[i] * x_[i]->Min();
				lb2 += wgt_[i] * x_[i]->Max();
			}
		}
			
		// DBG(cerr << "y_ub = " << y_ub.Value() << endl;);
		// DBG(cerr << "y_lb = " << y_lb.Value() << endl;);
		DBG(cerr << "ub2 = " << ub2 << endl;);
		DBG(cerr << "lb2 = " << lb2 << endl;);
		// DBG(cerr << "ub ok: " << (abs(y_ub.Value() - ub2) <= 1/prec_) << endl;);
		// DBG(cerr << "lb ok: " << (abs(y_lb.Value() - lb2) <= 1/prec_) << endl;);
		// assert(y_ub.Value() >= ub2 - 1/prec_);
		// assert(y_lb.Value() <= lb2 + 1/prec_);
		
		// // prune the output variable
		// y_->SetMax(ROUND(y_ub.Value()));
		// y_->SetMin(ROUND(y_lb.Value()));
		
		// FIXME for debug purpose
		// prune using the alternative bounds
		y_->SetMax(ROUND(ub2));
		y_->SetMin(ROUND(lb2));
				
		DBG(cerr << "<< after: " << this << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true

#undef DEBUG_ON
#define DEBUG_ON false	
	void PropagateOut2In() {
		DBG(cerr << "RealWgtLinearCst::PropagateOut2In" << endl;);
		DBG(cerr << ">> before: " << this << endl;);
		
		// propagate the output max
		if (y_->Max() < ROUND(y_ub.Value())) {
			// compute the de-rounded bound
			double ymax = DE_ROUND_UP(y_->Max());
			DBG(cerr << "ymax: " << ymax << endl;);
			
			// the best case configuration is the one corresponding to the
			// output lower bound
			double delta = (ymax - y_lb.Value());
			DBG(cerr << "delta: " << delta << endl;);
			for (int i = 0; i < n_; ++i) {
				if (wgt_[i] >= 0) {
					// compute an upper bound on the variable
					double x_ub = delta / wgt_[i] + x_[i]->Min();
					DBG(cerr << "x_ub[" << i << "]: " << x_ub << endl;);
					x_[i]->SetMax(ceil(x_ub));
				}
				else {
					// compute a lower bound on the variable
					double x_lb = delta / wgt_[i] + x_[i]->Max();
					DBG(cerr << "x_lb[" << i << "]: " << x_lb << endl;);
					x_[i]->SetMin(floor(x_lb));
				}
			}
			
			DBG(cerr << "<< after max: " << this << endl;);
		}
		
		// propagate the output min
		if (y_->Min() > ROUND(y_lb.Value())) {
			// compute the de-rounded bound
			double ymin = DE_ROUND_DOWN(y_->Min());
			DBG(cerr << "ymin: " << ymin << endl;);
			
			// the best case configuration is the one corresponding to the
			// output lower bound
			double delta = (ymin - y_ub.Value());
			DBG(cerr << "delta: " << delta << endl;);
			for (int i = 0; i < n_; ++i) {
				if (wgt_[i] >= 0) {
					// compute an upper bound on the variable
					double x_lb = delta / wgt_[i] + x_[i]->Max();
					DBG(cerr << "x_lb[" << i << "]: " << x_lb << endl;);
					x_[i]->SetMin(floor(x_lb));
				}
				else {
					// compute a lower bound on the variable
					double x_ub = delta / wgt_[i] + x_[i]->Min();
					DBG(cerr << "x_ub[" << i << "]: " << x_ub << endl;);
					x_[i]->SetMax(ceil(x_ub));
				}
			}
			
			DBG(cerr << "<< after min: " << this << endl;);
		}
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// Initial propagation method
#undef DEBUG_ON
#define DEBUG_ON true
	void InitialPropagate() {
		DBG(cerr << "RealWgtLinearCst::InitialPropagate" << endl;);
		DBG(cerr << ">> before: " << this << endl;);

		// compute initial bounds
		double lb = bias_ * prec_, ub = bias_ * prec_;
		for (int i = 0; i < n_; ++i) {
			// add the best case contribution of each variables (this depends
			// on the variable weight)
			if (wgt_[i] >= 0) {
				ub += wgt_[i] * x_[i]->Max();
				lb += wgt_[i] * x_[i]->Min();
			}
			else {
				ub += wgt_[i] * x_[i]->Min();
				lb += wgt_[i] * x_[i]->Max();
			}
		}
		DBG(cerr << "ub = " << ub << endl;);
		DBG(cerr << "lb = " << lb << endl;);		
		
		// store the bounds
		y_ub.SetValue(solver(), ub);
		y_lb.SetValue(solver(), lb);
		
		DBG(cerr << "y_ub = " << y_ub.Value() << endl;);
		DBG(cerr << "y_lb = " << y_lb.Value() << endl;);
		
		// prune the output variable
		y_->SetMax(ROUND(y_ub.Value()));
		y_->SetMin(ROUND(y_lb.Value()));
		
		DBG(cerr << "<< after: " << this << endl;);
		
		// run backward propagation
		PropagateOut2In();
	}
#undef DEBUG_ON
#define DEBUG_ON true
		
	// 
	string DebugString() const {
		string out;
			
		// add the description of the output variable
		out.append(y_->DebugString());
		out.append(" = ");
		// bias of the output neuron activity
		StringAppendF(&out, "%g", bias_ * prec_);
		for (int i = 0; i < n_; ++i) {
			out.append("+");
			StringAppendF(&out, "%g", wgt_[i]);
			out.append("*");
			out.append(x_[i]->DebugString());
		}
		
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint("RealWgtLinearCst", this);
		visitor->VisitIntegerExpressionArgument(
			ModelVisitor::kExpressionArgument, y_);
        for(int i = 0; i < n_; ++i) {
    			visitor->VisitIntegerExpressionArgument(
					ModelVisitor::kExpressionArgument, x_[0]);
        }
		visitor->EndVisitConstraint("RealWgtLinearCst", this);
	}

};
#undef DEBUG_ON
#define DEBUG_ON true

Constraint* MakeRealWgtLinearCst(Solver* const s, 
							  int size,
							  const IntVar* const* inputs,
							  IntVar* const output,
							  double bias,
							  double const* weights,
							  int32 prec,
							  string name) {
  CHECK(inputs != NULL) << "NULL input array, maybe a bad cast";
  CHECK(weights != NULL) << "NULL weight array, maybe a bad cast";
  CHECK(output != NULL) << "NULL neuron output, maybe a bad cast";
  CHECK_EQ(s, output->solver());
  CHECK(prec > 0) << "invalid neuron precision";
  for (int i = 0; i < size; ++i) CHECK_EQ(s, inputs[i]->solver());
  return s->RevAlloc(new RealWgtLinearCst(s, size, inputs, output,
											bias, weights, prec, name));
}


Constraint* MakeRealWgtLinearCst(Solver* s,
							  const std::vector<IntVar*>& inputs,
							  IntVar* const output,
							  double bias,
							  const std::vector<double>& weights,
							  int32 prec,
							  string name) {
  return MakeRealWgtLinearCst(s, inputs.size(), inputs.data(), output,
							  bias, weights.data(), prec, name);
}

// //==============================================================================
// // Propagator enhancer for two layer FF networks with tansig hidden neurons
// //==============================================================================

// class Tansig2LFFPropEnhancer : public Constraint {

// public:
// 	// step update policies
// 	typedef enum {su_simple,
// 				  su_polyak_nonvanishing} step_update_policy;

// private:
// 	// internal copy of the input weight array (flattened)
// 	double* wgt0_;
// 	// internal copy of the hidden neuron weight array (flattened)
// 	double* wgt1_;
// 	// internal pointers to the input variables
// 	IntVar** x_;
// 	// internal pointers to the activity variables
// 	IntVar** y_;
// 	// internal pointer to the output activity variable
// 	IntVar* z_;
// 	// number of inputs
// 	int n_;
// 	// number of hidden neurons
// 	int m_;
// 	// precision (stored as a double, but this is in fact an integer)
// 	double prec_;
// 	// constraint name
// 	string name_;
	
// 	// current multiplier values
// 	NumericalRevArray<double> lambda_ub; // for the upper bound
// 	NumericalRevArray<double> lambda_lb; // for the lower bound
// 	// last bounds
// 	NumericalRev<double> z_ub;
// 	NumericalRev<double> z_lb;
	
// 	// subgradient optimization parameters
// 	double fixed_alpha = 0.5; // alpha coefficient in the deflection
// 	double original_beta = fixed_alpha; // basic step length
// 	double delta_star_mult = 0.01; // used to obtain the minimum delta value
// 	double delta_attenuation = 0.75; // attenuation factor for delta
// 	double delta_mult = 0.25; // used to reset delta in the polyak policy
// 	int max_it; // number of iterations to perform
// 	step_update_policy su_policy = su_polyak_nonvanishing; // step update pol.
	
// public:
// 	Tansig2LFFPropEnhancer(Solver* const s,
// 						 int isize,
// 						 int hsize,
// 						 const IntVar* const* x,
// 						 const IntVar* const* y,
// 						 IntVar* z,
// 						 double const* wgt0,
// 						 double const* wgt1,
// 						 int32 prec,
// 						 string name)
// 	: Constraint(s), n_(isize), m_(hsize),
// 		prec_(prec), name_(name), z_(z),
// 		lambda_ub(hsize, 0), lambda_lb(hsize, 0), z_ub(0), z_lb(0)
// 	{
// 		assert(isize > 0 && hsize > 0);
		
// 		// init array of input variables
// 		x_ = new IntVar*[n_];
// 		memcpy(x_, x, n_ * sizeof(*x));
// 		// init array of activity variables
// 		y_ = new IntVar*[m_];
// 		memcpy(y_, y, m_ * sizeof(*y));
		
// 		// init array of input weights
// 		wgt0_ = new double[(n_+1)*(m_)];
// 		memcpy(wgt0_, wgt0, (n_+1)*(m_) * sizeof(*wgt0));
// 		// init array of hidden neuron weights
// 		wgt1_ = new double[(m_+1)];
// 		memcpy(wgt1_, wgt1, (m_+1) * sizeof(*wgt1));
		
// 		// init the subgradient optimization parameters (all invalid)
// 		fixed_alpha = -1;
// 		original_beta = -1;
// 		delta_star_mult = -1; // for the polyak_nonvanishing policy
// 		delta_attenuation = -1; // for the polyak_nonvanishing policy
// 		delta_mult = -1; // used to reset delta in the polyak policy
// 		max_it = -1;
// 		su_policy = su_polyak_nonvanishing;
// 	}
	
// 	virtual ~Tansig2LFFPropEnhancer() {
// 		delete[] wgt0_;
// 		delete[] wgt1_;
// 		delete[] x_;
// 		delete[] y_;
// 	}
	
// 	// access methods for weights and bias (those are all inlined when compiler
// 	// optimizations are on)
// 	double getWgt0(int i, int j) const {
// 		return wgt0_[j * (n_+1) + 1 + i];
// 	}
// 	double getBias0(int j) const {
// 		return wgt0_[j * (n_+1)];
// 	}
// 	double getWgt1(int j) const {
// 		return wgt1_[1 + j];
// 	}
// 	double getBias1() const {
// 		return wgt1_[0];
// 	}
	
// 	/**
// 	 * @brief Post method; the constraint perform forward propagation only
// 	 */
// #undef DEBUG_ON
// #define DEBUG_ON false
// 	void Post() {
// 		DBG(cerr << "Tansig2LFFPropEnhancer::Post" << endl;);
// 		// Prepare the daemons to run the actual propagation routines
// 		Demon* propd0 = MakeDelayedConstraintDemon0(solver(), this,
// 			&Tansig2LFFPropEnhancer::PropagateIn2Out, "PropagateIn2Out");

// 		// post on all network inputs
// 		for(int i = 0; i < n_; ++i) {
// 			// post the forward propagation daemon (delayed)
// 			x_[i]->WhenRange(propd0);
// 		}
// 		// post on all network activities
// 		for(int j = 0; j < m_; ++j) {
// 			// post the forward propagation daemon (delayed)
// 			y_[j]->WhenRange(propd0);
// 		}
// 	}
// #undef DEBUG_ON
// #define DEBUG_ON true
	
// 	// ========================================
// 	// = Methods for subgradient optimization =
// 	// ========================================
	
// 	// data structure to store the state of the subgradient optimization
// 	typedef struct {
// 		// multpliers
// 		double* lambda;
// 		// problem coeffiecients (they depend on the multipliers)
// 		double zl;
// 		double* yl;
// 		double* xl;
// 		// current solution
// 		double z;
// 		double* x;
// 		double* y;
// 		// current subgradient
// 		double* lambda_d;
// 		// current search direction
// 		double* lambda_s;
// 		// current estimated optimality gap
// 		double delta;
// 		// current iteration number
// 		int it_num;
// 		// current basic step length
// 		double beta;
// 		// current deflection coefficient
// 		double alpha;
		
// 	} lagrangian_state;
	
	
// 	// METHODS TO SOLVE THE LAGRANGIAN RELAXATION PROBLEM
// 	// reduced cost used for the "a" subproblem
// 	// double modobj(double w, double yl, double y) {
// 	// 	return w * TANSIG(y) + yl * y;
// 	// }

// 	double modobj(double w, double yl, double y) {
// 		return w * ROUND(TANSIG(y)*prec_) / prec_ + yl * y;
// 	}


// 	// implementation of argmin/argmax for a custom function
// 	double pick_best(double w, double yl,
// 					 double y1, double y2, bool max_notmin) {
// 		if (modobj(w,yl,y1) >= modobj(w,yl,y2))
// 			return max_notmin ? y1 : y2;
// 		else
// 			return max_notmin ? y2 : y1;
// 	}
	
// #undef DEBUG_ON
// #define DEBUG_ON true
// 	void solve_relaxation(lagrangian_state& s, bool max_notmin) {
// 		DBG(cerr << "Tansig2LFFPropEnhancer::solve_relaxation" << endl;);
// 		// COMPUTE THE PROBLEM COEFFICIENTS
// 		// Compute fixed bound offset
// 		s.zl = getBias1();
// 		for (int j = 0; j < m_; ++j)
// 			s.zl += getBias0(j) * s.lambda[j];
	
// 		// Compute the coefficients of all the "y" variables
// 		for (int j = 0; j < m_; ++j)
// 			s.yl[j] = -s.lambda[j];
	
// 		// Compute the coefficients of all the "x" variables
// 		for (int i = 0; i < n_; ++i) {
// 			s.xl[i] = 0; // reset the coefficient
// 			for (int j = 0; j < m_; ++j)
// 				s.xl[i] += s.lambda[j] * getWgt0(i,j);
// 		}
		
// 		DBG(
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.zl = " << s.zl << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.yl = "; print_array(s.yl, m_); cerr << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.xl = "; print_array(s.xl, n_); cerr << endl;
// 		);
		
// 		// START THE BOUND COMPUTATION
// 		// start from the fixed offset
// 		double bound = s.zl;
// 		// DBG(
// 		// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 		// 	 << "partial bound = " << bound << endl;
// 		// );
		
// 		// add the contribution of the "y" subproblems
// 		for (int j = 0; j < m_; ++j) {
// 			// get minimum and maximum value of the j-th "y" variable
// 			double ymin = DE_ROUND_UP(y_[j]->Min()) / prec_;
// 			double ymax = DE_ROUND_DOWN(y_[j]->Max()) / prec_;
// 			// cache the weight and the "y" coefficient
// 			double w = getWgt1(j);
// 			double yl = s.yl[j];
// 			// DBG(
// 			// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			// 	 << "w = " << w << ", yl = " << yl << endl;
// 			// );
	
// 			// pick the best value among the extremes
// 			double yval = pick_best(w, yl, ymin, ymax, max_notmin);

// 			// compute the two solutions to the second-degree equation (both
// 			// the "a" and "c" terms in the classical formula are 1). The "b"
// 			// term is computed here.
// 			// NOTE: the equation has real valued solutions only if "yl" is
// 			// non-zero, "w" and "yl" are opposite in sign and "yl" it is not
// 			// greater than than "w" in absolute value
// 			if (yl != 0 &&  w/yl < -1) {
// 				double b = 2 + 4 * w / yl;
// 				// solve the second degree equation
// 				double u1 = 0.5*(-b - sqrt(b*b - 4));
// 				double u2 = 0.5*(-b + sqrt(b*b - 4));
// 				// translate back to y values
// 				double y1 = max(ymin,-0.5 * log(u2));
// 				double y2 = min(ymax,-0.5 * log(u1));
// 				// pick the best value
// 				yval = pick_best(w, yl, yval, y1, max_notmin);
// 				yval = pick_best(w, yl, yval, y2, max_notmin);
// 			}
			
// 			// store the "y" value corresponding to the bound
// 			s.y[j] = yval;
// 			// udpate the bound
// 			bound += modobj(w, yl, yval);
// 			// DBG(
// 			// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			// 	 << "partial bound = " << bound << endl;
// 			// );
// 		}
		
// 		// add the contribution of each "x" variable to the bound
// 		for (int i = 0; i < n_; ++i) {
// 			// If the goal is to compute an upper bound, then:
// 			// -- xl[i] >= 0 ---> choose the largest possible "x"
// 			// -- xl[i] <= 0 ---> choose the lowest possible "x"
// 			// Do the opposite if the goal is to compute a lower bound.
// 			if (s.xl[i] >= 0) {
// 				if (max_notmin) s.x[i] = x_[i]->Max() / prec_;
// 				else s.x[i] = x_[i]->Min() / prec_;
// 			}
// 			else {
// 				if (max_notmin) s.x[i] = x_[i]->Min() / prec_;
// 				else s.x[i] = x_[i]->Max() / prec_;
// 			}
// 			// update the bound
// 			bound += s.xl[i] * s.x[i];
// 		}
		
// 		// store the bound in the lagrangian state
// 		s.z = bound;

// 		DBG(
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.z = " << s.z << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.y = "; print_array(s.y, m_); cerr << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.x = "; print_array(s.x, n_); cerr << endl;
// 		);
// 	} 
// #undef DEBUG_ON
// #define DEBUG_ON true

// #undef DEBUG_ON
// #define DEBUG_ON true
// 	void update_multipliers(lagrangian_state& s,
// 						step_update_policy su_policy,
// 						bool max_notmin) { // "true" for UB, "false" for LB
// 		DBG(cerr << "Tansig2LFFPropEnhancer::update_multipliers" << endl;);
// 		DBG(
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.alpha = " << s.alpha << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.beta = " << s.beta << endl;
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 			 << "s.delta = " << s.delta << endl;
// 		);

		
// 		// check the validity of the parameters
// 		assert(s.beta >= 0);
// 		assert(s.alpha > 0);
// 		assert(s.alpha <= 1);
// 		assert(!(s.alpha < 1) || s.beta <= s.alpha);
// 		assert(s.delta >= 0);
					
// 		// COMPUTE THE CURRENT GRADIENT
// 		for (int j = 0; j < m_; ++j) {
// 			// add the constant contribution
// 			s.lambda_d[j] = getBias0(j);
// 			// add the contribution connected to the "x" variables
// 			for (int i = 0; i < n_; ++i)
// 				s.lambda_d[j] += getWgt0(i,j) * s.x[i];
// 			// add the contribution connected to the "y" variables
// 			s.lambda_d[j] += -s.y[j];
// 		}
		
// 		DBG(
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 		     <<"lambda_d = "; print_array(s.lambda_d, m_); cerr << endl;
// 		);
		
// 		// OBTAIN THE SEARCH DIRECTION
// 		// NOTE: if alpha = 1, this is just the gradient
// 		for (int j = 0; j < m_; ++j)
// 			s.lambda_s[j] = s.alpha * s.lambda_d[j]
// 							+ (1 - s.alpha) * s.lambda_s[j];
// 		DBG(
// 		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
// 		     <<"lambda_s = "; print_array(s.lambda_s, m_); cerr << endl;
// 		);
		
// 		// DEFINE THE STEP SIZE
// 		// compute the square L2-norm of the direction vector
// 		double sds = 0;
// 		for (int j = 0; j < m_; ++j)
// 			sds += s.lambda_s[j] * s.lambda_s[j];
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "sds = " << sds << endl;
// 		);
	
// 		// define the (basic) step size
// 		double step_size;
// 		double road;
// 		if (su_policy == su_simple) {
// 			step_size = s.beta / (s.it_num * sqrt(sds));
// 			road = s.beta / s.it_num;
// 		}		
// 		// in case the Polyak policy is used, modify the step length
// 		else if (su_policy == su_polyak_nonvanishing) {
// 			// compute the step size
// 			double estimated_gap = max_notmin
// 									? s.z - (z_ub.Value() - s.delta)
// 									: (z_lb.Value() + s.delta) - s.z; 		
// 			step_size = s.beta * estimated_gap / sds;
// 			road = step_size * sqrt(sds);
// 		}
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "step_size = " << step_size << endl;
// 		);
				
// 		// decide the update direction (counter gradient-wise to get an upper
// 		// bound, gradient-wise to get a lower bound)
// 		step_size *= (max_notmin ? -1 : +1);
	
// 		// UPDATE THE MULTIPLIERS
// 		for (int j = 0; j < m_; ++j)
// 			s.lambda[j] += step_size * s.lambda_s[j];
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "s.lambda = "; print_array(s.lambda, m_); cerr << endl;
// 		);
// 	}
// #undef DEBUG_ON
// #define DEBUG_ON true

// #undef DEBUG_ON
// #define DEBUG_ON false
// 	void solve_lagrangian_problem(
// 					bool max_notmin) { // "true" for UB, "false" for LB
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") " << "x_ = [";
// 		for (int i = 0; i < n_; ++i)
// 			cerr << (i ? ", " : "") << x_[i];
// 		cerr << "]" << endl;
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") " << "y_ = [";
// 		for (int j = 0; j < m_; ++j)
// 			cerr << (j ? ", " : "") << y_[j];
// 		cerr << "]" << endl;
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "z_ = " << z_ << endl;
// 		);
					
// 		// build a data structure to store the lagrangian optimization state
// 		lagrangian_state s;
// 		s.lambda = new double[m_];
// 		s.yl = new double[m_];
// 		s.xl = new double[n_];
// 		s.x = new double[n_];
// 		s.y = new double[m_];
// 		s.lambda_d = new double[m_];
// 		s.lambda_s = new double[m_];
							
// 		// start from the stored multiplier values
// 		for (int j = 0; j < m_; ++j)
// 			s.lambda[j] = max_notmin ? lambda_ub.Value(j) : lambda_lb.Value(j);
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "s.lambda = "; print_array(s.lambda, m_); cerr << endl;
// 		);
	
// 		// reset search direction
// 		for (int j = 0; j < m_; ++j) s.lambda_s[j] = 0;
// 		DBG(
// 		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 			 << "s.lambda_s = "; print_array(s.lambda_s, m_); cerr << endl;
// 		);
		
// 		// init some parameter in the lagrangian state
// 		s.alpha = fixed_alpha;
// 		s.it_num = 0;
// 		s.beta = original_beta;
// 		s.delta = -1; // this is an invalid value: the true dalta is initialized afterwards
		
// 		// delta_star value (this is initialized at the first iteration)
// 		double delta_star = -1;
		
// 		// start the subgradient optimization
// 		while (s.it_num <= max_it) {
// 			DBG(
// 			cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 				 << "SUBGRADIENT OPTIMIZATION IT. " << s.it_num << endl;
// 			);
				
// 			// SOLVE THE RELAXATION WITH THE CURRENT MULTIPLIERS
// 			solve_relaxation(s, max_notmin);
				
// 			// UPDATE THE BOUND AND RECORD IMPROVEMENT EVENTS
// 			// default values for the even flags
// 			bool bound_improved = false;
// 			bool polyak_nonvanishing_improvement = false;
			
// 			// if this is the first iteration, save whatever bound we obtained			
// 			if (s.it_num == 0) {
// 				DBG(
// 				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 					 << "first bound found" << endl;
// 				);

// 				// record events
// 				bound_improved = true;
// 				polyak_nonvanishing_improvement = true; 
			
// 				// adapt delta_star (only for polyak_nonvanishing)
// 				if (su_policy == su_polyak_nonvanishing) {
// 					// updating delta_star seems to be a very good idea
// 					delta_star = delta_star_mult * abs(s.z);
// 					DBG(
// 					cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 						 << "delta_star = " << delta_star << endl;
// 					);
// 				}
// 			}
// 			else {
// 				DBG(
// 				if (max_notmin)
// 					cerr << "(lr-max) z_ub = " << z_ub.Value() << endl;
// 				else 
// 					cerr << "(lr-min) z_ub = " << z_lb.Value() << endl;
// 				);
// 				// compute the improvement amount
// 				double improvement = max_notmin ? z_ub.Value() - s.z :
// 												  s.z - z_lb.Value();
// 				DBG(
// 				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 					 << "improvement = " << improvement << endl;
// 				);

// 			    // detect a bound improvement event
// 				if (improvement > 0)
// 					bound_improved = true;
// 				// detect a sufficient improvement for the polyak policy with
// 				// non-vanishing threshold
// 				if (improvement >= s.delta)
// 					polyak_nonvanishing_improvement = true;
// 			}
				
// 			// STORE THE LAGRANGIAN MULTIPLIERS AND THE SOLUTION
// 			// (in case of an improvement)
// 			if (bound_improved) {
// 				// store the current bound and multipliers
// 				DBG(
// 				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 					 << "store new bound" << endl;
// 				);

// 				if (max_notmin) {
// 					z_ub.SetValue(solver(), s.z);
// 					for (int j = 0; j < m_; ++j)
// 						lambda_ub.SetValue(solver(), j, s.lambda[j]);		
// 				}
// 				else {
// 					z_lb.SetValue(solver(), s.z);
// 					for (int j = 0; j < m_; ++j)
// 						lambda_lb.SetValue(solver(), j,  s.lambda[j]);
// 				}
// 			}
			
// 			if (s.it_num >= max_it) break;
			
// 			// UPDATE THE LAGRANGIAN MULTIPLIERS
// 			if (su_policy == su_polyak_nonvanishing) {
// 				// if the estimated bound has been improved, reset delta
// 				if (polyak_nonvanishing_improvement) {
// 					DBG(
// 					cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 						 << "polyak improvement" << endl;
// 					);

// 					double best_b = max_notmin ? z_ub.Value() : z_lb.Value();
// 					s.delta = max(delta_star, abs(best_b) * delta_mult);
// 				}
// 				// otherwise, reduce the delta value
// 				else {
// 					DBG(
// 					cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
// 						 << "no polyak improvement" << endl;
// 					);

// 					s.delta = max(delta_star, s.delta*delta_attenuation);
// 				}
// 			}
// 			// if non improving steps are accepted
// 			else if (su_policy == su_simple){
// 				// do nothing
// 			}
// 			else {
// 				cerr << "ERROR: unknown step update policy" << endl;
// 				exit(1);
// 			}
			
// 			// one more iteration
// 			s.it_num++;
		
// 			// update multipliers and travelled road
// 			update_multipliers(s, su_policy, max_notmin);
// 		}
		
// 		delete[] s.lambda;
// 		delete[] s.yl;
// 		delete[] s.xl;
// 		delete[] s.x;
// 		delete[] s.y;
// 		delete[] s.lambda_d;
// 		delete[] s.lambda_s;
// 	}
// #undef DEBUG_ON
// #define DEBUG_ON true
	
// 	// method for forward propagation
// #undef DEBUG_ON
// #define DEBUG_ON true
// 	void PropagateIn2Out() {
// 		DBG(cerr << "Tansig2LFFPropEnhancer::PropagateIn2Out" << endl;);
// 		DBG(cerr << ">> before: " << this << endl;);
		
// 		// configure the parameters for the subgradient optimization
// 		fixed_alpha = 0.5;
// 		original_beta = fixed_alpha;
// 		delta_star_mult = 0.01;
// 		delta_attenuation = 0.75;
// 		delta_mult = 0.05;
// 		max_it = 5;
// 		su_policy = su_polyak_nonvanishing;
		
// 		// solve the lagrangian problem to obtain an upper bound
// 		solve_lagrangian_problem(true);
		
// 		DBG(
// 		cerr << "z_ub = " << z_ub.Value() << endl;
// 		cerr << "lambda_ub = [";
// 		for (int j = 0; j < m_; ++j)
// 			cerr << (j ? ", " : "") << lambda_ub.Value(j);
// 		cerr << "]" << endl;
// 		);
		
// 		// prune the output variable
// 		z_->SetMax(ROUND(z_ub.Value() * prec_));

// 		// solve the lagrangian problem to obtain a lower bound		
// 		solve_lagrangian_problem(false);
		
// 		DBG(
// 		cerr << "z_lb = " << z_lb.Value() << endl;
// 		cerr << "lambda_lb = [";
// 		for (int j = 0; j < m_; ++j)
// 			cerr << (j ? ", " : "") << lambda_lb.Value(j);
// 		cerr << "]" << endl;
// 		);
		
// 		// prune the intut variable
// 		z_->SetMin(ROUND(z_lb.Value() * prec_));

// 		DBG(cerr << "<< after: " << this << endl;);
// 	}
// #undef DEBUG_ON
// #define DEBUG_ON true

// 	// Initial propagation method
// #undef DEBUG_ON
// #define DEBUG_ON true
// 	void InitialPropagate() {
// 		DBG(cerr << "Tansig2LFFPropEnhancer::InitialPropagate" << endl;);
// 		DBG(cerr << ">> before: " << this << endl;);
		
// 		// configure the parameters for the subgradient optimization
// 		fixed_alpha = 0.5;
// 		original_beta = fixed_alpha;
// 		delta_star_mult = 0.01;
// 		delta_attenuation = 0.75;
// 		delta_mult = 0.25;
// 		max_it = 100;
// 		su_policy = su_polyak_nonvanishing;
		
// 		// solve the lagrangian problem to obtain an upper bound
// 		solve_lagrangian_problem(true);
		
// 		DBG(
// 		cerr << "z_ub = " << z_ub.Value() << endl;
// 		cerr << "lambda_ub = [";
// 		for (int j = 0; j < m_; ++j)
// 			cerr << (j ? ", " : "") << lambda_ub.Value(j);
// 		cerr << "]" << endl;
// 		);
		
// 		// prune the output variable
// 		z_->SetMax(ROUND(z_ub.Value() * prec_));

// 		// solve the lagrangian problem to obtain a lower bound		
// 		solve_lagrangian_problem(false);
		
// 		DBG(
// 		cerr << "z_lb = " << z_lb.Value() << endl;
// 		cerr << "lambda_lb = [";
// 		for (int j = 0; j < m_; ++j)
// 			cerr << (j ? ", " : "") << lambda_lb.Value(j);
// 		cerr << "]" << endl;
// 		);
		
// 		// prune the output variable
// 		z_->SetMin(ROUND(z_lb.Value() * prec_));
		
// 		// cerr << "EARLY EXIT, FOR DEBUG PURPOSE!" << endl;
// 		// exit(1);
// 		DBG(cerr << "<< after: " << this << endl;);
// 	}
// #undef DEBUG_ON
// #define DEBUG_ON true
	
// 	const char* get_ntype_string(neuron_type t) const {
// 		switch(t) {
// 			case ntype_purelin:
// 				return "purelin";
// 			case ntype_tansig:
// 				return "tansig";
// 			case ntype_hardlim:
// 				return "hardlim";
// 			default:
// 				cerr << "ERROR: unknown neuron type" << endl;
// 				exit(1);
// 		}
// 	}
	
// 	// 
// 	string DebugString() const {
// 		string out;
		
// 		// add the description of the output variable
// 		out.append(z_->DebugString());
// 		out.append(" = ");
// 		// bias of the output neuron activity
// 		StringAppendF(&out, "%g", getBias1() * prec_);
// 		for (int j = 0; j < m_; ++j) {
// 			out.append("+");
// 			StringAppendF(&out, "%g", getWgt1(j));
// 			out.append("*");
// 			out.append("tansig");
// 			out.append("(");
// 			// append hidden neuron bias
// 			StringAppendF(&out, "%g", getBias0(j) * prec_);
// 			for (int i = 0; i < n_; ++i) {
// 				out.append(" + ");
// 				StringAppendF(&out, "%g", getWgt0(i, j));
// 				out.append("*");
// 				out.append(x_[i]->DebugString());
// 			}
// 			out.append(")");
// 		}
// 		out.append(")");
		
// 		return out;
// 	}
	
// 	// the accept method needs not to be virtual
// 	void Accept(ModelVisitor* const visitor) const {
// 		visitor->BeginVisitConstraint("2LNNCst", this);
// 		visitor->VisitIntegerExpressionArgument(
// 				ModelVisitor::kExpressionArgument,z_);			
//         for(int i = 0; i < n_; ++i) {
//     			visitor->VisitIntegerExpressionArgument(
// 					ModelVisitor::kExpressionArgument,
// 					x_[i]);
// 		}
//         for(int j = 0; j < m_; ++j) {
//     			visitor->VisitIntegerExpressionArgument(
// 					ModelVisitor::kExpressionArgument,
// 					y_[j]);
//         }
// 		visitor->EndVisitConstraint("2LNNCst", this);
// 	}

// };

// Constraint* MakeTansig2LFFPropEnhancer(Solver* const s,
// 						 int isize,
// 						 int hsize,
// 						 const IntVar* const* inputs,
// 						 const IntVar* const* hidden_activities,
// 						 IntVar* output_activity,
// 						 double const* wgt0,
// 						 double const* wgt1,
// 						 int32 prec,
// 						 string name) {
//   CHECK(inputs != NULL) << "NULL input array, maybe a bad cast";
//   CHECK(output_activity != NULL) << "NULL output activity, maybe a bad cast";
//   CHECK(hidden_activities != NULL) << "NULL hidden acts., maybe a bad cast";
//   CHECK(wgt0 != NULL) << "NULL input weight array, maybe a bad cast";
//   CHECK(wgt1 != NULL) << "NULL hidden weight array, maybe a bad cast";
//   CHECK(isize > 0) << "number of inputs";
//   CHECK(hsize > 0) << "number of hidden neurons";
//   CHECK(prec > 0) << "invalid neuron precision";
//   for (int i = 0; i < isize; ++i)
// 	  CHECK_EQ(s, inputs[i]->solver());
//   for (int j = 0; j < hsize; ++j)
// 	  CHECK_EQ(s, hidden_activities[j]->solver());
//   CHECK_EQ(s, output_activity->solver());
  
//   return s->RevAlloc(new Tansig2LFFPropEnhancer(s, isize, hsize,
//   						inputs, hidden_activities, output_activity,
// 						wgt0, wgt1,
// 						prec, name));
// }

// Constraint* MakeTansig2LFFPropEnhancer(Solver* s,
// 							  const std::vector<IntVar*>& inputs,
// 							  const std::vector<IntVar*>& hidden_activities,
// 							  IntVar* output_activity,
// 							  const std::vector<double>& wgt0,
// 							  const std::vector<double>& wgt1,
// 							  int32 prec,
// 							  string name) {
//   return MakeTansig2LFFPropEnhancer(s, inputs.size(), hidden_activities.size(),
//   							inputs.data(), hidden_activities.data(),
// 							output_activity,
// 							wgt0.data(), wgt1.data(),
// 							prec, name);
// }

// ========================================================================
// = Lagrangian based propagator for 2 layer networks with tansig neurons =
// ========================================================================

class Tansig2LFFCst : public Constraint {

public:
	// step update policies
	typedef enum {su_simple,
				  su_polyak_nonvanishing} step_update_policy;

private:
	// number of inputs
	int n_;
	// number of hidden neurons
	int m_;
	// precision (stored as a double, but this is in fact an integer)
	double prec_;
	// constraint name
	string name_;
	
	// internal copy of the input weight array (flattened)
	const std::vector<double> wgt0_;
	// internal copy of the hidden neuron weight array (flattened)
	const std::vector<double> wgt1_;

	// internal pointers to the input variables
	const std::vector<IntExpr*> x_;
	
	// internal pointer to the output variable
	IntExpr* out_;

	// tighest z bounds (standard propagation)
	NumericalRev<double> z_ub;
	NumericalRev<double> z_lb;
	// tighest activity bounds
	NumericalRevArray<double> y_ub;
	NumericalRevArray<double> y_lb;
	// tighest x bounds (standard propagation)
	NumericalRevArray<double> x_ub;
	NumericalRevArray<double> x_lb;
	
	// z bounds (for incremental propagation -- standard approach)
	NumericalRev<double> z_ub_inc;
	NumericalRev<double> z_lb_inc;
	// activity bounds (for incremental propagation -- standard approach)
	NumericalRevArray<double> y_ub_inc;
	NumericalRevArray<double> y_lb_inc;
	
	// lagrangian z bounds
	NumericalRev<double> z_ubl;
	NumericalRev<double> z_lbl;
	
	// flag to record if lagrangian y->z propagation must be performed
	Rev<bool> trigger_y2z_lag;
	
	// activation counter
	Rev<int64> act_cnt;
	
	// current multiplier values (for incremental propagation)
	NumericalRevArray<double> lambda_ub; // for the upper bound
	NumericalRevArray<double> lambda_lb; // for the lower bound
	
	// subgradient optimization parameters
	double fixed_alpha; //= 0.5; // alpha coefficient in the deflection
	double original_beta; // = fixed_alpha; // basic step length
	double delta_star_mult; // = 0.01; // used to obtain the minimum delta value
	double delta_attenuation; // = 0.75; // attenuation factor for delta
	double delta_mult; // = 0.25; // used to reset delta in the polyak policy
	int max_it; // number of iterations to perform
	step_update_policy su_policy; // = su_polyak_nonvanishing; // step update pol.
	int max_nbad; // = -1; // maximum number of non-improving iterations before 
					   // randomizing the multipliers (-1 = no randomization)
	
	// data structure to store the state of the subgradient optimization
	typedef struct {
		// multpliers
		double* lambda;
		// problem coeffiecients (they depend on the multipliers)
		double zl;
		double* yl;
		double* xl;
		// current solution
		double z;
		double* x;
		double* y;
		// current subgradient
		double* lambda_d;
		// current search direction
		double* lambda_s;
		// current estimated optimality gap
		double delta;
		// current iteration number
		int it_num;
		// current basic step length
		double beta;
		// current deflection coefficient
		double alpha;
	} lagrangian_state;
	
	// state data for the solution of the lagrangian problem
	// build a data structure to store the lagrangian optimization state
	// WARNING: the data in this structure is not reversible!
	lagrangian_state s_;
	
	// number of subgradient iterations since the last bound improvement
	NumericalRev<int64> nbad_lb;
	NumericalRev<int64> nbad_ub;
	
	// for development purpose: operation mode
	// mode_ = 0: standard propagation only, incremental (BUGGY!)
	// mode_ = 1: standard propagation only, non incremental
	// mode_ = 2: standard (non-inc) & lagrangian propagation (z only)
	// mode_ = 3: standard (non-inc) & lagrangian propagation (x & z)
	int64 mode_;
	
	// counter to collect statistics
	int64 cnt_l_act;
	int64 cnt_l_filter;
	int64 cnt_lx_filter;
	
public:
	Tansig2LFFCst(Solver* const s,
						 const std::vector<IntExpr*>& x,
						 IntExpr* out,
						 const std::vector<double>& wgt0,
						 const std::vector<double>& wgt1,
						 int32 prec,
						 int64 mode)
	: Constraint(s), n_(x.size()), m_(wgt0.size() / (n_+1)),
		prec_(prec), name_("Tansig2LFFCst"),
		x_(x), out_(out),
		x_ub(n_, DOUBLE_MAX), x_lb(n_, DOUBLE_MIN),
		y_ub(m_, DOUBLE_MAX), y_lb(m_, DOUBLE_MIN),
		z_ub(DOUBLE_MAX), z_lb(DOUBLE_MIN),
		y_ub_inc(m_, DOUBLE_MAX), y_lb_inc(m_, DOUBLE_MIN),
		z_ub_inc(DOUBLE_MAX), z_lb_inc(DOUBLE_MIN),
		z_ubl(DOUBLE_MAX), z_lbl(DOUBLE_MIN),
		trigger_y2z_lag(false), act_cnt(0),
		lambda_ub(m_, 0), lambda_lb(m_, 0),
		wgt0_(wgt0), wgt1_(wgt1),
		mode_(mode),
		nbad_lb(0), nbad_ub(0),
		cnt_l_act(0),
		cnt_l_filter(0),
		cnt_lx_filter(0)
	{	
		assert(mode_ >= 0 && mode_ <= 3);
		// cerr << "mode_ = " << mode_ << endl;
		
		// cerr << "n_ = " << n_ << endl;
		// cerr << "m_ = " << m_ << endl;
		//
		// cerr << "x_lb = [";
		// for (int i = 0; i < n_; ++i) cerr << " " << x_lb.Value(i);
		// cerr << " ]" << endl;
		// cerr << "x_ub = [";
		// for (int i = 0; i < n_; ++i) cerr << " " << x_ub.Value(i);
		// cerr << " ]" << endl;
		//
		// cerr << "z_lb = " << z_lb.Value() << endl;
		// cerr << "z_ub = " << z_ub.Value() << endl;
		//
		// cerr << "y_lb = [";
		// for (int j = 0; j < m_; ++j) cerr << " " << y_lb.Value(j);
		// cerr << " ]" << endl;
		// cerr << "y_ub = [";
		// for (int j = 0; j < m_; ++j) cerr << " " << y_lb.Value(j);
		// cerr << " ]" << endl;
		//
		// exit(1);

		
		// init the subgradient optimization parameters (all invalid)
		fixed_alpha = -1;
		original_beta = -1;
		delta_star_mult = -1; // for the polyak_nonvanishing policy
		delta_attenuation = -1; // for the polyak_nonvanishing policy
		delta_mult = -1; // used to reset delta in the polyak policy
		max_it = -1;
		su_policy = su_polyak_nonvanishing;
		
		// init the lagrangian state
		s_.lambda = new double[m_];
		s_.yl = new double[m_];
		s_.xl = new double[n_];
		s_.x = new double[n_];
		s_.y = new double[m_];
		s_.lambda_d = new double[m_];
		s_.lambda_s = new double[m_];
	}
	
	virtual ~Tansig2LFFCst() {
		// tear down the lagrangian state
		delete[] s_.lambda;
		delete[] s_.yl;
		delete[] s_.xl;
		delete[] s_.x;
		delete[] s_.y;
		delete[] s_.lambda_d;
		delete[] s_.lambda_s;
		
		// print some statistics
		cout << "- cnt_l_act = " << cnt_l_act << endl;
		cout << "- cnt_l_filter = " << cnt_l_filter << endl;
		cout << "- cnt_lx_filter = " << cnt_lx_filter << endl;
		
		// make a call to the parent destructor
		// Constraint::~Constraint();
	}
	
	// access methods for weights and bias (those are all inlined when compiler
	// optimizations are on)
	double getWgt0(int i, int j) const {
		return wgt0_[j * (n_+1) + 1 + i];
	}
	double getBias0(int j) const {
		return wgt0_[j * (n_+1)];
	}
	double getWgt1(int j) const {
		return wgt1_[1 + j];
	}
	double getBias1() const {
		return wgt1_[0];
	}
	
#undef DEBUG_ON
#define DEBUG_ON true
	void Post() {
		DBG(cerr << ">> Tansig2LFFCst::Post" << endl;);
		// register the daemons to perform the standard Neuron Constraint
		// propagation (incremental version)
		if (mode_ >= 1) {
			out_->WhenRange(MakeConstraintDemon0(solver(), this,
												 &Tansig2LFFCst::std_z2x,
												 "std_z2x"));
			for (int i = 0; i < n_; ++i) {
				x_[i]->WhenRange(MakeConstraintDemon1(solver(), this,
													  &Tansig2LFFCst::std_x2z,
													 "std_x2z", i));
			}
		}
		else {
			// non incremental version
			out_->WhenRange(MakeConstraintDemon0(solver(), this,
												 &Tansig2LFFCst::std_z2x_noinc,
												 "std_z2x"));
			for (int i = 0; i < n_; ++i) {
				x_[i]->WhenRange(MakeConstraintDemon0(solver(), this,
													  &Tansig2LFFCst::std_x2z_noinc,
													 "std_x2z"));
			}
		}
		
		// Lagrangian propagation
		if (mode_ >= 2) {
			// Prepare the daemons to run the actual propagation routines
			Demon* propd0 = MakeDelayedConstraintDemon1(solver(), this,
				&Tansig2LFFCst::lag_x2z, "lag_x2z", false);
			Demon* propd1 = MakeDelayedConstraintDemon0(solver(), this,
				&Tansig2LFFCst::lag_z2x, "lag_z2x");

			// post on all network inputs
			for(int i = 0; i < n_; ++i) {
				// post the forward propagation daemon (delayed)
				x_[i]->WhenRange(propd0);
			}
			// post on the network output (delayed)
			out_->WhenRange(propd1);			
		}

		DBG(cerr << "<< Tansig2LFFCst::Post" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true	
	
	// ===================================================================
	// = Methods for "standard" Neuron Cst propagation (non-incremental) =
	// ===================================================================
		
	// compute z bounds by reasoning on the y bounds (non incremental)
	void std_y2z_noinc() {
		DBG(cerr << ">> Tansig2LFFCst::std_y2z_noinc" << endl;);
		DBG(cerr << this << endl;);
		
		double z_lb_loc = getBias1(), z_ub_loc = getBias1();
		// loop over all ys
		for (int j = 0; j < m_; ++j) {
			// add the contribution of a single y
			double w = getWgt1(j);
			if (w >= 0) {
				z_lb_loc += w * TANSIG(y_lb.Value(j));
				z_ub_loc += w * TANSIG(y_ub.Value(j));
			}
			else {
				z_lb_loc += w * TANSIG(y_ub.Value(j));
				z_ub_loc += w * TANSIG(y_lb.Value(j));
			}
		}
		// update the incremental bounds
		z_lb_inc.SetValue(solver(), z_lb_loc);
		z_ub_inc.SetValue(solver(), z_ub_loc);
		// update the tighest bounds
		z_lb.SetValue(solver(), max(z_lb_loc, z_lb.Value()));
		z_ub.SetValue(solver(), min(z_ub_loc, z_ub.Value()));
		DBG(cerr << "z = [" << z_lb.Value() << "," << z_ub.Value() << "]" << endl;);
		// prune the out variable
		int64 out_lb = ROUND(TANSIG(z_lb.Value()) * prec_);
		int64 out_ub = ROUND(TANSIG(z_ub.Value()) * prec_);
		out_->SetMin(out_lb);
		out_->SetMax(out_ub);

		DBG(cerr << this << endl;);		
		DBG(cerr << "<< Tansig2LFFCst::std_y2z_noinc" << endl;);
	}
	
	// compute y bounds by reasoning on the x bounds (non incremental)
	void std_x2z_noinc() {
		DBG(cerr << ">> Tansig2LFFCst::std_x2z_noinc" << endl;);
		DBG(cerr << this << endl;);
		
		DBG(cerr << "x bounds:";
			for (int i = 0; i < n_; ++i)
				cerr << " [" << x_lb.Value(i) << ", " << x_ub.Value(i) << "]";
			cerr << endl;);
			
		// update all x bounds (and check for modifications)
		bool modified = false;
		for (int i = 0; i < n_; ++i) {
			if ((x_[i]->Min() / prec_) > x_lb.Value(i)) {
				x_lb.SetValue(solver(), i, x_[i]->Min() / prec_);
				modified = true;
			}
			if ((x_[i]->Max() / prec_) < x_ub.Value(i)) {
				x_ub.SetValue(solver(), i, x_[i]->Max() / prec_);
				modified = true;
			}
		}
		
		DBG(cerr << "x bounds:";
			for (int i = 0; i < n_; ++i)
				cerr << " [" << x_lb.Value(i) << ", " << x_ub.Value(i) << "]";
			cerr << endl;);
		
		// if no (tighest) bound was modified, skip the method
		if (!modified) {
			DBG(cerr << "<< Tansig2LFFCst::std_x2z_noinc [early]" << endl;)
			return;
		}
		
		// loop over all ys
		modified = false;
		for (int j = 0; j < m_; ++j) {
			// compute upper and lower bounds
			double y_lb_loc = getBias0(j), y_ub_loc = getBias0(j);
			for (int i = 0; i < n_; ++i) {
				// add the contribution of a single x
				double w = getWgt0(i, j);
				if (w >= 0) {
					y_lb_loc += w * x_lb.Value(i);
					y_ub_loc += w * x_ub.Value(i);
				}
				else {
					y_lb_loc += w * x_ub.Value(i);
					y_ub_loc += w * x_lb.Value(i);
				}
			}
			// update the icremental bounds
			y_lb_inc.SetValue(solver(), j, y_lb_loc);
			y_ub_inc.SetValue(solver(), j, y_ub_loc);
			// update tightest bounds
			if (y_lb_loc > y_lb.Value(j)) {
				y_lb.SetValue(solver(), j, y_lb_loc);
				modified = true;
			}
			if (y_ub_loc < y_ub.Value(j)) {
				y_ub.SetValue(solver(), j, y_ub_loc);
				modified = true;
			}
			DBG(cerr << "y[" << j << "] = [" << y_lb.Value(j) << "," << y_ub.Value(j) << "]" << endl;);
		}
		// update z bounds
		if (modified) {
			// propagate y bounds to z bounds
			std_y2z_noinc();
		}
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_x2z_noinc" << endl;);
	}
	
	// compute x bounds by reasoning on the y bounds (non-incremental)
	void std_y2x_noinc(int j) {
		DBG(cerr << ">> Tansig2LFFCst::std_y2x_noinc" << endl;);
		DBG(cerr << this << endl;);
		
		DBG(
		for(int i = 0; i < n_; ++i)
			cerr << "x[" << i << "] = [" << x_lb.Value(i) << "," << x_ub.Value(i) << "]" << endl;
		cerr << "y[" << j << "] = [" << y_lb.Value(j) << "," << y_ub.Value(j) << "]" << endl;
		)
		
		// compute the current y bound based on x
		double y_sum_lb = getBias0(j), y_sum_ub = getBias0(j);
		for (int i = 0; i < n_; ++i) {
			double w = getWgt0(i, j);
			if (w >= 0) {
				y_sum_lb += w * x_lb.Value(i);
				y_sum_ub += w * x_ub.Value(i);
			}
			else {
				y_sum_lb += w * x_ub.Value(i);
				y_sum_ub += w * x_lb.Value(i);
			}
		}
		
		DBG(cerr << "y_sum_lb = " << y_sum_lb << endl;)
		DBG(cerr << "y_sum_ub = " << y_sum_ub << endl;)
		
		// loop over all x
		for (int i = 0; i < n_; ++i) {
			// compute the residuals
			double res_min = 0, res_max = 0, w = getWgt0(i, j);
			if (w >= 0) {
 				res_min = y_lb.Value(j) - (y_sum_ub - w * x_ub.Value(i));
				res_max = y_ub.Value(j) - (y_sum_lb - w * x_lb.Value(i));
			}
			else {
				res_min = y_lb.Value(j) - (y_sum_ub - w * x_lb.Value(i));
				res_max = y_ub.Value(j) - (y_sum_lb - w * x_ub.Value(i));
			}
			// compute the bounds
			double x_lb_loc = (w >= 0) ? res_min /w : res_max / w;
			double x_ub_loc = (w >= 0) ? res_max /w : res_min / w;
			// apply rounding
			int64 x_var_lb = safe_ceil(x_lb_loc * prec_);
			int64 x_var_ub = safe_floor(x_ub_loc * prec_);
			// update the bounds
			if (x_var_lb/prec_ > x_lb.Value(i))
				x_lb.SetValue(solver(), i, x_var_lb/prec_);
			if (x_var_ub/prec_ < x_ub.Value(i))
				x_ub.SetValue(solver(), i, x_var_ub/prec_);
			DBG(cerr << "x[" << i << "] = [" << x_lb.Value(i) << "," << x_ub.Value(i) << "]" << endl;);
			// prune the x variable
			x_[i]->SetMin(x_var_lb);
			x_[i]->SetMax(x_var_ub);
		}
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_y2x_noinc" << endl;)
	}
		
	double invtansig_ub(double v) {
		return (v >= 1) ? NN_INFINITY : (v <= -1) ? INV_LB : INVTANSIG(v);
	}
	
	double invtansig_lb(double v) {
		return (v >= 1) ? INV_UB : (v <= -1) ? -NN_INFINITY : INVTANSIG(v);
	}
	
	// compute x bounds by reasoning on the z bounds (non incremental)
	void std_z2x_noinc() {
		DBG(cerr << ">> Tansig2LFFCst::std_z2x_noinc" << endl;);
		DBG(cerr << this << endl;);
		
		// obtain starting z bounds
		double z_ub_loc = invtansig_ub(DE_ROUND_UP(out_->Max()) / prec_);
		double z_lb_loc = invtansig_lb(DE_ROUND_DOWN(out_->Min()) / prec_);
		
		// update the tightest bounds on z
		DBG(cerr << "z bounds = [" << z_lb.Value() << ", " << z_ub.Value() << "]" << endl;)
		bool modified = false;
		double old_lb = z_lb.Value(), old_ub = z_ub.Value();
		if (z_lb_loc > z_lb.Value()) {
			z_lb.SetValue(solver(), z_lb_loc);
			modified = true;
		}
		if (z_ub_loc < z_ub.Value()) {
			z_ub.SetValue(solver(), z_ub_loc);
			modified = true;
		}
		DBG(cerr << "z bounds = [" << z_lb.Value() << ", " << z_ub.Value() << "]" << endl;)
		
		// if no bound was modified, this method can be skipped completely
		if (!modified) {
			DBG(cerr << "<< Tansig2LFFCst::std_z2x_noinc [early]" << endl;)
			return;
		}
		
		DBG(
		for(int i = 0; i < n_; ++i)
			cerr << "x[" << i << "] = [" << x_lb.Value(i) << "," << x_ub.Value(i) << "]" << endl;
		)
		
		// compute the "forward" bounds for z (due to the current domain of the
		// y "variables")
		double z_sum_lb = getBias1(), z_sum_ub = getBias1();
		for (int j = 0; j < m_; ++j) {
			double w = getWgt1(j);
			if (w >= 0) {
				z_sum_lb += w * TANSIG(y_lb.Value(j));
				z_sum_ub += w * TANSIG(y_ub.Value(j));
			}
			else {
				z_sum_lb += w * TANSIG(y_ub.Value(j));
				z_sum_ub += w * TANSIG(y_lb.Value(j));
			}
		}
		
		// loop over all y variables
		for (int j = 0; j < m_; ++j) {
			// compute min/max residual
			double res_min = 0, res_max = 0, w = getWgt1(j);
			if (w >= 0) {
				res_min = z_lb.Value() - (z_sum_ub - w * TANSIG(y_ub.Value(j)));
				res_max = z_ub.Value() - (z_sum_lb - w * TANSIG(y_lb.Value(j)));
			}
			else {
				res_min = z_lb.Value() - (z_sum_ub - w * TANSIG(y_lb.Value(j)));
				res_max = z_ub.Value() - (z_sum_lb - w * TANSIG(y_ub.Value(j)));
			}
			// compute bounds
			double y_lb_loc = invtansig_lb((w >= 0) ? res_min / w : res_max / w);
			double y_ub_loc = invtansig_ub((w >= 0) ? res_max / w : res_min / w);
			// update the bounds
			bool modified = false;
			if (y_lb_loc > y_lb.Value(j)) {
				y_lb.SetValue(solver(), j, y_lb_loc);
				modified = true;
			}
			if (y_ub_loc < y_ub.Value(j)) {
				y_ub.SetValue(solver(), j, y_ub_loc);
				modified = true;
			}
			DBG(cerr << "y[" << j << "] = [" << y_lb.Value(j) << "," << y_ub.Value(j) << "]" << endl;);
			// propagate the new bound on x
			if (modified) {
				// the lagrangian bound needs to be updated
				trigger_y2z_lag.SetValue(solver(), true);
				// trigger propagation on the next layer
				std_y2x_noinc(j);
			}
		}
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_z2x_noinc" << endl;)
	}
	
	// ===============================================================
	// = Methods for "standard" Neuron Cst propagation (incremental) =
	// ===============================================================
	
	// compute z bounds by reasoning on the y bounds (incremental method)
	void std_y2z(int j, double old_lb, double old_ub) {
		DBG(cerr << ">> Tansig2LFFCst::std_y2z" << endl;);
		DBG(cerr << this << endl;);
		
		// add the contribution of a single y
		double z_lb_delta = 0, z_ub_delta = 0;
		double w = getWgt1(j);
		if (w >= 0) {
			z_lb_delta = w * (TANSIG(y_lb.Value(j)) - TANSIG(old_lb));
			z_ub_delta = w * (TANSIG(y_ub.Value(j)) - TANSIG(old_ub));
		}
		else {
			z_lb_delta = w * (TANSIG(y_ub.Value(j)) - TANSIG(old_ub));
			z_ub_delta = w * (TANSIG(y_lb.Value(j)) - TANSIG(old_lb));
		}
		// compute the incremental bound
		double z_lb_loc = z_lb_inc.Value() + z_lb_delta;
		double z_ub_loc = z_ub_inc.Value() + z_ub_delta;
		
		DBG(cerr << "z_inc = ";);
		DBG(cerr << " [" << z_lb_inc.Value() << "," << z_ub_inc.Value() << "] -->";);
		DBG(cerr << " [" << z_lb_loc << "," << z_ub_loc << "]" << endl;);
		
		// update the incremental bound
		z_lb_inc.SetValue(solver(), z_lb_loc);
		z_ub_inc.SetValue(solver(), z_ub_loc);
		// update the tightest bounds and prune the output variable
		DBG(cerr << "z = ";);
		DBG(cerr << " [" << z_lb.Value() << "," << z_ub.Value() << "] -->";);
		if (z_lb_loc > z_lb.Value()) {
			z_lb.SetValue(solver(), z_lb_loc);
			int64 out_lb = ROUND(TANSIG(z_lb.Value()) * prec_);
			out_->SetMin(out_lb);
		}
		if (z_ub_loc < z_ub.Value()) {
			z_ub.SetValue(solver(), z_ub_loc);
			int64 out_ub = ROUND(TANSIG(z_ub.Value()) * prec_);
			out_->SetMax(out_ub);
		}
		DBG(cerr << " [" << z_lb.Value() << "," << z_ub.Value() << "]" << endl;);

		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_y2z" << endl;);
	}
	
	// compute y bounds by reasoning on the x bounds (incremental method)
	void std_x2z(int i) {
		DBG(cerr << ">> Tansig2LFFCst::std_x2z" << endl;);
		DBG(cerr << this << endl;);
		
		// store old bounds used for propagation
		double old_lb = x_lb.Value(i), old_ub = x_ub.Value(i);
		bool modified = false;
		if ((x_[i]->Min() / prec_) > x_lb.Value(i)) {
			x_lb.SetValue(solver(), i, x_[i]->Min() / prec_);
			modified = true;
		}
		if ((x_[i]->Max() / prec_) < x_ub.Value(i)) {
			x_ub.SetValue(solver(), i, x_[i]->Max() / prec_);
			modified = true;
		}

		DBG(cerr << "x[" << i << "] = ";);
		DBG(cerr << " [" << old_lb << "," << old_ub << "] -->";);
		DBG(cerr << " [" << x_lb.Value(i) << "," << x_ub.Value(i) << "]" << endl;);
		
		// if the x bound was not modified, skip this method
		if (!modified) {
			DBG(cerr << "<< Tansig2LFFCst::std_x2z [early]" << endl;)
			return;
		}
		
		// loop over all ys
		for (int j = 0; j < m_; ++j) {
			// add the contribution of the current x variable
			double y_lb_delta = 0, y_ub_delta = 0;
			double w = getWgt0(i, j);
			if (w >= 0) {
				y_lb_delta = w * (x_lb.Value(i) - old_lb);
				y_ub_delta = w * (x_ub.Value(i) - old_ub);
			}
			else {
				y_lb_delta = w * (x_ub.Value(i) - old_ub);
				y_ub_delta = w * (x_lb.Value(i) - old_lb);
			}			
			// compute the bounds
			double y_lb_loc = y_lb_inc.Value(j) + y_lb_delta;
			double y_ub_loc = y_ub_inc.Value(j) + y_ub_delta;
			
			DBG(cerr << "y_inc[" << j << "] = ";);
			DBG(cerr << " [" << y_lb_inc.Value(j) << "," << y_ub_inc.Value(j) << "] -->";);
			DBG(cerr << " [" << y_lb_loc << "," << y_ub_loc << "]" << endl;);
			
			// update the incremental bounds
			y_lb_inc.SetValue(solver(), j, y_lb_loc);
			y_ub_inc.SetValue(solver(), j, y_ub_loc);
			// update tighest bounds
			double old_y_lb = y_lb.Value(j);
			double old_y_ub = y_ub.Value(j);
			bool modified = false;
			if (y_lb_loc > old_y_lb) {
				y_lb.SetValue(solver(), j, y_lb_loc);
				modified = true;
			}
			if (y_ub_loc < old_y_ub) {
				y_ub.SetValue(solver(), j, y_ub_loc);
				modified = true;
			}
			
			DBG(cerr << "y[" << j << "] = ";);
			DBG(cerr << " [" << old_y_lb << "," << old_y_ub << "] -->";);
			DBG(cerr << " [" << y_lb.Value(j) << "," << y_ub.Value(j) << "]" << endl;);
			
			// propagate
			if (modified)
				std_y2z(j, old_y_lb, old_y_ub);
		}

		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_x2z" << endl;);
	}
	
	double safe_ceil(double v) {
		double cv = ceil(v);
		if (fabs(cv-1 - v) < NN_EPSILON/prec_) return cv-1;
		return cv;
	}
	
	double safe_floor(double v) {
		double fv = floor(v);
		if (fabs(fv+1 - v) < NN_EPSILON/prec_) return fv+1;
		return fv;
	}
	
	// compute x bounds by reasoning on the y bounds
	void std_y2x(int j, double old_lb, double old_ub,
						vector<int64>& x_t_lb, vector<int64>& x_t_ub) {
		DBG(cerr << ">> Tansig2LFFCst::std_y2x" << endl;);
		DBG(cerr << this << endl;);
		
		// loop over all x
		for (int i = 0; i < n_; ++i) {
			// compute the residuals
			double res_min = 0, res_max = 0, w = getWgt0(i, j);
			if (w >= 0) {
 				res_min = y_lb.Value(j) - (y_ub_inc.Value(j) - w * x_ub.Value(i));
				res_max = y_ub.Value(j) - (y_lb_inc.Value(j) - w * x_lb.Value(i));
			}
			else {
				res_min = y_lb.Value(j) - (y_ub_inc.Value(j) - w * x_lb.Value(i));
				res_max = y_ub.Value(j) - (y_lb_inc.Value(j) - w * x_ub.Value(i));
			}
			// compute the bounds
			double x_lb_loc = (w >= 0) ? res_min / w : res_max / w;
			double x_ub_loc = (w >= 0) ? res_max / w : res_min / w;
			// apply rounding
			int64 x_var_lb = safe_ceil(x_lb_loc * prec_);
			int64 x_var_ub = safe_floor(x_ub_loc * prec_);
			// update the temporary bounds
			x_t_lb[i] = max(x_t_lb[i], x_var_lb);
			x_t_ub[i] = min(x_t_ub[i], x_var_ub);
		}
		
		DBG(cerr << "temp x bounds:";
			for (int i = 0; i < n_; ++i)
				cerr << " [" << x_t_lb[i]/prec_ << ", " << x_t_ub[i]/prec_ << "]";
			cerr << endl;);
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_y2x" << endl;);
	}
	
	// compute y bounds by reasoning on the z bounds
	void std_z2x() {
		DBG(cerr << ">> Tansig2LFFCst::std_z2x" << endl;);
		DBG(cerr << this << endl;);
		
		// obtain starting z bounds
		double z_ub_loc = invtansig_ub(DE_ROUND_UP(out_->Max()) / prec_);
		double z_lb_loc = invtansig_lb(DE_ROUND_DOWN(out_->Min()) / prec_);
		
		// update the tightest bounds on z
		bool modified = false;
		if (z_lb_loc > z_lb.Value()) {
			z_lb.SetValue(solver(), z_lb_loc);
			modified = true;
		}
		if (z_ub_loc < z_ub.Value()) {
			z_ub.SetValue(solver(), z_ub_loc);
			modified = true;
		}
		
		// if no bound was modified, this method can be skipped completely
		if (!modified) {
			DBG(cerr << "<< Tansig2LFFCst::std_z2x [early]" << endl;)
			return;
		}
		
		DBG(cerr << "z = [" << z_lb.Value() << "," << z_ub.Value() << "]" << endl;);
		DBG(cerr << "z_inc = [" << z_lb_inc.Value() << "," << z_ub_inc.Value() << "]" << endl;);
		
		// prepare a vector of temporary bounds for the x variables
		vector<int64> x_t_lb(n_, INT64_MIN);
		vector<int64> x_t_ub(n_, INT64_MAX);
		
		// this flag is raised if any y variable is modified
		bool modified2 = false;
		
		// loop over all y variables
		for (int j = 0; j < m_; ++j) {
			// compute min/max residual
			double res_min = 0, res_max = 0, w = getWgt1(j);
			if (w >= 0) {
				res_min = z_lb.Value() - (z_ub_inc.Value() - w * TANSIG(y_ub.Value(j)));
				res_max = z_ub.Value() - (z_lb_inc.Value() - w * TANSIG(y_lb.Value(j)));
			}
			else {
				res_min = z_lb.Value() - (z_ub_inc.Value() - w * TANSIG(y_lb.Value(j)));
				res_max = z_ub.Value() - (z_lb_inc.Value() - w * TANSIG(y_ub.Value(j)));
			}
			// compute bounds
			double y_lb_loc = invtansig_lb((w >= 0) ? res_min / w : res_max / w);
			double y_ub_loc = invtansig_ub((w >= 0) ? res_max / w : res_min / w);
			// update the bounds
			modified = false;
			double old_y_lb = y_lb.Value(j);
			double old_y_ub = y_ub.Value(j);
			if (y_lb_loc > old_y_lb) {
				y_lb.SetValue(solver(), j, y_lb_loc);
				modified = true;
			}
			if (y_ub_loc < old_y_ub) {
				y_ub.SetValue(solver(), j, y_ub_loc);
				modified = true;
			}
			DBG(cerr << "y[" << j << "] = ";);
			DBG(cerr << " [" << old_y_lb << "," << old_y_ub << "] -->";);
			DBG(cerr << " [" << y_lb.Value(j) << "," << y_ub.Value(j) << "]" << endl;);
			// keep track if at least one y bound was modified
			modified2 |= modified;
			
			if (modified) {
				// the lagrangian z bounds need to be updated
				trigger_y2z_lag.SetValue(solver(), true);
				// trigger an update of the incremental bounds for z
				std_y2z(j, old_y_lb, old_y_ub);
				// update the x bounds (without modifying the actual x
				// variables, lest the incremental computation for other y
				// values gets broken)
				std_y2x(j, old_y_lb, old_y_ub, x_t_lb, x_t_ub);
			}
		}
		
		// enforce the temporary bounds (if any y bound was modified)
		if (modified2) {
			for (int i = 0; i < n_; ++i) {
				x_[i]->SetMin(x_t_lb[i]);
				x_[i]->SetMax(x_t_ub[i]);
			}
		}
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::std_z2x" << endl;);
	}
	
	// ========================================
	// = Methods for subgradient optimization =
	// ========================================
	
	// ===============================================================
	// = METHOD TO PRUNE THE X VARIABLES BASED ON THE LAG RELAXATION =
	// ===============================================================

#undef DEBUG_ON
#define DEBUG_ON false
	void lag_x_pruning(bool max_notmin) {
		DBG(cerr << ">> Tansig2LFFCst::lag_x_pruning" << endl;);
		
		// prune the x variables (this is based on the fact that the
		// lagrangian z bound is a linear function of each x_i)
		
		// cerr << "max_notmin = " << max_notmin << endl;
		// cerr << "out_ = " << out_ << endl;
		// cerr << "DE_ROUND_DOWN(out_->Min()) / prec_ = " << DE_ROUND_DOWN(out_->Min()) / prec_ << endl;
		// cerr << "DE_ROUND_UP(out_->Max()) / prec_ = " << DE_ROUND_UP(out_->Max()) / prec_ << endl;
		
		// get the actual z limit (domain max or domain min)
		double z_lim = max_notmin
					   ? invtansig_ub(DE_ROUND_DOWN(out_->Min()) / prec_)
					   : invtansig_lb(DE_ROUND_UP(out_->Max()) / prec_);
				
		for (int i = 0; i < n_; ++i) {
			// compute the residual
			double z_res = z_lim - (s_.z - s_.xl[i] * s_.x[i]);
			// compute the x bound (valid only ix s_.xl[i] is non-zero)
			if (fabs(s_.xl[i]) >= NN_EPSILON) {
				double x_bound = z_res / s_.xl[i];
				// enforce the bound
				if ((max_notmin && s_.xl[i] > 0) ||
					(!max_notmin && s_.xl[i] < 0)){
					// we have a lower bound
					int64 x_lb_loc = safe_ceil(x_bound * prec_);
					
					if (x_lb_loc > x_[i]->Min()) ++cnt_lx_filter;
					
					x_[i]->SetMin(x_lb_loc);
				}
				else {
					// we have a upper bound
					int64 x_ub_loc = safe_floor(x_bound * prec_);
					
					if (x_ub_loc < x_[i]->Max()) ++cnt_lx_filter;
					
					x_[i]->SetMax(x_ub_loc);
				}
			}			
		}
		
		DBG(cerr << "<< Tansig2LFFCst::lag_x_pruning" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// ======================================================
	// = METHODS TO SOLVE THE LAGRANGIAN RELAXATION PROBLEM =
	// ======================================================
	
	// reduced cost used for the "a" subproblem (with rounding)
	double modobj(double w, double yl, double y) {
		return w * TANSIG(y) + yl * y;
	}

	// implementation of argmin/argmax for a custom function
	double pick_best(double w, double yl,
					 double y1, double y2, bool max_notmin) {
		if (modobj(w,yl,y1) >= modobj(w,yl,y2))
			return max_notmin ? y1 : y2;
		else
			return max_notmin ? y2 : y1;
	}

#undef DEBUG_ON
#define DEBUG_ON true
	void solve_relaxation(bool max_notmin) {
		DBG(cerr << ">> Tansig2LFFCst::solve_relaxation" << endl;);
		
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "s_.lambda = "; print_array(s_.lambda, m_); cerr << endl;
		);
		
		// COMPUTE THE PROBLEM COEFFICIENTS
		// Compute fixed bound offset
		s_.zl = getBias1();
		for (int j = 0; j < m_; ++j)
			s_.zl += getBias0(j) * s_.lambda[j];

		// Compute the coefficients of all the "y" variables
		for (int j = 0; j < m_; ++j)
			s_.yl[j] = -s_.lambda[j];

		// Compute the coefficients of all the "x" variables
		for (int i = 0; i < n_; ++i) {
			s_.xl[i] = 0; // reset the coefficient
			for (int j = 0; j < m_; ++j)
				s_.xl[i] += s_.lambda[j] * getWgt0(i,j);
		}

		DBG(
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.zl = " << s_.zl << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.yl = "; print_array(s_.yl, m_); cerr << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.xl = "; print_array(s_.xl, n_); cerr << endl;
		);

		// START THE BOUND COMPUTATION
		// start from the fixed offset
		double bound = s_.zl;
		// DBG(
		// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
		// 	 << "partial bound = " << bound << endl;
		// );

		// add the contribution of the "y" subproblems
		for (int j = 0; j < m_; ++j) {
			// get minimum and maximum value of the j-th "y" variable
			double ymin = y_lb.Value(j);
			double ymax = y_ub.Value(j);
			// cache the weight and the "y" coefficient
			double w = getWgt1(j);
			double yl = s_.yl[j];
			// DBG(
			// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			// 	 << "w = " << w << ", yl = " << yl << endl;
			// );

			// pick the best value among the extremes
			//TODO I am checking 4 values here, it should be possible to check 2
			double yval = pick_best(w, yl, ymin, ymax, max_notmin);

			// compute the two solutions to the second-degree equation (both
			// the "a" and "c" terms in the classical formula are 1). The "b"
			// term is computed here.
			// NOTE: the equation has real valued solutions only if "yl" is
			// non-zero, "w" and "yl" are opposite in sign and "yl" it is not
			// greater than than "w" in absolute value
			if (yl != 0 &&  w/yl < -1) {
				double b = 2 + 4 * w / yl;
				// solve the second degree equation
				double u1 = 0.5*(-b - sqrt(b*b - 4));
				double u2 = 0.5*(-b + sqrt(b*b - 4));
				// translate back to y values
				double y1 = max(ymin,-0.5 * log(u2));
				double y2 = min(ymax,-0.5 * log(u1));
				// pick the best value
				yval = pick_best(w, yl, yval, y1, max_notmin);
				yval = pick_best(w, yl, yval, y2, max_notmin);
			}

			// store the "y" value corresponding to the bound
			s_.y[j] = yval;
			// udpate the bound
			bound += modobj(w, yl, yval);
			// DBG(
			// cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			// 	 << "partial bound = " << bound << endl;
			// );
		}

		// add the contribution of each "x" variable to the bound
		for (int i = 0; i < n_; ++i) {
			// If the goal is to compute an upper bound, then:
			// -- xl[i] >= 0 ---> choose the largest possible "x"
			// -- xl[i] <= 0 ---> choose the lowest possible "x"
			// Do the opposite if the goal is to compute a lower bound.
			if (s_.xl[i] >= 0) {
				if (max_notmin) s_.x[i] = x_ub.Value(i);
				else s_.x[i] = x_lb.Value(i);
			}
			else {
				if (max_notmin) s_.x[i] = x_lb.Value(i);
				else s_.x[i] = x_ub.Value(i);
			}
			// update the bound
			bound += s_.xl[i] * s_.x[i];
		}
		
		// store the bound in the lagrangian state
		s_.z = bound;
		
		// prune the x variables (simple approach with linear cost)
		if (mode_ >= 3) {
			lag_x_pruning(max_notmin);
		}
		
		DBG(
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.z = " << s_.z << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.y = "; print_array(s_.y, m_); cerr << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.x = "; print_array(s_.x, n_); cerr << endl;
		);
		
		DBG(cerr << "<< Tansig2LFFCst::solve_relaxation" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true

#undef DEBUG_ON
#define DEBUG_ON true
	void update_multipliers(step_update_policy su_policy,
						bool max_notmin) { // "true" for UB, "false" for LB
		DBG(cerr << ">> Tansig2LFFCst::update_multipliers" << endl;);
		DBG(
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.alpha = " << s_.alpha << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.beta = " << s_.beta << endl;
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
			 << "s_.delta = " << s_.delta << endl;
		);

		// check the validity of the parameters
		assert(s_.beta >= 0);
		assert(s_.alpha > 0);
		assert(s_.alpha <= 1);
		assert(!(s_.alpha < 1) || s_.beta <= s_.alpha);
		assert(s_.delta >= 0);
		
		// if multiplier randomization is active and the number of iterations
		// without an improvement has reached the limit
		if (max_nbad > 0 &&
			(max_notmin ? nbad_ub.Value() : nbad_lb.Value()) >= max_nbad) {
				
			// make a small random perturbation to the multipliers
			for (int j = 0; j < m_; ++j) {
				// randomize the multiplier with a 10% probability
				if (solver()->Rand64(10) == 0) {
					// draw a random number in ]0.1 * -1, 0.1 * 1[
					double r = 0.1 * (solver()->Rand64(2001) - 1000) / 1000.0;
					s_.lambda[j] += r * s_.lambda[j];
				}
			}
			
			// reset the number of iterations without an improvement
			if (max_notmin) nbad_ub.SetValue(solver(), 0);
			else nbad_lb.SetValue(solver(), 0);
						
			// return (the standard update does not apply)
			return;
		}

		// COMPUTE THE CURRENT GRADIENT
		for (int j = 0; j < m_; ++j) {
			// add the constant contribution
			s_.lambda_d[j] = getBias0(j);
			// add the contribution connected to the "x" variables
			for (int i = 0; i < n_; ++i)
				s_.lambda_d[j] += getWgt0(i,j) * s_.x[i];
			// add the contribution connected to the "y" variables
			s_.lambda_d[j] += -s_.y[j];
		}

		DBG(
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
		     <<"lambda_d = "; print_array(s_.lambda_d, m_); cerr << endl;
		);

		// OBTAIN THE SEARCH DIRECTION
		// NOTE: if alpha = 1, this is just the gradient
		for (int j = 0; j < m_; ++j)
			s_.lambda_s[j] = s_.alpha * s_.lambda_d[j]
							+ (1 - s_.alpha) * s_.lambda_s[j];
		DBG(
		cerr << "(lr-"<<(max_notmin ? "max" : "min")<<") "
		     <<"lambda_s = "; print_array(s_.lambda_s, m_); cerr << endl;
		);

		// DEFINE THE STEP SIZE
		// compute the square L2-norm of the direction vector
		double sds = 0;
		for (int j = 0; j < m_; ++j)
			sds += s_.lambda_s[j] * s_.lambda_s[j];
		
		// if the norm is too small, then round it to 0
		sds = (sds < NN_EPSILON) ? 0 : sds;
		
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "sds = " << sds << endl;
		);

		// define the (basic) step size
		double step_size = 0;
		double road = 0;
		// the step is 0 if the direction vector has 0 norm
		if (sds != 0) {
			if (su_policy == su_simple) {
				step_size = s_.beta / (s_.it_num * sqrt(sds));
				road = s_.beta / s_.it_num;
			}
			// in case the Polyak policy is used, modify the step length
			else if (su_policy == su_polyak_nonvanishing) {
				// compute the step size
				double estimated_gap = max_notmin
										? s_.z - (z_ubl.Value() - s_.delta)
										: (z_lbl.Value() + s_.delta) - s_.z;
				step_size = s_.beta * estimated_gap / sds;
				road = step_size * sqrt(sds);
			}
		}
		
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "step_size = " << step_size << endl;
		);

		// decide the update direction (counter gradient-wise to get an upper
		// bound, gradient-wise to get a lower bound)
		step_size *= (max_notmin ? -1 : +1);

		// UPDATE THE MULTIPLIERS
		for (int j = 0; j < m_; ++j)
			s_.lambda[j] += step_size * s_.lambda_s[j];
		
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "s_.lambda = "; print_array(s_.lambda, m_); cerr << endl;
		);
		DBG(cerr << "<< Tansig2LFFCst::update_multipliers" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true

#undef DEBUG_ON
#define DEBUG_ON false
	void solve_lagrangian_problem(
					bool max_notmin, // "true" for UB, "false" for LB
					bool first) { // true when the cst is first activated
		DBG(cerr << ">> Tansig2LFFCst::solve_lagrangian_problem" << endl;);
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") " << "x_ = [";
		for (int i = 0; i < n_; ++i)
			cerr << (i ? ", " : "") << x_[i]; //TODO how to handle this?
		cerr << "]" << endl;
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") " << "y_ = [";
		for (int j = 0; j < m_; ++j)
			cerr << (j ? ", " : "")
				 << "[" << y_lb.Value(j)
				 << ", " << y_ub.Value(j) << "]";
		cerr << "]" << endl;
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "z_ = [" << z_lbl.Value() << ", "
				 		 << z_ubl.Value() << "]" << endl;
		);

		// start from the stored multiplier values
		for (int j = 0; j < m_; ++j)
			s_.lambda[j] = max_notmin ? lambda_ub.Value(j) : lambda_lb.Value(j);
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "s_.lambda = "; print_array(s_.lambda, m_); cerr << endl;
		);

		// reset search direction
		for (int j = 0; j < m_; ++j) s_.lambda_s[j] = 0;
		DBG(
		cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
			 << "s_.lambda_s = "; print_array(s_.lambda_s, m_); cerr << endl;
		);

		// init some parameter in the lagrangian state
		s_.alpha = fixed_alpha;
		s_.it_num = 0;
		s_.beta = original_beta;
		s_.delta = -1; // this is an invalid value: the true dalta is initialized afterwards

		// delta_star value (this is initialized at the first iteration)
		double delta_star = -1;

		// start the subgradient optimization
		while (s_.it_num <= max_it) {
			DBG(
			cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
				 << "SUBGRADIENT OPTIMIZATION IT. " << s_.it_num << endl;
			);

			// SOLVE THE RELAXATION WITH THE CURRENT MULTIPLIERS
			solve_relaxation(max_notmin);

			// UPDATE THE BOUND AND RECORD IMPROVEMENT EVENTS
			// default values for the even flags
			bool bound_improved = false;
			bool polyak_nonvanishing_improvement = false;
			
			// each time the constraint is triggered, re-adjust delta_star
			if (s_.it_num == 0 && su_policy == su_polyak_nonvanishing) {
				// updating delta_star seems to be a very good idea
				delta_star = delta_star_mult * abs(s_.z);
				DBG(
				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
					 << "delta_star = " << delta_star << endl;
				);
			}

			// if this is the first iteration, save whatever bound we obtained
			if (first && s_.it_num == 0) {
				DBG(
				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
					 << "first bound found" << endl;
				);

				// record events
				bound_improved = true;
				polyak_nonvanishing_improvement = true;
			}
			else {
				DBG(
				if (max_notmin)
					cerr << "(lr-max) z_ub = " << z_ubl.Value() << endl;
				else
					cerr << "(lr-min) z_ub = " << z_lbl.Value() << endl;
				);
				// compute the improvement amount
				double improvement = max_notmin ? z_ubl.Value() - s_.z :
												  s_.z - z_lbl.Value();
				DBG(
				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
					 << "improvement = " << improvement << endl;
				);

			    // detect a bound improvement event
				if (improvement > 0)
					bound_improved = true;
				// detect a sufficient improvement for the polyak policy with
				// non-vanishing threshold
				if (improvement >= s_.delta)
					polyak_nonvanishing_improvement = true;
			}

			// STORE THE LAGRANGIAN MULTIPLIERS AND THE SOLUTION
			// (in case of an improvement)
			if (bound_improved) {
				// store the current bound and multipliers
				DBG(
				cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
					 << "store new bound" << endl;
				);

				if (max_notmin) {
					z_ubl.SetValue(solver(), s_.z);
					for (int j = 0; j < m_; ++j)
						lambda_ub.SetValue(solver(), j, s_.lambda[j]);
					
					// reset the number of iterations since last improvement
					if (!first) nbad_ub.SetValue(solver(), 0);
				}
				else {
					z_lbl.SetValue(solver(), s_.z);
					for (int j = 0; j < m_; ++j)
						lambda_lb.SetValue(solver(), j,  s_.lambda[j]);
					
					// reset the number of iterations since last improvement
					if (!first) nbad_lb.SetValue(solver(), 0);
				}
			}
			else {
				if (max_notmin) nbad_ub.Add(solver(), 1);
				else nbad_lb.Add(solver(), 1);
			}
			
			// stop if the maximum number of iterations has been reached
			if (s_.it_num >= max_it) break;

			// UPDATE THE LAGRANGIAN MULTIPLIERS
			if (su_policy == su_polyak_nonvanishing) {
				// if the estimated bound has been improved, reset delta
				int local_nbad = (max_notmin ? nbad_ub.Value() : nbad_lb.Value());
				if (polyak_nonvanishing_improvement ||
					(max_nbad > 0 && local_nbad >= max_nbad)) {
					DBG(
					cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
						 << "polyak improvement" << endl;
					);

					double best_b = max_notmin ? z_ubl.Value() : z_lbl.Value();
					s_.delta = max(delta_star, abs(best_b) * delta_mult);
				}
				// otherwise, reduce the delta value
				else {
					DBG(
					cerr << "(lr-" << (max_notmin ? "max" : "min") << ") "
						 << "no polyak improvement" << endl;
					);

					s_.delta = max(delta_star, s_.delta*delta_attenuation);
				}
			}
			// if non improving steps are accepted
			else if (su_policy == su_simple){
				// do nothing
			}
			else {
				cerr << "ERROR: unknown step update policy" << endl;
				exit(1);
			}

			// one more iteration
			s_.it_num++;

			// update multipliers and travelled road
			update_multipliers(su_policy, max_notmin);
		}

		DBG(cerr << "<< Tansig2LFFCst::solve_lagrangian_problem" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true

//
// PARAMETERS FOR THE LAGRANGIAN PROPAGATOR
//

#define PAR_2LFFCst_ALPHA 0.5				// alpha coefficient for subgradient deflection
#define PAR_2LFFCst_BETA 0.5				// beta parameter in the polyak step strategy
#define PAR_2LFFCst_DELTA_STAR_MULT 0.01	// delta* parameter in the polyak step strategy
#define PAR_2LFFCst_DELTA_ATTENUATION 0.75	// delta attenuation in the polyak step strategy
#define PAR_2LFFCst_DELTA_MULT_FIRST 0.25	// delta multiplier in the polyak step strategy (fist execution)
#define PAR_2LFFCst_DELTA_MULT_LATER 0.05	// delta multiplier in the polyak step strategy (other executions)

#define PAR_2LFFCst_MAX_IT_FIRST 1000		// max number of subgradient iterations (first execution)
#define PAR_2LFFCst_MAX_IT_LATER 5			// max number of subgradient iterations (other executions)
#define PAR_2LFFCst_NBAD_BEFOR_RAND -1		// number unsuccessful iterations before multiplier randomization
											// <= 0 for no randomization
#define PAR_2LFFCst_ACTIVATION_BUDGET 18	// stop the Lagrangian propagation after N executions
											// <= 0 for no limitation

	// method for forward propagation
#undef DEBUG_ON
#define DEBUG_ON true
	void lag_x2z(bool first) {
		DBG(cerr << ">> Tansig2LFFCst::lag_x2z" << endl;);
		DBG(cerr << "before: " << this << endl;);
				
		// // init the lagrangian state
		// lagrangian_state s_;
		// s_.lambda = new double[m_];
		// s_.yl = new double[m_];
		// s_.xl = new double[n_];
		// s_.x = new double[n_];
		// s_.y = new double[m_];
		// s_.lambda_d = new double[m_];
		// s_.lambda_s = new double[m_];
		
		// limit the constraint activations
		if (PAR_2LFFCst_ACTIVATION_BUDGET > 0) {
			if (act_cnt.Value() < PAR_2LFFCst_ACTIVATION_BUDGET) {
				++cnt_l_act; // count one more activation
				act_cnt.SetValue(solver(), act_cnt.Value() + 1);
			}
			else
				return;			
		}
		else {
			++cnt_l_act; // count one more activation
		}

		// first = true;

		// configure the parameters for the subgradient optimization
		fixed_alpha = PAR_2LFFCst_ALPHA;
		original_beta = PAR_2LFFCst_BETA;
		delta_star_mult = PAR_2LFFCst_DELTA_STAR_MULT;
		delta_attenuation = PAR_2LFFCst_DELTA_ATTENUATION;
		delta_mult = (first ? PAR_2LFFCst_DELTA_MULT_FIRST : PAR_2LFFCst_DELTA_MULT_LATER);
		max_it = (first ? PAR_2LFFCst_MAX_IT_FIRST : PAR_2LFFCst_MAX_IT_LATER); // 5
		su_policy = su_polyak_nonvanishing;
		max_nbad = PAR_2LFFCst_NBAD_BEFOR_RAND; // number of iterations before mult. randomization

		// solve the lagrangian problem to obtain an upper bound
		solve_lagrangian_problem(true, first);
		
		// cout << "z_ubl = " << z_ubl.Value() << endl; // TODO bound testing

		DBG(
		cerr << "z_ubl = " << z_ubl.Value() << endl;
		cerr << "lambda_ub = [";
		for (int j = 0; j < m_; ++j)
			cerr << (j ? ", " : "") << lambda_ub.Value(j);
		cerr << "]" << endl;
		);
		
		// record if some filtering occurred
		if (ROUND(TANSIG(z_ubl.Value()) * prec_) < out_->Max()) ++cnt_l_filter;

		// prune the output variable
		out_->SetMax(ROUND(TANSIG(z_ubl.Value()) * prec_));

		// solve the lagrangian problem to obtain a lower bound
		solve_lagrangian_problem(false, first);

		// cout << "z_lbl = " << z_lbl.Value() << endl; // TODO bound testing

		DBG(
		cerr << "z_lbl = " << z_lbl.Value() << endl;
		cerr << "lambda_lb = [";
		for (int j = 0; j < m_; ++j)
			cerr << (j ? ", " : "") << lambda_lb.Value(j);
		cerr << "]" << endl;
		);
		
		// record if some filtering occurred
		if (ROUND(TANSIG(z_lbl.Value()) * prec_) > out_->Min()) ++cnt_l_filter;

		// prune the intut variable
		out_->SetMin(ROUND(TANSIG(z_lbl.Value()) * prec_));
		
		// // tear down the lagrangian state
		// delete[] s_.lambda;
		// delete[] s_.yl;
		// delete[] s_.xl;
		// delete[] s_.x;
		// delete[] s_.y;
		// delete[] s_.lambda_d;
		// delete[] s_.lambda_s;

		DBG(cerr << "after: " << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::lag_x2z" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// method for backward propagation
#undef DEBUG_ON
#define DEBUG_ON true
	void lag_z2x() {
		DBG(cerr << ">> Tansig2LFFCst::lag_z2x" << endl;);
		DBG(cerr << "before: " << this << endl;);
		
		// obtain starting z bounds
		double z_ub_loc = invtansig_ub(DE_ROUND_UP(out_->Max()) / prec_);
		double z_lb_loc = invtansig_lb(DE_ROUND_DOWN(out_->Min()) / prec_);
		
		// check if a change of z has tigthened the y bounds via standard
		// propagation. In such case, the lagrangian bounds must be re-computed
		// (i.e. x2z lagrangian propagation must be re-done)
		// in second place, the lagrangian propagation should be re-triggered
		// if z has been modified so that the new bounds are tigther than those
		// computed via the relaxation
		if (trigger_y2z_lag.Value() ||
			z_lb_loc > z_lbl.Value() ||
			z_ub_loc < z_ubl.Value()) {
			// reset the flag
			trigger_y2z_lag.SetValue(solver(), false);
			// trigger forward lagrangian propagation
			lag_x2z(false);
		}

		DBG(cerr << "after: " << this << endl;);	
		DBG(cerr << "<< Tansig2LFFCst::lag_z2x" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true


	// Initial propagation method
#undef DEBUG_ON
#define DEBUG_ON true
	void InitialPropagate() {
		DBG(cerr << ">> Tansig2LFFCst::InitialPropagate" << endl;);
		DBG(cerr << this << endl;);
		
		
		// perform standard neuron constraint propagation and sync all variable
		// and activity bounds
		std_x2z_noinc();
		std_z2x_noinc();
		
		// do the same for lagrangian propagation
		if (mode_ >= 2){
			lag_x2z(true);
		}
		
		// exit(0); // TODO for bound testing
				
		// // configure the parameters for the subgradient optimization
		// fixed_alpha = 0.5;
		// original_beta = fixed_alpha;
		// delta_star_mult = 0.01;
		// delta_attenuation = 0.75;
		// delta_mult = 0.25;
		// max_it = 100;
		// su_policy = su_polyak_nonvanishing;
		//
		// // solve the lagrangian problem to obtain an upper bound
		// solve_lagrangian_problem(true);
		//
		// DBG(
		// cerr << "z_ub = " << z_ub.Value() << endl;
		// cerr << "lambda_ub = [";
		// for (int j = 0; j < m_; ++j)
		// 	cerr << (j ? ", " : "") << lambda_ub.Value(j);
		// cerr << "]" << endl;
		// );
		//
		// // prune the output variable
		// out_->SetMax(ROUND(TANSIG(z_ub.Value()) * prec_));
		//
		// // solve the lagrangian problem to obtain a lower bound
		// solve_lagrangian_problem(false);
		//
		// DBG(
		// cerr << "z_lb = " << z_lb.Value() << endl;
		// cerr << "lambda_lb = [";
		// for (int j = 0; j < m_; ++j)
		// 	cerr << (j ? ", " : "") << lambda_lb.Value(j);
		// cerr << "]" << endl;
		// );
		//
		// // prune the output variable
		// out_->SetMin(ROUND(TANSIG(z_lb.Value()) * prec_));
		//
		// // cerr << "EARLY EXIT, FOR DEBUG PURPOSE!" << endl;
		// // exit(1);
		
		DBG(cerr << this << endl;);
		DBG(cerr << "<< Tansig2LFFCst::InitialPropagate" << endl;);
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	const char* get_ntype_string(neuron_type t) const {
		switch(t) {
			case ntype_purelin:
				return "purelin";
			case ntype_tansig:
				return "tansig";
			case ntype_hardlim:
				return "hardlim";
			default:
				cerr << "ERROR: unknown neuron type" << endl;
				exit(1);
		}
	}
	
	// 
	string DebugString() const {
		string out;
		
		// add the description of the output variable
		out.append(out_->DebugString());
		out.append(" = round(");
		// bias of the output neuron activity
		StringAppendF(&out, "%g", getBias1());
		for (int j = 0; j < m_; ++j) {
			out.append("+");
			StringAppendF(&out, "%g", getWgt1(j));
			out.append("*");
			out.append("tansig");
			out.append("(");
			// append hidden neuron bias
			StringAppendF(&out, "%g", getBias0(j));
			for (int i = 0; i < n_; ++i) {
				out.append(" + ");
				StringAppendF(&out, "%g", getWgt0(i, j));
				out.append("*");
				out.append(x_[i]->DebugString());
				StringAppendF(&out, "/%g", prec_);
			}
			out.append(")");
		}
		out.append(")");
		StringAppendF(&out, " * %g)", prec_);
		
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint("2LNNCst", this);
		visitor->VisitIntegerExpressionArgument(
				ModelVisitor::kExpressionArgument,out_);			
        for(int i = 0; i < n_; ++i) {
    			visitor->VisitIntegerExpressionArgument(
					ModelVisitor::kExpressionArgument,
					x_[i]);
		}
		//         for(int j = 0; j < m_; ++j) {
		//     			visitor->VisitIntegerExpressionArgument(
		// ModelVisitor::kExpressionArgument,
		// y_[j]);
		//         }
		visitor->EndVisitConstraint("2LNNCst", this);
	}

};

Constraint* MakeTansig2LFFCst(Solver* const s,
					 const std::vector<IntExpr*>& x,
					 IntExpr* out,
					 const std::vector<double>& wgt0,
					 const std::vector<double>& wgt1,
					 int32 prec,
					 int64 mode) {
	 return s->RevAlloc(new Tansig2LFFCst(s, x, out, wgt0, wgt1,
	 									  prec, mode));
}

// Constraint* MakeTansig2LFFCst(Solver* const s,
// 					 const std::vector<IntVar*>& x,
// 					 IntExpr* out,
// 					 const std::vector<double>& wgt0,
// 					 const std::vector<double>& wgt1,
// 					 int32 prec,
// 					 string name) {
// 	 return s->RevAlloc(new Tansig2LFFCst(s, (const std::vector<IntExpr*>&) x, out, wgt0, wgt1, prec, name));
// }

//==============================================================================
// Floating Product Constraint (used for normalizations)
// TODO: use separate objects for positive and negative coefficients
//==============================================================================

class RoundedProductConstraint : public Constraint
{
private:
	// pointer to the input variable
	IntExpr* const in_;
    // pointer to the output variable (holding the product result)
    IntExpr* const out_;
	// coefficient
	double coeff_;
    // type constant (used during the visit method)
    static char type_[];
	
public:
	/**
	 * @brief Constructor to build a rounded product constraint
     * Constructor to build a rounded product constraint, representing the+
     * expression y = round(x * v)
     * where "v" is a real valued coefficient and "x", "y" are integer
     * expressions 
	 * @param s the solver
	 * @param input the multplied varible
	 * @param output the expression holding the product result
	 * @param coeff number the real valued coefficient
	 */
	RoundedProductConstraint(Solver* const s,
                             IntExpr* const input,
                             IntExpr* const output,
                             double coeff)
	: Constraint(s), out_(output), in_(input), coeff_(coeff) {}

	virtual ~RoundedProductConstraint() {}
	

	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
        // post on input and output variable
        if (coeff_ > 0) {
            in_->WhenRange(MakeConstraintDemon0(solver(), this,
                           &RoundedProductConstraint::PropagateXtoYpos, "x2y"));
            out_->WhenRange(MakeConstraintDemon0(solver(), this,
                            &RoundedProductConstraint::PropagateYtoXpos, "y2x"));
        }
        else {
            in_->WhenRange(MakeConstraintDemon0(solver(), this,
                                                &RoundedProductConstraint::PropagateXtoYneg, "x2y"));
            out_->WhenRange(MakeConstraintDemon0(solver(), this,
                                                 &RoundedProductConstraint::PropagateYtoXneg, "y2x"));            
        }
	}
    
    void PropagateXtoYpos() {
        double prodMax = in_->Max() * coeff_;
        out_->SetMax((int64) ROUND(prodMax));
        double prodMin = in_->Min() * coeff_;
        out_->SetMin((int64) ROUND(prodMin));
    }
    
    void PropagateXtoYneg() {
        double prodMax = in_->Min() * coeff_;
        out_->SetMax((int64) ROUND(prodMax));
        double prodMin = in_->Max() * coeff_;
        out_->SetMin((int64) ROUND(prodMin));
    }
    
    void PropagateYtoXpos() {
        // in order to take into account the rouding, the bounds of the "out_"
        // variable must be adjusted
        double divMax = DE_ROUND_UP(out_->Max()) / coeff_;
        in_->SetMax((int64) floor(divMax));
        double divMin = DE_ROUND_DOWN(out_->Min()) / coeff_;
        in_->SetMin((int64) ceil(divMin));
    }
    
    void PropagateYtoXneg() {
        // in order to take into account the rouding, the bounds of the "out_"
        // variable must be adjusted
        double divMax = DE_ROUND_DOWN(out_->Min()) / coeff_;
        divMax += 0.5 - NN_EPSILON;
        in_->SetMax((int64) floor(divMax));
        double divMin = DE_ROUND_UP(out_->Max()) / coeff_;
        divMin -= 0.5;
        in_->SetMin((int64) ceil(divMin));
    }
	
	/**
	 * @brief Propagation method
	 */
	void InitialPropagate() {
        if (coeff_ > 0) {
            PropagateXtoYpos();
            PropagateYtoXpos();
        }
        else {
            PropagateXtoYneg();
            PropagateYtoXneg();
        }
	}
	
	
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		out.append(out_->DebugString());
		out.append(" = ROUND(");
        StringAppendF(&out, "%lf", coeff_);
        out.append(" * ");
        out.append(in_->DebugString());
        out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(type_, this);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												in_);        
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												out_);
		visitor->EndVisitConstraint(type_, this);
	}
}; // end of RoundedProductConstraint

//
// Define the "type_" fixed attribute
//
char RoundedProductConstraint::type_[] = "RoundedProductCst";

//
// Building function for a rounded product constraint
//
Constraint* MakeRoundedProductConstraint(Solver* s,
                              IntExpr* const input,
							  IntExpr* const output,
							  double coeff) {
    CHECK(input != NULL) << "NULL input expression, maybe a bad cast";
    CHECK(output != NULL) << "NULL output expression, maybe a bad cast";
    CHECK(coeff != 0) << "avoid 0 coefficients in rounded products";
    return s->RevAlloc(new RoundedProductConstraint(s, input, output, coeff));
}

//==============================================================================
// Rounded division between integer expression
// ATTN: the denominator must be strictly positive and the numeraor non-negative
//==============================================================================

class RoundedDivisionConstraint : public Constraint
{
private:
	// pointer to the numerator
	IntExpr* const num_;
	// pointer to the denominator
	IntExpr* const den_;
	// pointer to the output variable (holding the product result)
	IntExpr* const out_;
	int64 out_min_;
	int64 out_max_;
	// type constant (used during the visit method)
	static char type_[];
	
public:
	/**
	 * @brief Constructor to build a rounded division constraint
     * Constructor to build a rounded division constraint, representing the
     * expression z = round(x / y)
     * where "x", "y", "z" are integer expressions 
	 * @param s the solver
	 * @param num the numerator
	 * @param den the denominator
	 * @param output the expression holding the product result
	 */
	RoundedDivisionConstraint(Solver* const s,
                             IntExpr* const num,
                             IntExpr* const den,
                             IntExpr* const output)
	: Constraint(s), out_(output), num_(num), den_(den) {
		this->out_min_ = output->Min();
		this->out_max_ = output->Max();
	}

	virtual ~RoundedDivisionConstraint() {}
	

	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
		Demon* toNUM = MakeConstraintDemon0(solver(), this,
			&RoundedDivisionConstraint::Propagate2NUM, "2num");
		Demon* toDEN = MakeConstraintDemon0(solver(), this,
			&RoundedDivisionConstraint::Propagate2DEN, "2den");
		Demon* toOUT = MakeConstraintDemon0(solver(), this,
			&RoundedDivisionConstraint::Propagate2OUT, "2out");		

		num_->WhenRange(toDEN);
		num_->WhenRange(toOUT);
		den_->WhenRange(toNUM);
		den_->WhenRange(toOUT);
		out_->WhenRange(toNUM);
		out_->WhenRange(toDEN);
	}

#undef DEBUG_ON
#define DEBUG_ON true
	void Propagate2OUT() {
		DBG(cerr << "Propagate2OUT" << endl;)
		DBG(cerr << this << endl;)
		
		// compute an upper bound on the quotient variable
		if (num_->Max() > 0) {
			// if the denominator cannot be zero:
			if (den_->Min() > 0)
				out_->SetMax(ROUND(num_->Max() / (double) den_->Min()));
			// otherwise, do nothing
		}
		else if (num_->Max() == 0) {
			out_->SetMax(0);
		}
		else {
			// if the denominator can be non-zero
			if (den_->Max() > 0)
				out_->SetMax(ROUND(num_->Max() / (double) den_->Max()));
			// otherwise, 
			else solver()->Fail();
		}
		
		// compute a lower bound on the quotient variable
		if (num_->Min() > 0) {
			// if the denominator can be non-zero
			if (den_->Max() > 0)
				out_->SetMin(ROUND(num_->Min() / (double) den_->Max()));
			// otherwise, fail
			else solver()->Fail();
		}
		else if (num_->Min() == 0) {
			out_->SetMin(0);
		}
		else {
			// if the denomintator cannot be zero
			if (den_->Min() > 0)
				out_->SetMin(ROUND(num_->Min() / (double) den_->Min()));
			// otherwise, do nothing
		}
		
		DBG(cerr << this << endl;)
	}

   void Propagate2NUM() {
		DBG(cerr << "Propagate2NUM" << endl;)
		DBG(cerr << this << endl;)

		// de-round and saturate
		double outUB = DE_ROUND_UP(out_->Max());
		if (outUB > out_max_) outUB = out_max_;
		double outLB = DE_ROUND_DOWN(out_->Min());
		if (outLB < out_min_) outLB = out_min_;
				
		// COMPUTE AN UPPER BOUND ON THE NUMERATOR
		if (outUB >= 0) {
			// if the denominator can be non-zero
			if (den_->Max() > 0)
				num_->SetMax(floor(den_->Max() * outUB));
			// otherwise, set the numerator upper bound to 0
			else num_->SetMax(0);
		}
		else {
			// if the denominator cannot be zero
			if (den_->Min() > 0) 
				num_->SetMax(floor(den_->Min() * outUB));
			// otherwise, the largest negative bound will work
			else num_->SetMax(-1);
		}
		
		if (outLB > 0) {
			// if the denominator cannot be zero
			if (den_->Min() > 0)
				num_->SetMin(ceil(den_->Min() * outLB));
			// otherwise
			else num_->SetMin(1);
		}
		else {
			// if the denominator can be non-zero
			if (den_->Max() > 0)
				num_->SetMin(ceil(den_->Max() * outLB));
			// otherwise
			else num_->SetMin(0);
		}
		
		DBG(cerr << this << endl;)
   }

   void Propagate2DEN() {
		DBG(cerr << "Propagate2DEN" << endl;)
		DBG(cerr << this << endl;)

		// de-round and saturate
		double outUB = DE_ROUND_UP(out_->Max());
		if (outUB > out_max_) outUB = out_max_;
		double outLB = DE_ROUND_DOWN(out_->Min());
		if (outLB < out_min_) outLB = out_min_;

		// test the feasibility of a zero denominator
		if (num_->Max() < 0 || num_->Min() > 0) den_->SetMin(1);

		if (outUB > 0) {
			den_->SetMin(ceil(num_->Min() / outUB));
		}
		else if (outUB < 0){
			den_->SetMax(floor(num_->Min() / outUB));
		}
		// otherwise, we cannot deduce anything on the denominator
		
		if (outLB > 0) {
			den_->SetMax(floor(num_->Max() / outLB));
		}
		else if (outLB < 0) {
			den_->SetMin(ceil(num_->Max() / outLB));
		}
		// otherwise, we cannot deduce anything on the denominator


		// if (outUB > 0) {
		// 	double minDen = num_->Min() / outUB;
		// 	den_->SetMin(ceil(minDen)); // irregular
		// }
		// else if (outUB < 0) {
		// 	double maxDen = num_->Max() / outUB;
		// 	den_->SetMax(floor(maxDen)); // irregular
		// }
		DBG(cerr << this << endl;)
	}
#undef DEBUG_ON
#define DEBUG_ON true

	
	/**
	 * @brief Propagation method
	 */
	void InitialPropagate() {
		Propagate2NUM();
		Propagate2DEN();
		Propagate2OUT();
	}
	
	
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		out.append(out_->DebugString());
		out.append(" = ROUND(");
		out.append(num_->DebugString());
		out.append(" / ");
		out.append(den_->DebugString());
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(type_, this);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												num_);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												den_);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												out_);
		visitor->EndVisitConstraint(type_, this);
	}
}; // end of RoundedProductConstraint

//
// Define the "type_" fixed attribute
//
char RoundedDivisionConstraint::type_[] = "RoundedDivisionCst";

//
// Building function for a rounded product constraint
//
Constraint* MakeRoundedDivisionConstraint(Solver* s,
								IntExpr* const num,
								IntExpr* const den,
								IntExpr* const output) {
	CHECK(num != NULL) << "NULL numerator expression, maybe a bad cast";
	CHECK(num != NULL) << "NULL denominator expression, maybe a bad cast";
	CHECK(den != NULL) << "NULL output expression, maybe a bad cast";
	CHECK(den->Min() >= 0) << "The denominator must be non negative";
	// CHECK(num->Min() >= 0) << "The numerator must non-negative";
	// CHECK(output->Min() >= 0) << "The output must non-negative";
	return s->RevAlloc(new RoundedDivisionConstraint(s, num, den, output));
}

// =========================================
// = Minimum and second minimum constraint =
// =========================================

class RegretConstraint : public Constraint
{
private:
	// size of the array
	int size_;
	// internal pointers to the input variables
	IntExpr** in_;
	// pointer to the numerator
	IntExpr* const extr_;
	// pointer to the denominator
	IntExpr* const second_extr_;
	// direction (true = min, false = max)
	bool dir_;
	
	// reversible integers to store the last computed bounds values
	Rev<int64> extr_min_;
	// minimum for the second_extr_ variable
	Rev<int64> second_extr_min_;
	// maximum for the extr_ variable
	Rev<int64> extr_max_;
	// maximum for the second_extr_ variable
	Rev<int64> second_extr_max_;
	
	// type constant (used during the visit method)
	static char type_[];
	
public:
	RegretConstraint(Solver* const s,
							  int size,
							  const IntExpr* const* inputs,
                              IntExpr* const extr,
                              IntExpr* const second_extr)
	: Constraint(s), size_(size), extr_(extr), second_extr_(second_extr),
		extr_min_(0), second_extr_min_(0), extr_max_(0), second_extr_max_(0) {
		assert(size_ == 2);
		// init array of input variables
		in_ = new IntExpr*[size_];
		// for (int i = 0; i < size_; ++i) in_[i] = inputs[i];	
		memcpy(in_, inputs, size_ * sizeof(*inputs));
	}

	virtual ~RegretConstraint() {
		delete[] in_;
	}
	
	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
		// build the propagation demon
		Demon* in_to_out = MakeDelayedConstraintDemon0(solver(), this,
			&RegretConstraint::PropagateIN2OUT, "PropagateIN2OUT");
		Demon* out_to_int = MakeDelayedConstraintDemon0(solver(), this,
			&RegretConstraint::PropagateOUT2IN, "PropagateOUT2IN");
		
		// post over the input variables
		for (int i = 0; i < size_; ++i)
			in_[i]->WhenRange(in_to_out);
		
		// post over the output variables
		extr_->WhenRange(out_to_int);
		second_extr_->WhenRange(out_to_int);
	}

#undef DEBUG_ON
#define DEBUG_ON true
	void PropagateIN2OUT() {
		DBG(cerr << "PropagateIN2OUT" << endl;)
		DBG(cerr << this << endl;)

		// minimum for the extr_ variable
		int64 l_extr_min = in_[0]->Min();
		// minimum for the second_extr_ variable
		int64 l_second_extr_min = std::numeric_limits<int>::max(); 
		// maximum for the extr_ variable
		int64 l_extr_max = in_[0]->Max(); 
		// maximum for the second_extr_ variable
		int64 l_second_extr_max = std::numeric_limits<int>::max(); 
						
		// look for the minimum and the second minimum
		for (int i = 1; i < size_; ++i) {
			// use the variables lower bounds to determine the lower bounds
			// for the output variables
			if (in_[i]->Min() <= l_extr_min) {
				l_second_extr_min = l_extr_min;
				l_extr_min = in_[i]->Min();
			}
			else if (in_[i]->Min() < l_second_extr_min) {
				l_second_extr_min = in_[i]->Min();
			}
			// use the variables lower bounds to determine the lower bounds
			// for the output variables
			if (in_[i]->Max() <= l_extr_max) {
				l_second_extr_max = l_extr_max;
				l_extr_max = in_[i]->Max();
			}
			else if (in_[i]->Max() < l_second_extr_max) {
				l_second_extr_max = in_[i]->Max();
			}
		}
		
		// store the bound values
		extr_min_.SetValue(solver(), l_extr_min);
		second_extr_min_.SetValue(solver(), l_second_extr_min);
		extr_max_.SetValue(solver(), l_extr_max);
		second_extr_max_.SetValue(solver(), l_second_extr_max);
		
		// filter
		extr_->SetMin(l_extr_min);
		second_extr_->SetMin(l_second_extr_min);
		extr_->SetMax(l_extr_max);
		second_extr_->SetMax(l_second_extr_max);
		
		DBG(cerr << this << endl;)
	}

   void PropagateOUT2IN() {
		DBG(cerr << "PropagateOUT2IN" << endl;)
		DBG(cerr << this << endl;)

		// skip the propagation if the last bounds are not tighter than those
		// those computed by PropagateIN2OUT
		if (!(
			  extr_->Min() <= extr_min_.Value() &&
			  extr_->Max() >= extr_max_.Value() &&
			  second_extr_->Min() <= second_extr_min_.Value() &&
			  second_extr_->Max() >= second_extr_max_.Value()
			)) {
			// number of candidate extremes in the array
			int cand_extr_n = 0;
			// index of the candidate extreme, if there is a single one
			int cand_extr_i = -1; 
		
			// filter values using the extreme item
			for (int i = 0; i < size_; ++i) {
				if (in_[i]->Min() <= extr_->Min()) {
					// no item in the array can be bigger that the mimimum
					in_[i]->SetMin(extr_->Min());
					// record that a candidate extreme has been found
					cand_extr_n++;
					// record its index
					cand_extr_i = i;
				}
			}
		
			// if a single candidate was found, then we know exactly which item
			// will be the minimum.
			// NOTE: in this case, it is safe to store the (single) candidate
			// index in a reversible field, but there is no big performance
			// improvement (the asymptotic complexity stays the same)
			if (cand_extr_n == 1) {
				// we can safely set its upper bound
				in_[cand_extr_i]->SetMax(extr_->Max());
			
				// number of candidate second extremes in the array
				int cand_second_extr_n = 0;
				// index of the candidate second extreme, if there is a single one
				int cand_second_extr_i = -1; 
			
				// prune all other items using the second extreme
				for (int i = 0; i < size_; ++i) {
					if (i != cand_extr_i) {
						// no other item can be bigger that the second mimimum
						in_[i]->SetMin(second_extr_->Min());
						// record that a candidate second extreme has been found
						cand_second_extr_n++;
						// record its index
						cand_second_extr_i = i;
					}
				}
			
				// if a single candidate second extreme has been found, then we know
				// exactly which item is the second extreme and we can prune the
				// upper bound
				if (cand_second_extr_n == 1)
					in_[cand_second_extr_i]->SetMax(second_extr_->Max());
			}
		}
		
		DBG(cerr << this << endl;)
   }
#undef DEBUG_ON
#define DEBUG_ON true
	
	/**
	 * @brief Propagation method
	 */
	void InitialPropagate() {
		PropagateIN2OUT();
		PropagateOUT2IN();
	}
	
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		out.append("ABS(");
		out.append(extr_->DebugString());
		out.append("-");
		out.append(second_extr_->DebugString());
		out.append(") = REGRET(");
		for (int i = 0; i < size_; ++i) {
			if (i > 0) out.append(", ");
			out.append(in_[i]->DebugString());
		}
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(type_, this);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												extr_);
		visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												second_extr_);
		for (int i = 0; i < size_; ++i)
			visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
													in_[i]);
		visitor->EndVisitConstraint(type_, this);
	}
}; // end of RegretConstraint

//
// Define static type attribute
//
char RegretConstraint::type_[] = "RegretCst";


//
// Functions to build the constraint
//
Constraint* MakeRegretConstraint(Solver* const s,
							  int size,
							  const IntExpr* const* inputs,
                              IntExpr* const extr,
                              IntExpr* const second_extr) {
	CHECK(inputs != NULL) << "NULL input array, maybe a bad cast";
	CHECK(extr != NULL) << "NULL extr, maybe a bad cast";
	CHECK(second_extr != NULL) << "NULL second_extr, maybe a bad cast";
	CHECK(size >= 2) << "The array size must be at least 2";
	return s->RevAlloc(new RegretConstraint(s, size, inputs, extr, second_extr));
}

Constraint* MakeRegretConstraint(Solver* s,
                           const std::vector<IntExpr*>& inputs,
                           IntExpr* const extr,
						   IntExpr* const second_extr) {
    return MakeRegretConstraint(s, inputs.size(), inputs.data(),
								extr, second_extr);
}

Constraint* MakeRegretConstraint(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntExpr* const extr,
						   IntExpr* const second_extr) {
    return MakeRegretConstraint(s, inputs.size(), (IntExpr**) inputs.data(),
								extr, second_extr);
}

//==============================================================================
// Average Constraint
// This constraint enforces: y = (\sum_i w_i * x_ i) / (\sum_i x_i)
// All x_i are assumed to be non negative!
//==============================================================================

// comment this to disable incremental propagation
// #define AVG_NO_INCREMENTAL_

class AverageConstraint : public Constraint {
private:
	// internal copy of the weight array
	int64* wgt_;
	// internal pointers to the input variables
	IntVar** in_;
	// permutation indices (used to keep the original order in prints)
	int* sort_idx_;
	// internal pointer output variable
	IntVar* const out_;
	// number of inputs
	int size_;
	// name
	string name_;
	// constraint type
	static char type_[];
	
	// information abour the input data, exploited to speed up propagation
	// maximum domain value
	int64 dom_max_;
	// minimum domain value
	int64 dom_min_;
	
	// some reversible internal data
	// lower bound on the average
	Rev<int64> avg_lb_num_;
	Rev<int64> avg_lb_den_;
	// upper bound on the average
	Rev<int64> avg_ub_num_;
	Rev<int64> avg_ub_den_;
	// configuration to miminize the max average
	Rev<int64> avg_minr_num_;
	Rev<int64> avg_minr_den_;
	// configuration to maximize the min average
	Rev<int64> avg_maxr_num_;
	Rev<int64> avg_maxr_den_;
	// left threshold (last included item in the lower bounding configuration)
	Rev<int64> avg_left_thid_;
	// right threshold (last included item in the upper bounding configuration)
	Rev<int64> avg_right_thid_;
	
	// Flags to trigger selective propagation
	Rev<bool> max_prop_; // "propagation based on avg max must be performed"
	Rev<bool> min_prop_; // "propagation based on avg min must be performed"
	
public:
	AverageConstraint(Solver* const s,
						 int size,
						 const IntVar* const* inputs,
						 IntVar* const output,
						 int64 const* weights,
						 string name)
	: Constraint(s), size_(size), out_(output), name_(name),
		avg_lb_num_(INT64_MIN), avg_lb_den_(1), 
		avg_minr_num_(INT64_MIN), avg_minr_den_(1), 
		avg_ub_num_(INT64_MAX), avg_ub_den_(1), 
		avg_maxr_num_(INT64_MAX), avg_maxr_den_(1),
		avg_left_thid_(-1), avg_right_thid_(size),
		max_prop_(true), min_prop_(true)
	{
		// init array of input variables
		in_ = new IntVar*[size_];
		// for (int i = 0; i < size_; ++i) in_[i] = inputs[i];	
		memcpy(in_, inputs, size_ * sizeof(*inputs));
		// init array of weights
		wgt_ = new int64[size_];
		// for (int i = 0; i < size_; ++i) wgt_[i] = weights[i];
		memcpy(wgt_, weights, size_ * sizeof(*weights));

		// find min/max weight and min/max doman value
		dom_min_ = INT64_MAX;
		dom_max_ = INT64_MIN;
		for (int i = 0; i < size_; ++i) {
			dom_min_ = min(dom_min_, in_[i]->Min());
			dom_max_ = max(dom_max_, in_[i]->Max());
		}
						
		// sort inputs by increasing weight (using bubble sort: this method is
		// executed prior to search and is therefore not performance critical)
		sort_idx_ = new int[size_];
		for(int i = 0; i < size_; ++i) sort_idx_[i] = i;
		for (int k = size-1; k >= 1; --k) {
			bool swap_done = false;
			for (int i = 0; i < k; ++i)
				if (wgt_[i] > wgt_[i+1]) {
					// swap weight
					double temp_wgt = wgt_[i];
					wgt_[i] = wgt_[i+1];
					wgt_[i+1] = temp_wgt;
					// swap integer expression
					IntVar* temp_expr = in_[i];
					in_[i] = in_[i+1];
					in_[i+1] = temp_expr;
					// swap permutation indices
					int temp_sortidx = sort_idx_[i];
					sort_idx_[i] = sort_idx_[i+1];
					sort_idx_[i+1] = temp_sortidx;
					// raise swap flag
					swap_done = true;
				}
			// early exit in case no swap has been performed
			if (!swap_done) break;
		}
	}
    
    ~AverageConstraint() {
    	delete[] in_;
		delete[] wgt_;
		delete[] sort_idx_;
    }
    
	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
#ifdef AVG_NO_INCREMENTAL_
		// build the propagation demon
		Demon* propd0 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraint::PropagateNoIncrRight, "PropagateNoIncrRight");
		Demon* propd1 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraint::PropagateNoIncrLeft, "PropagateNoIncrLeft");
		// post on the constraint output and inputs
		out_->WhenRange(propd0);
		out_->WhenRange(propd1);
		for(int i = 0; i < size_; ++i) {
			in_[i]->WhenRange(propd0);
			in_[i]->WhenRange(propd1);
		}		
#else
		// build the propagation demon
		Demon* propd0 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraint::PropagateMaxAvg, "PropagateMaxAvg");
		Demon* propd1 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraint::PropagateMinAvg, "PropagateMinAvg");
      // post on constraint inputs
		for(int i = 0; i < size_; ++i) {
			in_[i]->WhenRange(MakeConstraintDemon1(solver(), this,
									&AverageConstraint::VarChanged, "VarChanged", i));
			in_[i]->WhenRange(propd0);
			in_[i]->WhenRange(propd1);
		}
		// post on constraint output
		out_->WhenRange(MakeConstraintDemon0(solver(), this,
							&AverageConstraint::AvgChanged, "AvgChanged"));
		out_->WhenRange(propd0);
		out_->WhenRange(propd1);
#endif
	}
   
	// safe division: in an average, the denominator may be 0 only if the
	// numerator is 0 too; hence, if the denominator is zero we can safely
	// return 0 as a result.
	#define SAFEDIV(X,Y) ((X) == 0 ? 0 : (X)/(Y))
	
	// MAIN PROCEDURES:
	// Lower bounding configuration (the upper bounding one is dual):
	// 1) Insert all items with minimum quantity, i.e. min(x_i)
	// 2) Maximize quantity of items with minimum weight, as long as:
	//    (W + wgt_i * (max(x_i) - min(x_i))) / (X + (max(x_i) - min(x_i))) <
	//             W/D
	//    i.e. as long as:
	//    wgt_i < W/X
	// where W is the current total weight of the configuration and X is the 
	// current sum of x values.
	
	// Maximal Support (Residual) Configuration (the minimal one is dual):
	// 1) Insert all items with minimum quantity, i.e. min(x_i)
	// 2) Maximize quantity of items with maximum weight, as long as:
	//    wgt_i > min(avg)
	
	// The threshold element is the the first minimized item left at minimum
	// value in the maximal support configuration.
	
	// Based on min(avg), it is possible to compute:
	// 1) An upper bound on the elements left of (or including) the threshold
	// 2) A lower bound on the elements right of the threshold
	
	// ------------------------------------------------------------------------
	// Basic Propagation method: avg min to inputs
	
	void PropagateAvgMinToInput(int64 maxrconf_num, int64 maxrconf_den,
										int64 left_thid, double avg_lb) {
		// start by computing the unrounded minimum of the average variable
		double avg_min = DE_ROUND_DOWN(out_->Min());
		avg_min = avg_min < avg_lb ? avg_lb : avg_min;
		// avg_min = avg_min > avg_ub ? avg_ub : avg_min;
		
		// perform filtering on the weights lower than avg_min (those are
		// minimized in the maximal residual configuration)
		for(int i = 0; i <= left_thid; ++i) {
			// we have:
			// (othcontr_num + w_i * in_i) / (othcontr_den + in_i) >= avg_min
			// handle a zero denominator
			if (maxrconf_den == in_[i]->Min())
				if (avg_min <= 0) in_[i]->SetMax(0);
				else solver()->Fail();
			else {
				// compute maximum residual value
				double ms = (maxrconf_num - maxrconf_den * avg_min);
				// compute weight difference (reduced weight) for this element
				double dwgt = wgt_[i] - avg_min;
				// compute and apply bound
				double ub = -ms/dwgt + in_[i]->Min();
				in_[i]->SetMax(floor(ub));
				// early stop condition				
				if (-ms/dwgt + dom_min_ >= dom_max_) break;
			}
		}
				
		// perform filtering on the weights higher than avg_min (those are
		// maximized in the maximal residual configuration)
		for (int i = size_-1; i > left_thid; --i) {
			// we have:
			// (othcontr_num + w_i * in_i) / (othcontr_den + in_i) >= avg_min
			// handle a zero denominator
			if (maxrconf_den == in_[i]->Max())
				if (avg_min <= 0); // do nothing
				else in_[i]->SetMin(1);
			else {
				// compute maximum residual value
				double ms = (maxrconf_num - maxrconf_den * avg_min);
				// compute weight difference for this element
				double dwgt = wgt_[i] - avg_min;
				// compute and apply bound
				double lb = -ms/dwgt + in_[i]->Max();
				in_[i]->SetMin(ceil(lb));
				// early stop condition
				if (-ms/dwgt + dom_max_ <= dom_min_) break;
			}
		}
	}
	
	// ------------------------------------------------------------------------
	// Basic Propagation method: avg max to inputs
	
	void PropagateAvgMaxToInput(int64 minrconf_num, int64 minrconf_den,
										int64 right_thid, double avg_ub) {
		// start by computing the unrounded maximum of the average variable
		double avg_max = DE_ROUND_UP(out_->Max());
		// avg_max = avg_max < avg_lb ? avg_lb : avg_max;
		avg_max = avg_max > avg_ub ? avg_ub : avg_max;
						
		// perform filtering on the weights higher than avg_max (those are
		// minimized in the minimmal residual configuration)
		for(int i = size_-1; i >= right_thid; --i) {
			// we have:
			// (othcontr_num + w_i * in_i) / (othcontr_den + in_i) <= avg_max
			// handle a zero denominator
			if (minrconf_den == in_[i]->Min())
				if (avg_max >= 0) in_[i]->SetMax(0);
				else solver()->Fail();
			else {
				// compute minimum residual value
				double ms = (minrconf_num - minrconf_den * avg_max);
				// compute weight difference for this element
				double dwgt = wgt_[i] - avg_max;
				// compute and apply bound
				double ub = -ms/dwgt + in_[i]->Min();
				in_[i]->SetMax(floor(ub));
				// early stop condition
				if (-ms/dwgt + dom_min_ >= dom_max_) break;
			}
		}
		
		// perform filtering on the weights lower than avg_max (those are
		// maximized in the minimmal residual configuration)
		for(int i = 0; i < right_thid; ++i) {
			// we have:
			// (othcontr_num + w_i * in_i) / (othcontr_den + in_i) <= avg_max
			// handle a zero denominator
			if (minrconf_den == in_[i]->Max())
				if (avg_max >= 0); // do nothing
				else in_[i]->SetMax(1);
			else {
				// compute minimum residual value
				double ms = (minrconf_num - minrconf_den * avg_max);
				// compute weight difference for this element
				double dwgt = wgt_[i] - avg_max;
				// compute and apply bound
				double lb = -ms/dwgt + in_[i]->Max();
				in_[i]->SetMin(ceil(lb));
				// early stop condition
				if (-ms/dwgt + dom_max_ <= dom_min_) break;
			}
		}									
	}
	
	
	// ------------------------------------------------------------------------
	// Non incremental Propagation: avg-LB and avg-min to inputs
	
#undef DEBUG_ON
#define DEBUG_ON false
	void PropagateNoIncrLeft() {
		DBG(cerr << "PropagateNoIncrLeft" << endl;)
		
		// ENFORCE LOWER BOUND ON AVG VARIABLE
		// Compute lower bounding configuration
		double lbconf_num = 0, lbconf_den = 0;	
		for (int i = 0; i < size_; ++i) { // include all elements, minimized
			lbconf_num += wgt_[i] * in_[i]->Min();
			lbconf_den += in_[i]->Min();
		}
		for (int i = 0; i < size_; ++i) { // maximize left-most elements
			if (wgt_[i] < SAFEDIV(lbconf_num, lbconf_den)) {
				lbconf_num += wgt_[i] * (in_[i]->Max() - in_[i]->Min());
				lbconf_den += (in_[i]->Max() - in_[i]->Min());
			}
		}
		// compute and apply lower bound
		double avg_lb = SAFEDIV(lbconf_num, lbconf_den);
		out_->SetMin(ROUND(avg_lb));
		
		// CHECK IF FURTHER PROPAGATION IS NEEDED
		if (out_->Min() <= ROUND(avg_lb)) {
			DBG(cerr << "cst status after prop.: \t" << this << endl;)
			DBG(cerr << "no further propagation" << endl;)
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateNoIncrLeft -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		
		// PROPAGATE MIN AVG ON THE INPUT VARIABLES
		// compute maximal support (residual) configuration
		double maxrconf_num = 0, maxrconf_den = 0;
		int left_thid = -1;
		for(int i = 0; i < size_; ++i) {
			if (wgt_[i] < out_->Min() ) {
				// include the item, minimized
				maxrconf_num += wgt_[i] * in_[i]->Min();
				maxrconf_den += in_[i]->Min();
				// update the threshold
				left_thid = i;
			}
			else {
				// include the item, maximized
				maxrconf_num += wgt_[i] * in_[i]->Max();
				maxrconf_den += in_[i]->Max();
			}
		}
				
		// Propagata Avg Min to inputs
		PropagateAvgMinToInput(maxrconf_num, maxrconf_den, left_thid, avg_lb);
		
		// print modified state
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// ------------------------------------------------------------------------
	// Non incremental propagation: avg-UB and avg-max to inputs

#undef DEBUG_ON
#define DEBUG_ON false
	void PropagateNoIncrRight() {
		DBG(cerr << "PropagateNoIncrRight" << endl;)
		
		// ENFORCE UPPER BOUND ON AVG VARIABLE
		// Compute upper bounding configuration
		double ubconf_num = 0, ubconf_den = 0;	
		for (int i = 0; i < size_; ++i) { // include all elements, minimized
			ubconf_num += wgt_[i] * in_[i]->Min();
			ubconf_den += in_[i]->Min();
		}
		for (int i = size_-1; i >= 0; --i) { // maximize right-most elements
			if (wgt_[i] > SAFEDIV(ubconf_num, ubconf_den)) {
				ubconf_num += wgt_[i] * (in_[i]->Max() - in_[i]->Min());
				ubconf_den += (in_[i]->Max() - in_[i]->Min());
			}
		}
		// compute and apply lower bound
		double avg_ub = SAFEDIV(ubconf_num, ubconf_den);
		out_->SetMax(ROUND(avg_ub));
		
		// CHECK IF FURTHER PROPAGATION IS NEEDED
		if (out_->Max() >= ROUND(avg_ub)) {
			DBG(cerr << "cst status after prop.: \t" << this << endl;)
			DBG(cerr << "no further propagation" << endl;)
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateNoIncrRight -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		
		// PROPAGATE MIN AVG ON THE INPUT VARIABLES
		// compute minimal support (residual) configuration
		double minrconf_num = 0, minrconf_den = 0;
		int right_thid = size_;
		for(int i = size_-1; i >= 0; --i) {
			if (wgt_[i] > out_->Max() ) {
				// include the item, minimized
				minrconf_num += wgt_[i] * in_[i]->Min();
				minrconf_den += in_[i]->Min();
				// update the threshold
				right_thid = i;
			}
			else {
				// include the item, maximized
				minrconf_num += wgt_[i] * in_[i]->Max();
				minrconf_den += in_[i]->Max();
			}
		}
				
		// Propagata Avg Min to inputs
		PropagateAvgMaxToInput(minrconf_num, minrconf_den, right_thid, avg_ub);
		
		// print modified state
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}
#undef DEBUG_ON
#define DEBUG_ON true

	// ------------------------------------------------------------------------
	// Methods to update the inclusion threshold of the lower/upper bound and
	// min/max residual configurations. The methods also perform propagation
	// on the average variable
		
	// update the left threshold and the lower bounding/ maximal residual conf.
	// the method also perform propagation on the min of the average variable
#undef DEBUG_ON
#define DEBUG_ON false
	void UpdateLTandPropagate() {
		DBG(cerr << "UpdateLTandPropagate" << endl;)
		// init num/den for the configurations to be updated
		int64 new_lb_num = avg_lb_num_.Value();
		int64 new_lb_den = avg_lb_den_.Value();
		int64 new_maxr_num = avg_maxr_num_.Value();
		int64 new_maxr_den = avg_maxr_den_.Value();
		
		// start the update
		int temp_idx = avg_left_thid_.Value();
		for(int i = temp_idx+1; i < size_; ++i) {
			double cur_lb = SAFEDIV((double) new_lb_num, new_lb_den);
			if (wgt_[i] < ROUND(cur_lb) ||
				wgt_[i] < DE_ROUND_DOWN(out_->Min())) {
				// update lower bounding configuration (maximize the item, which
				// was previously minimized
				new_lb_num += wgt_[i] * (in_[i]->Max() - in_[i]->Min());
				new_lb_den += (in_[i]->Max() - in_[i]->Min());
				// update maximal residual configuration (minimize the item, which
				// was previously maximized
				new_maxr_num += wgt_[i] * (in_[i]->Min() - in_[i]->Max());
				new_maxr_den += (in_[i]->Min() - in_[i]->Max());
				// update the threshold index
				temp_idx = i;
			}
			else break;
		}
		
		// perform propagation on the minimum of the average variable
		double avg_lb = SAFEDIV((double) new_lb_num, new_lb_den);
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		out_->SetMin(ROUND(avg_lb));
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
		
		// store new values
		if (avg_left_thid_.Value() != temp_idx) {
			avg_lb_num_.SetValue(solver(), new_lb_num);
			avg_lb_den_.SetValue(solver(), new_lb_den);
			avg_maxr_num_.SetValue(solver(), new_maxr_num);
			avg_maxr_den_.SetValue(solver(), new_maxr_den);		
			avg_left_thid_.SetValue(solver(), temp_idx);
		}
		
	}
#undef DEBUG_ON
#define DEBUG_ON true

	// update the right threshold and the upper bounding/ minima residual conf.
	// the method also perform propagation on the max of the average variable
#undef DEBUG_ON
#define DEBUG_ON false
	void UpdateRTandPropagate() {
		DBG(cerr << "UpdateRTandPropagate" << endl;)
		// init num/den for the configurations to be updated
		int64 new_ub_num = avg_ub_num_.Value();
		int64 new_ub_den = avg_ub_den_.Value();
		int64 new_minr_num = avg_minr_num_.Value();
		int64 new_minr_den = avg_minr_den_.Value();
		
		// start the update
		int temp_idx = avg_right_thid_.Value();
		for(int i = temp_idx-1; i >= 0; --i) {
			double cur_ub = SAFEDIV((double) new_ub_num, new_ub_den);
			if (wgt_[i] > ROUND(cur_ub) ||
				wgt_[i] > DE_ROUND_UP(out_->Max())) {
				// update upper bounding configuration (maximize the item, which
				// was previously minimized
				new_ub_num += wgt_[i] * (in_[i]->Max() - in_[i]->Min());
				new_ub_den += (in_[i]->Max() - in_[i]->Min());
				// update minimal residual configuration (minimize the item, which
				// was previously maximized
				new_minr_num += wgt_[i] * (in_[i]->Min() - in_[i]->Max());
				new_minr_den += (in_[i]->Min() - in_[i]->Max());
				// update the threshold index
				temp_idx = i;
			}
			else break;
		}
		
		// perform propagation on the minimum of the average variable
		double avg_ub = SAFEDIV((double) new_ub_num, new_ub_den);
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		out_->SetMax(ROUND(avg_ub));
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
		
		// store new values
		if (avg_right_thid_.Value() != temp_idx) {
			avg_ub_num_.SetValue(solver(), new_ub_num);
			avg_ub_den_.SetValue(solver(), new_ub_den);
			avg_minr_num_.SetValue(solver(), new_minr_num);
			avg_minr_den_.SetValue(solver(), new_minr_den);		
			avg_right_thid_.SetValue(solver(), temp_idx);
		}
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// -------------------------------------------------------------------------
	// Propagation methods
	
#undef DEBUG_ON
#define DEBUG_ON false
	void PropagateMaxAvg() {
		DBG(cerr << "PropagateMaxAvg" << endl;)

		// update the left inclusion threshold and propagate on the avg var
		UpdateRTandPropagate();		
		
		// compute current lower bound on the average
		double avg_ub = SAFEDIV((double) avg_ub_num_.Value(), avg_ub_den_.Value());
		// // compute current lower bound on the average
		// double avg_lb = SAFEDIV((double) avg_lb_num_.Value(), avg_lb_den_.Value());
		
		// perform propagation only in case it is scheduled
		if (max_prop_.Value() == false) return;
		// if the current minimum of the average variable is due to the lower
		// bound configuration, we can perform no pruining on the input vars
		//TODO now that the threshold updates are performed right before, the following code can be simplified
		if (out_->Max() >= ROUND(avg_ub)) {
			DBG(cerr << "trivial case: no propagation" << endl;)
			max_prop_.SetValue(solver(), false); // reset the propagation flag
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateMaxAvg -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		
		PropagateAvgMaxToInput(avg_minr_num_.Value(), avg_minr_den_.Value(),
									avg_right_thid_.Value(), avg_ub);
		
		// reset the propagation schedule flag
		max_prop_.SetValue(solver(), false);
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}
#undef DEBUG_ON
#define DEBUG_ON true

#undef DEBUG_ON
#define DEBUG_ON false
	void PropagateMinAvg() {
		DBG(cerr << "PropagateMinAvg" << endl;)

		// update the left inclusion threshold and propagate on the avg var
		UpdateLTandPropagate();
		
		// // compute current lower bound on the average
		// double avg_ub = SAFEDIV((double) avg_ub_num_.Value(), avg_ub_den_.Value());
		// compute current lower bound on the average
		double avg_lb = SAFEDIV((double) avg_lb_num_.Value(), avg_lb_den_.Value());
		
		// perform propagation only in case it is scheduled
		if (min_prop_.Value() == false) return;
		// if the current minimum of the average variable is due to the lower
		// bound configuration, we can perform no pruining on the input vars
		if (out_->Min() <= ROUND(avg_lb)) {
			DBG(cerr << "trivial case: no propagation" << endl;)
			min_prop_.SetValue(solver(), false); // reset the propagation flag
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateMinAvg -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
				
		PropagateAvgMinToInput(avg_maxr_num_.Value(), avg_maxr_den_.Value(),
										avg_left_thid_.Value(), avg_lb);

		// reset the propagation schedule flag
		min_prop_.SetValue(solver(), false);
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// void PropagateINOUT() {
	// 	// TODO re-write PropagateINOUT
	// 	DBG(cerr << "cst before prop: " << this << endl;)
	// 	PropagateMinAvg();
	// 	PropagateMaxAvg();
	// 	DBG(cerr << "cst after prop: " << this << endl;)
	// }
	
	/**
	 * @brief Propagation method
	 */
#undef DEBUG_ON
#define DEBUG_ON false
	void InitialPropagate() {
#ifdef AVG_NO_INCREMENTAL_
		PropagateNoIncrRight();
		PropagateNoIncrLeft();
#else
		DBG(cerr << "InitialPropagate" << endl;)
		// Initial setups for the notable configurations: all variables are
		// minimized for the lower/upper bound configurations, all variables are
		// maximized for the minimal/maximal residual configuration
		int64 init_lbub_num = 0;
		int64 init_lbub_den = 0;
		int64 init_minmaxr_num = 0;
		int64 init_minmaxr_den = 0;
		for (int i = 0; i < size_; ++i) {
		    init_lbub_num += wgt_[i] * in_[i]->Min();
		    init_lbub_den += in_[i]->Min();
		    init_minmaxr_num += wgt_[i] * in_[i]->Max();
		    init_minmaxr_den += in_[i]->Max();
		}
		
		// Initialize lower/upper bound configurations
		avg_lb_num_.SetValue(solver(), init_lbub_num);
		avg_lb_den_.SetValue(solver(), init_lbub_den);
		avg_ub_num_.SetValue(solver(), init_lbub_num);
		avg_ub_den_.SetValue(solver(), init_lbub_den);
		
		// Initialize minimal/maximal residual configurations
		avg_minr_num_.SetValue(solver(), init_minmaxr_num);
		avg_minr_den_.SetValue(solver(), init_minmaxr_den);
		avg_maxr_num_.SetValue(solver(), init_minmaxr_num);
		avg_maxr_den_.SetValue(solver(), init_minmaxr_den);
		
		// Initialize the left/rigt threshold (this is already done at
		// construction time, but must be repeated to correctly handle restarts)
		avg_left_thid_.SetValue(solver(), -1);
		avg_right_thid_.SetValue(solver(), size_);
		
		// perform all kind of propagation
		// UpdateLTandPropagate();
		// UpdateRTandPropagate();
		PropagateMinAvg();
		PropagateMaxAvg();
#endif
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	// -------------------------------------------------------------------------
	// Methods to managed variable changes, trigger threshold updates and
	// schedule delayed propagation
#undef DEBUG_ON
#define DEBUG_ON false
	void VarChanged(int i) {
		DBG(cerr << "VarChanged(" << i << ")" << endl;)
		//
		// STEP 1: udpdate contribution in all notable configurations
		//
				
		// process changes in the variable MAX		
		if (in_[i]->Max() != in_[i]->OldMax()) {
			int64 num_delta = wgt_[i] * (in_[i]->Max() - in_[i]->OldMax());
			int64 den_delta = in_[i]->Max() - in_[i]->OldMax();
			
			// update lower bound configuration
			if (i <= avg_left_thid_.Value()) {
				avg_lb_num_.SetValue(solver(), avg_lb_num_.Value() + num_delta);
				avg_lb_den_.SetValue(solver(), avg_lb_den_.Value() + den_delta);
			}
			// update upper bound configuration
			if (i >= avg_right_thid_.Value()) {
				avg_ub_num_.SetValue(solver(), avg_ub_num_.Value() + num_delta);
				avg_ub_den_.SetValue(solver(), avg_ub_den_.Value() + den_delta);
			}
			// update maximal residual configuration
			if (i > avg_left_thid_.Value()) {
				avg_maxr_num_.SetValue(solver(), avg_maxr_num_.Value() + num_delta);
				avg_maxr_den_.SetValue(solver(), avg_maxr_den_.Value() + den_delta);
			}
			// update minimal residual configuration
			if (i < avg_right_thid_.Value()) {
				avg_minr_num_.SetValue(solver(), avg_minr_num_.Value() + num_delta);
				avg_minr_den_.SetValue(solver(), avg_minr_den_.Value() + den_delta);
			}
		}

		// process changes in the variable MIN
		if (in_[i]->Min() != in_[i]->OldMin()) {
			int64 num_delta = wgt_[i] * (in_[i]->Min() - in_[i]->OldMin());
			int64 den_delta = in_[i]->Min() - in_[i]->OldMin();
			
			// update lower bound configuration
			if (i > avg_left_thid_.Value()) {
				avg_lb_num_.SetValue(solver(), avg_lb_num_.Value() + num_delta);
				avg_lb_den_.SetValue(solver(), avg_lb_den_.Value() + den_delta);				
			}
			// update upper bound configuration
			if (i < avg_right_thid_.Value()) {
				avg_ub_num_.SetValue(solver(), avg_ub_num_.Value() + num_delta);
				avg_ub_den_.SetValue(solver(), avg_ub_den_.Value() + den_delta);
			}
			// update maximal residual configuration
			if (i <= avg_left_thid_.Value()) {
				avg_maxr_num_.SetValue(solver(), avg_maxr_num_.Value() + num_delta);
				avg_maxr_den_.SetValue(solver(), avg_maxr_den_.Value() + den_delta);
			}
			// update minimal residual configuration
			if (i >= avg_right_thid_.Value()) {
				avg_minr_num_.SetValue(solver(), avg_minr_num_.Value() + num_delta);
				avg_minr_den_.SetValue(solver(), avg_minr_den_.Value() + den_delta);
			}	
		}
				
		//
		// STEP 2: update thresholds, perform avg var propagation and schedule
		// delayed propagation
		//
				
		// changes for avg_left_thid_ variable below the left threshold
		if (i <= avg_left_thid_.Value()) {
			if (in_[i]->Max() != in_[i]->OldMax()) {
				// UpdateLTandPropagate();
				DBG(cerr << "scheduling avg max propagation" << endl;)
				max_prop_.SetValue(solver(), true);
			}
			if (in_[i]->Min() != in_[i]->OldMin()) {
				// UpdateRTandPropagate();
				DBG(cerr << "scheduling avg min propagation" << endl;)
				min_prop_.SetValue(solver(), true);
			}
		}
		
		// changes for a variable between the thresholds
		if (i > avg_left_thid_.Value() && i < avg_right_thid_.Value()) {
			if (in_[i]->Min() != in_[i]->OldMin()) {
				// UpdateLTandPropagate();
				// UpdateRTandPropagate();
			}
			if (in_[i]->Max() != in_[i]->OldMax()) {
				DBG(cerr << "scheduling avg max propagation" << endl;)
				max_prop_.SetValue(solver(), true);
				DBG(cerr << "scheduling avg min propagation" << endl;)
				min_prop_.SetValue(solver(), true);
			}
		}
		
		// changes for a variable above the right threshold
		if (i >= avg_right_thid_.Value()) {
			if (in_[i]->Max() != in_[i]->OldMax()) {
				// UpdateRTandPropagate();
				DBG(cerr << "scheduling avg min propagation" << endl;)
				min_prop_.SetValue(solver(), true);
			}
			if (in_[i]->Min() != in_[i]->OldMin()) {
				// UpdateLTandPropagate();
				DBG(cerr << "scheduling avg max propagation" << endl;)
				max_prop_.SetValue(solver(), true);
			}
		}
	}
#undef DEBUG_ON
#define DEBUG_ON true

#undef DEBUG_ON
#define DEBUG_ON false
	void AvgChanged() {
		DBG(cerr << "AvgChanged" << endl;)
		
		// process changes of the average variable MAX
		if (out_->Max() != out_->OldMax()) {
			// update the right threshold and schedule delayed propagation from
			// max avg to input variables
			// NOTE: sometimes, no propagation is necessary. This is checked by
			//       the PropagateMaxAvg method
			// UpdateRTandPropagate();
			DBG(cerr << "scheduling avg max propagation" << endl;)
			max_prop_.SetValue(solver(), true);
		}

		// process changes of the average variable MIN
		if (out_->Min() != out_->OldMin()) {
			// update the left threshold and schedule delayed propagation from
			// min avg to input variables
			// NOTE: sometimes, no propagation is necessary. This is checked by
			//       the PropagateMaxAvg method
			// UpdateLTandPropagate();
			DBG(cerr << "scheduling avg min propagation" << endl;)
			min_prop_.SetValue(solver(), true);
		}
	}
#undef DEBUG_ON
#define DEBUG_ON true
	
	
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		
		// append output
		out.append(out_->DebugString());
		out.append(" = AVG(");

		// append arguments
		for (int i = 0; i < size_; ++i) {
         if (i > 0) out.append(", ");
			StringAppendF(&out, "%d", wgt_[sort_idx_[i]]);
			out.append("*");
			out.append(in_[sort_idx_[i]]->DebugString());
		}
		
		// close string and return
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(type_, this);
        visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
                                                out_);
        for(int i = 0; i < size_; ++i)
            visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												   in_[i]);
		visitor->EndVisitConstraint(type_, this);
	}    

};

//
// Define static type attribute
//
char AverageConstraint::type_[] = "AverageCst";


//
// Functions to build an average constraint
//

Constraint* MakeAverageCst(Solver* const s, 
                        int size,
                        const IntVar* const* inputs,
                        IntVar* const output,
                        int64 const* weights,
                        string name) {
    CHECK(inputs != NULL) << "NULL avg cst input array, maybe a bad cast";
    CHECK(weights != NULL) << "NULL avg cst weight array, maybe a bad cast";
    CHECK(output != NULL) << "NULL avg cst output, maybe a bad cast";
    CHECK_EQ(s, output->solver());
    for (int i = 0; i < size; ++i) {
        CHECK_EQ(s, inputs[i]->solver());
        CHECK(inputs[i]->Min() >= 0) << "inputs of an average cst must be non-negative";
    }
    return s->RevAlloc(new AverageConstraint(s, size, inputs, output,
                                             weights, name));
}


Constraint* MakeAverageCst(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntVar* const output,
                           const std::vector<int64>& weights,
                           string name) {
    return MakeAverageCst(s, inputs.size(), inputs.data(), output,
                          weights.data(), name);
}


//==============================================================================
// Average Constraint with Variable Term Values
// This constraint enforces: y = (\sum_i v_i * w_ i) / (\sum_i w_i)
// All w_i (weights) are assumed to be non negative!
//==============================================================================

class AverageConstraintVTV : public Constraint {
private:
	// internal copy of the weight array
	IntVar** wgt_;
	// internal pointers to the input variables
	IntVar** in_;
	// internal pointer output variable
	IntVar* const out_;
	// number of inputs
	int size_;
	// name
	string name_;
	// constraint type
	static char type_[];
	
	// information abour the input data, exploited to speed up propagation
	// minimum domain value
	int64 dom_max_;
	// maximum domain value
	int64 dom_min_;
		
public:
	AverageConstraintVTV(Solver* const s,
						 int size,
						 const IntVar* const* inputs,
						 IntVar* const output,
						 const IntVar* const* weights,
						 string name)
	: Constraint(s), size_(size), out_(output), name_(name)
	{
		// init array of term-weights
		in_ = new IntVar*[size_];
		memcpy(in_, inputs, size_ * sizeof(*inputs));
		// init array of term-values
		wgt_ = new IntVar*[size_];
		memcpy(wgt_, weights, size_ * sizeof(*weights));

		// find min/max weight and min/max doman value
		dom_min_ = INT64_MAX;
		dom_max_ = INT64_MIN;
		for (int i = 0; i < size_; ++i) {
			dom_min_ = min(dom_min_, in_[i]->Min());
			dom_max_ = max(dom_max_, in_[i]->Max());
		}
	}
    
    ~AverageConstraintVTV() {}
    
	/**
	 * @brief Post method; the constraints enforces bound consistency
	 */
	void Post() {
		// build the propagation demon
		Demon* propd0 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraintVTV::PropagateNoIncrRight, "PropagateNoIncrRight");
		Demon* propd1 = MakeDelayedConstraintDemon0(solver(), this,
			&AverageConstraintVTV::PropagateNoIncrLeft, "PropagateNoIncrLeft");
		// post on the constraint output and inputs
		out_->WhenRange(propd0);
		out_->WhenRange(propd1);
		for(int i = 0; i < size_; ++i) {
			in_[i]->WhenRange(propd0);
			in_[i]->WhenRange(propd1);
		}
	}
   
	// safe division: in an average, the denominator may be 0 only if the
	// numerator is 0 too; hence, if the denominator is zero we can safely
	// return 0 as a result.
	#define SAFEDIV(X,Y) ((X) == 0 ? 0 : (X)/(Y))
		
	// ------------------------------------------------------------------------
	// Non incremental Propagation: avg-LB and avg-min to inputs
	
	void PropagateNoIncrLeft() {
		DBG(cerr << "PropagateNoIncrLeft" << endl;)
				
		// sort elements by increasing min(v_i) (using insertion sort: since
		// the array should be almost order, this a very reasonable choice)
		for (int j = 1; j < size_; ++j) {
			// make a hole where the current element is
			IntVar* twgt = wgt_[j];
			IntVar* tin = in_[j];
			int i = j;
			// shift previous elements forward as long as they have greater
			// minimum wgt
			while (i > 0 && wgt_[i-1]->Min() > twgt->Min() ) {
				wgt_[i] = wgt_[i-1];
				in_[i] = in_[i-1];
				--i;
			}
			// insert the current item
			wgt_[i] = twgt;
			in_[i] = tin;
		}
		
		DBG(for (int i = 1; i < size_; ++i)
		assert(wgt_[i-1]->Min() < wgt_[i]->Min());)
		
		// ENFORCE LOWER BOUND ON AVG VARIABLE		
		// Compute lower bounding configuration
		double lbconf_num = 0, lbconf_den = 0;	
		for (int i = 0; i < size_; ++i) { // include all elements, minimized
			lbconf_num += wgt_[i]->Min() * in_[i]->Min();
			lbconf_den += in_[i]->Min();
		}
		// maximize left-most elements
		for (int i = 0; i < size_; ++i) {
			if (wgt_[i]->Min() < SAFEDIV(lbconf_num, lbconf_den)) {
				lbconf_num += wgt_[i]->Min() * (in_[i]->Max() - in_[i]->Min());
				lbconf_den += (in_[i]->Max() - in_[i]->Min());
			}
		}
		// compute and apply lower bound
		double avg_lb = SAFEDIV(lbconf_num, lbconf_den);
		out_->SetMin(ROUND(avg_lb));
		
		// CHECK IF FURTHER PROPAGATION IS NEEDED
		if (out_->Min() <= ROUND(avg_lb)) {
			DBG(cerr << "cst status after prop.: \t" << this << endl;)
			DBG(cerr << "no further propagation" << endl;)
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateNoIncrLeft -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		
		// de-round the average maximum
		double avg_min = DE_ROUND_DOWN(out_->Min());
		
		// PROPAGATE MIN AVG ON THE INPUT VARIABLES
		// compute maximal support (residual) configuration
		double maxrconf_val = 0;
		double maxrconf_in = 0;
		for(int i = 0; i < size_; ++i) {
			double rel_wgt = (wgt_[i]->Max()-avg_min);
			if (wgt_[i]->Max() < avg_min ) {
				// include the item, minimized
				maxrconf_val += rel_wgt * in_[i]->Min();
				maxrconf_in += in_[i]->Min();
			}
			else {
				// include the item, maximized
				maxrconf_val += rel_wgt * in_[i]->Max();
				maxrconf_in += in_[i]->Max();
			}
		}
		
		// I can safely assume that "\sum_i wgt_[i] > 0". In fact, the sum is
		// 0 only if all weights are bound to 0. In such a case, the average
		// must be 0 as well: if this is not the case, a fail occurs BEFORE
		// reaching this code section
			
		// propagate on each term
		for (int i = 0; i < size_; ++i) {
			// STEP 1: remove the contribution of term "i" from the maximal
			// support configuration
			double rel_wgt = wgt_[i]->Max()-avg_min;
			double theta = - (maxrconf_val - rel_wgt * in_[i]->Min());
			double theta_in = maxrconf_in - in_[i]->Min();
			if (wgt_[i]->Max() > avg_min) {
				theta += rel_wgt * (in_[i]->Max() - in_[i]->Min());
				theta_in -= (in_[i]->Max() - in_[i]->Min());
			}
			// Now we have: "in_[i] * (wgt_[i] - avg_min) >= theta"
			// Hence we just need to perform filtering on the inequality
			
			// STEP 2: filter on wgt_[i] variable
			if (theta > 0) {
				// a LOWER BOUND is obtained for MAXIMUM wgt_[i]
				if (in_[i]->Max() == 0) solver()->Fail();
				else wgt_[i]->SetMin(ceil(theta/in_[i]->Max() + avg_min));
			}
			else if (theta_in == 0) {
				// if the avg min is non positive, we can set in_[i] to 0
				if (avg_min <= 0 && in_[i]->Min() == 0); // do nothing
				// we get a LOWER BOUND, non depending on in_[i]
				else wgt_[i]->SetMin(ceil(avg_min));
			}
			else {
				// a LOWER BOUND is obtained for MINIMUM wgt_[i]
				if (in_[i]->Min() == 0); // do nothing
				else wgt_[i]->SetMin(ceil(theta/in_[i]->Min() + avg_min));
			}
			
			// STEP 3: filter on the in_[i] variable
			// let "wgt_[i] - out_" be the "relative weight" of the term
			if (theta > 0) {
				// if the relative weight CAN BE POSITIVE, we obtain a LOWER
				// BOUND for MAXIMUM wgt_[i]
				if (wgt_[i]->Max() - avg_min > 0)
					in_[i]->SetMin( ceil(theta/(wgt_[i]->Max() - avg_min)) );
				// otherwise, if the relative weight is NON-POSITIVE, we get a
				// strictly negative upper bound on in_[i] and an INFEASIBILITY
				else solver()->Fail();
			}
			// FIXME do not check theta == 0, check the sum of weights instead
			else if (theta_in == 0) {
				// if avg min is non positive, we can set in_[i] to 0
				if (avg_min <= 0); // do nothing
				else in_[i]->SetMin(1);
			}
			else {
				// if the relative weight CAN BE NON-NEGATIVE, we get a
				// non-positive lower bound on in_[i] and hence NO PROPAGATION.
				if (wgt_[i]->Max() - avg_min >= 0); // do nothing
				// otherwise, if the relative weight is STRICTLY NEGATIVE we
				// obtain an UPPER BOUND for MAXIMUM wgt_[i]
				else
					in_[i]->SetMax( floor(theta/(wgt_[i]->Max() - avg_min)) );
			}	
		}
		
		// print modified state
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}
	
	// ------------------------------------------------------------------------
	// Non incremental propagation: avg-UB and avg-max to inputs
	
	void PropagateNoIncrRight() {
		DBG(cerr << "PropagateNoIncrRight" << endl;)
		
		// sort elements by increasing max(v_i) (using insertion sort: since
		// the array should be almost ordered, this a very reasonable choice)
		for (int j = 1; j < size_; ++j) {
			// make a hole where the current element is
			IntVar* twgt = wgt_[j];
			IntVar* tin = in_[j];
			int i = j;
			// shift previous elements forward as long as they have greater
			// minimum wgt
			while (i > 0 && wgt_[i-1]->Max() > twgt->Max() ) {
				wgt_[i] = wgt_[i-1];
				in_[i] = in_[i-1];
				--i;
			}
			// insert the current item
			wgt_[i] = twgt;
			in_[i] = tin;
		}
		
		// ENFORCE UPPER BOUND ON AVG VARIABLE
		// Compute upper bounding configuration
		double ubconf_num = 0, ubconf_den = 0;	
		for (int i = 0; i < size_; ++i) { // include all elements, minimized
			ubconf_num += wgt_[i]->Max() * in_[i]->Min();
			ubconf_den += in_[i]->Min();
		}
		for (int i = size_-1; i >= 0; --i) { // maximize right-most elements
			if (wgt_[i]->Max() > SAFEDIV(ubconf_num, ubconf_den)) {
				ubconf_num += wgt_[i]->Max() * (in_[i]->Max() - in_[i]->Min());
				ubconf_den += (in_[i]->Max() - in_[i]->Min());
			}
		}
		// compute and apply lower bound
		double avg_ub = SAFEDIV(ubconf_num, ubconf_den);
		out_->SetMax(ROUND(avg_ub));
		
		// CHECK IF FURTHER PROPAGATION IS NEEDED
		if (out_->Max() >= ROUND(avg_ub)) {
			DBG(cerr << "cst status after prop.: \t" << this << endl;)
			DBG(cerr << "no further propagation" << endl;)
			return;
		}
		// otherwise, we can perform filtering on the input variables
		DBG(cerr << "PropagateNoIncrRight -- HEAVY PROPAGATION" << endl;)
		DBG(cerr << "cst status: \t\t\t" << this << endl;)
		
		// de-round the average maximum
		double avg_max = DE_ROUND_UP(out_->Max());
		
		// PROPAGATE MIN AVG ON THE INPUT VARIABLES
		// compute minimal support (residual) configuration
		double minrconf_val = 0;
		double minrconf_in = 0;
		for(int i = size_-1; i >= 0; --i) {
			double rel_wgt = (wgt_[i]->Min()-avg_max);
			if (wgt_[i]->Min() > avg_max ) {
				// include the item, minimized
				minrconf_val += rel_wgt * in_[i]->Min();
				minrconf_in += in_[i]->Min();
			}
			else {
				// include the item, maximized
				minrconf_val += rel_wgt * in_[i]->Max();
				minrconf_in += in_[i]->Max();
			}
		}
				
		// I can safely assume that "\sum_i wgt_[i] > 0". In fact, the sum is
		// 0 only if all weights are bound to 0. In such a case, the average
		// must be 0 as well: if this is not the case, a fail occurs BEFORE
		// reaching this code section
		
		// propagate on each term
		for (int i = 0; i < size_; ++i) {
			// STEP 1: remove the contribution of term "i" from the minimal
			// support configuration
			double rel_wgt = (wgt_[i]->Min()-avg_max);
			double theta = - (minrconf_val - rel_wgt * in_[i]->Min());
			double theta_in = minrconf_in - in_[i]->Min();
			if (wgt_[i]->Min() <= avg_max) {
				theta += rel_wgt * (in_[i]->Max() - in_[i]->Min());
				theta_in -= (in_[i]->Max() - in_[i]->Min());
			}
			// Now we have: "in_[i] * (wgt_[i] - avg_max) <= theta"
			// Hence we just need to perform filtering on the inequality
			
			// STEP 2: filter on wgt_[i] variable
			if (theta < 0) {
				// an UPPER BOUND is obtained for MAXIMUM wgt_[i]
				if (in_[i]->Max() == 0) solver()->Fail();
				else wgt_[i]->SetMax(floor(theta/in_[i]->Max() + avg_max));
			}
			else if (theta_in == 0) {
				// if the avg max is non negative, we can set in_[i] to 0
				if (avg_max >= 0 && in_[i]->Min() == 0); // do nothing
				// we get an UPPER BOUND, non depending on in_[i]
				else wgt_[i]->SetMax(floor(avg_max));
			}
			else {
				// an UPPER BOUND is obtained for MINIMUM wgt_[i]
				if (in_[i]->Min() == 0); // do nothing
				else wgt_[i]->SetMax(floor(theta/in_[i]->Min() + avg_max));
			}
			
			// STEP 3: filter on the in_[i] variable
			// let "wgt_[i] - out_" be the "relative weight" of the term
			if (theta < 0) {
				// if the relative weight CAN BE NEGATIVE, we obtain an LOWER
				// BOUND for MINIMUM wgt_[i]
				if (wgt_[i]->Min() - avg_max < 0)
					in_[i]->SetMin( ceil(theta/(wgt_[i]->Min() - avg_max)) );
				// otherwise, if the relative weight is NON-NEGATIVE, we get a
				// strictly negative upper bound on in_[i] and an INFEASIBILITY
				else solver()->Fail();
			}
			else if (theta_in == 0) {
				// if avg min is non negative, we can set in_[i] to 0
				if (avg_max >= 0); // do nothing
				else in_[i]->SetMin(1);
			}
			else {
				// if the relative weight CAN BE NON-POSITIVE, we get a
				// non-positive lower bound on in_[i] and hence NO PROPAGATION.
				if (wgt_[i]->Min() - avg_max <= 0); // do nothing
				// otherwise, if the relative weight is STRICTLY POSITIVE we
				// obtain an UPPER BOUND for MINIMUM wgt_[i]
				else
					in_[i]->SetMax( floor(theta/(wgt_[i]->Min() - avg_max)) );
			}	
		}
		
		// print modified state
		DBG(cerr << "cst status after prop.: \t" << this << endl;)
	}

	/**
	 * @brief Propagation method
	 */
	void InitialPropagate() {
		PropagateNoIncrRight();
		PropagateNoIncrLeft();
	}
		
	/**
	 * @brief Print debug string (this contains the neuron activity)
	 * @return the debug string
	 */
	string DebugString() const {
		string out;
		
		// append output
		out.append(out_->DebugString());
		out.append(" = AVG(");

		// append arguments
		for (int i = 0; i < size_; ++i) {
         if (i > 0) out.append(", ");
			out.append(wgt_[i]->DebugString());
			out.append("*");
			out.append(in_[i]->DebugString());
		}
		
		// close string and return
		out.append(")");
		return out;
	}
	
	// the accept method needs not to be virtual
	void Accept(ModelVisitor* const visitor) const {
		visitor->BeginVisitConstraint(type_, this);
        visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
                                                out_);
        for(int i = 0; i < size_; ++i)
            visitor->VisitIntegerExpressionArgument(ModelVisitor::kExpressionArgument,
												   in_[i]);
		visitor->EndVisitConstraint(type_, this);
	}    
};

//
// Define static type attribute
//
char AverageConstraintVTV::type_[] = "AverageCst";

//
// Functions to build an average constraint
//

Constraint* MakeAverageCst(Solver* const s, 
                        int size,
                        const IntVar* const* inputs,
                        IntVar* const output,
                        const IntVar* const* weights,
                        string name) {
    CHECK(inputs != NULL) << "NULL avg cst input array, maybe a bad cast";
    CHECK(weights != NULL) << "NULL avg cst weight array, maybe a bad cast";
    CHECK(output != NULL) << "NULL avg cst output, maybe a bad cast";
    CHECK_EQ(s, output->solver());
    for (int i = 0; i < size; ++i) {
        CHECK_EQ(s, inputs[i]->solver());
        CHECK(inputs[i]->Min() >= 0) << "inputs of an average cst must be non-negative";
    }
    return s->RevAlloc(new AverageConstraintVTV(s, size, inputs, output,
                                             weights, name));
}


Constraint* MakeAverageCst(Solver* s,
                           const std::vector<IntVar*>& inputs,
                           IntVar* const output,
                           const std::vector<IntVar*>& weights,
                           string name) {
    return MakeAverageCst(s, inputs.size(), inputs.data(), output,
                          weights.data(), name);
}

// //==============================================================================
// // Decision class to fix a set of variables. No actions is performed on btk
// //==============================================================================

// class MyFixValues : public Decision {
// public:
// 	MyFixValues(const std::vector<IntVar*>& vars, 
// 					const std::vector<int64>& vals)
// 					: vars_(vars), vals_(vals) {}
// 	virtual ~MyFixValues() {}
// 	virtual void Apply(Solver* const s) {
// 		for (int i = 0; i < vars_.size(); ++i)
// 			vars_[i]->SetValue(vals_[i]);
// 	}
// 	virtual void Refute(Solver* const s) {
// 	}
// 	virtual string DebugString() const {
// 		return "MyFixValues";
// 	}
// 	virtual void Accept(DecisionVisitor* const visitor) const {
// 		// visitor->VisitSetVariableValue(var_, value_);
// 	}

// private:
// 	std::vector<IntVar*> vars_;
// 	std::vector<int64> vals_;
// };

// Decision* MakeMyFixValues(Solver* s, const std::vector<IntVar*>& vars,
// 												const std::vector<int64>& vals) {
//   return s->RevAlloc(new MyFixValues(vars, vals));
// }

	
} // end of operations_research namespace