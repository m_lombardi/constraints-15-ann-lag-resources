// Copyright 2010-2011 Google
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Magic square problem
//
// Solves the problem where all numbers in an nxn array have to be different
// while the sums on diagonals, rows, and columns have to be the same.
// The problem is trivial for odd orders, but not for even orders.
// We do not handle odd orders with the trivial method here.

#include "base/commandlineflags.h"
#include "base/commandlineflags.h"
#include "base/integral_types.h"
#include "base/logging.h"
#include "base/stringprintf.h"
#include "constraint_solver/constraint_solver.h"

#include "neuron_cst.h"

using namespace operations_research;
using namespace std;


//==============================================================================
// Main program
//==============================================================================
	
class NCTestState {
public:
    vector<int64> in_lb;
    vector<int64> in_ub;
    int64 out_lb, out_ub;
};
    
/* some parameters  */
#define FREE_VAL -1000000
int prec = 1; // used in all the tests

// for the neuron constraints
int ninputs = 2;
double weights[] = {0.5, 0.5};
double bias = -0.6;
int in_defaults[] = {FREE_VAL, FREE_VAL};
int out_default = FREE_VAL;
    
void reset() {
    prec = 1;
    ninputs = 2;
    weights[0] = 0.5;
    weights[1] = 0.5;
    bias = -0.6;
    in_defaults[0] = FREE_VAL;
    in_defaults[1] = FREE_VAL;
    out_default = FREE_VAL;
}

NCTestState test_hardlim(int prec, int ninputs, double weights[],
                         double bias, int in_defaults[], int out_default)
{
    /* build the solver */
    Solver solver("A simple Neuron Cst test");
        
    /* build variables  */
    vector<IntVar*> inputs;
    solver.MakeIntVarArray(ninputs, 0, prec, "in", &inputs);
    IntVar* output = solver.MakeIntVar(-2*prec, 2*prec, string("out"));
        
    /* build neuron constraint */
    Constraint* neuron = MakeHardlimNeuron(&solver,
                                           ninputs,
                                           (IntExpr**) &inputs[0],
                                           output,
                                           bias,
                                           weights,
                                           prec);
        
    solver.AddConstraint(neuron);
    cout << "Initial Neuron Constraint state: " << neuron << endl;
        
    /* set some variables */
    for(int i = 0; i < ninputs; ++i) {
        if (in_defaults[i] != FREE_VAL) {
            Constraint* cst = solver.MakeEquality((IntExpr*) inputs[0],
                                                  in_defaults[i]);
            solver.AddConstraint(cst);
            cst->PostAndPropagate();
        }
    }
    if (out_default != FREE_VAL) {
        Constraint* cst = solver.MakeEquality(output, out_default);
        solver.AddConstraint(cst);
        cst->PostAndPropagate();
            
    }
        
    /* propagate the neuron constraint */
    neuron->PostAndPropagate();
    cout << "Neuron Constraint after propagation: " << neuron << endl;	
        
    /* search */
    NCTestState res;
    for(int i = 0; i < ninputs; ++i) {
        res.in_lb.push_back(inputs[i]->Min());
        res.in_ub.push_back(inputs[i]->Max());
    }
    res.out_lb = output->Min();
    res.out_ub = output->Max();
        
    return res;
}	


NCTestState test_tansig(int prec, int ninputs, double weights[],
                        double bias, int in_defaults[], int out_default)
{
    /* build the solver */
    Solver solver("A simple Neuron Cst test");
        
    /* build variables  */
    vector<IntVar*> inputs;
    solver.MakeIntVarArray(ninputs, 0, prec, "in", &inputs);
    IntVar* output = solver.MakeIntVar(-2*prec, 2*prec, string("out"));
        
    /* build neuron constraint */
    Constraint* neuron = MakeTansigNeuron(&solver,
                                          ninputs,
                                          (IntExpr**) &inputs[0],
                                          output,
                                          bias,
                                          weights,
                                          prec);
        
    solver.AddConstraint(neuron);
    cout << "Initial Neuron Constraint state: " << neuron << endl;
        
    /* set some variables */
    for(int i = 0; i < ninputs; ++i) {
        if (in_defaults[i] != FREE_VAL) {
            Constraint* cst = solver.MakeEquality((IntExpr*) inputs[0],
                                                  in_defaults[i]);
            solver.AddConstraint(cst);
            cst->PostAndPropagate();
        }
    }
    if (out_default != FREE_VAL) {
        Constraint* cst = solver.MakeEquality(output, out_default);
        solver.AddConstraint(cst);
        cst->PostAndPropagate();
    }
        
    /* propagate the neuron constraint */
    neuron->PostAndPropagate();
    cout << "Neuron Constraint after propagation: " << neuron << endl;	
        
    /* search */
    NCTestState res;
    for(int i = 0; i < ninputs; ++i) {
        res.in_lb.push_back(inputs[i]->Min());
        res.in_ub.push_back(inputs[i]->Max());
    }
    res.out_lb = output->Min();
    res.out_ub = output->Max();
        
    return res;
}
	
// class NNTestState {
// public:
//     vector<int64> in_lb;
//     vector<int64> in_ub;
//     vector<int64> out_lb;
//     vector<int64> out_ub;
// };
// 
// NNTestState test_2lff_network(int isize, int hsize, int osize,
// 							  int* i_min, int* i_max,
// 							  int* o_min, int* o_max,
// 							  double* wgt0, double* wgt1, int prec) {
// 							  	
//     /* build the solver */
//     Solver solver("A test for a 2-layer neural network");
//         
//     /* build variables  */
//     vector<IntVar*> inputs;
//     vector<IntVar*> activities;
//     vector<IntVar*> outputs;
//     solver.MakeIntVarArray(isize, -2*prec, 2*prec, "in", &inputs);
//     solver.MakeIntVarArray(hsize, -1000*prec, 1000*prec, "y", &activities);
//     solver.MakeIntVarArray(osize, -1000*prec, 1000*prec, "out", &outputs);
//         
//     /* build neural network constraint */
// 	Constraint* nn = MakeTwoLayerFFNetworkCst(&solver,
// 							 isize,
// 							 hsize, // number of hidden neurons
// 							 osize,
// 							 inputs.data(),
// 							 activities.data(),
// 							 outputs.data(),
// 							 wgt0,
// 							 wgt1,
// 							 ntype_purelin,
// 							 prec);	
// 	        
//     solver.AddConstraint(nn);
//     cout << "Initial Neural Network Constraint state: " << nn << endl;
// 	
// 	/* post the neural network constraint */
//     nn->Post();
//         
//     /* set some variables */
//     for(int i = 0; i < isize; ++i) {
//         Constraint* cst = solver.MakeBetweenCt(inputs[i], i_min[i], i_max[i]);
//         solver.AddConstraint(cst);
//         cst->PostAndPropagate();
//     }
// 	for (int k = 0; k < osize; ++k) {
//         Constraint* cst = solver.MakeBetweenCt(outputs[k], o_min[k], o_max[k]);
//         solver.AddConstraint(cst);
//         cst->PostAndPropagate();
//     }
//         
//     /* propagate the neural network constraint */
//     // nn->PostAndPropagate();
//     cout << "Neural Network Constraint after propagation: " << nn << endl;	
//         
//     /* search */
//     NNTestState res;
//     for(int i = 0; i < isize; ++i) {
//         res.in_lb.push_back(inputs[i]->Min());
//         res.in_ub.push_back(inputs[i]->Max());
//     }
// 	for (int k = 0; k < osize; ++k) {
//         res.out_lb.push_back(outputs[k]->Min());
//         res.out_ub.push_back(outputs[k]->Max());
//     }
// 
//     return res;							
// }
    
    
//
// Rounded product
//
    
class RPTestState {
public:
    int x_min, x_max;
    int y_min, y_max;
};
    
RPTestState test_rounded_prod(int x, int y, int bias, double k)
{
    /* build the solver */
    Solver solver("Test rounded product constraint");
        
    /* build variables  */
    IntVar* x_var = solver.MakeIntVar(-100, 100, string("x"));
    IntVar* y_var = solver.MakeIntVar(-100, 100, string("y"));
        
    /* apply bias */
    IntExpr* x_expr = x_var;
    if (bias != 0) x_expr = solver.MakeSum(x_var, bias);
        
    /* set some variables */
    if (x != FREE_VAL) {
        Constraint* cst_x = solver.MakeEquality(x_expr, x);
        solver.AddConstraint(cst_x);
        cst_x->PostAndPropagate();
    }
    if (y != FREE_VAL) {
        Constraint* cst_y = solver.MakeEquality(y_var, y);
        solver.AddConstraint(cst_y);
        cst_y->PostAndPropagate();
    }
        
    /* build neuron constraint */
    Constraint* cst = MakeRoundedProductConstraint(&solver, x_expr, y_var, k);
    solver.AddConstraint(cst);
    cout << "Initial constraint status: " << cst << endl;
        
    /* propagate the neuron constraint */
    cst->PostAndPropagate();
    cout << "Constraint after propagation: " << cst << endl;	
        
    /* search */
    RPTestState res;
    res.x_min = x_var->Min();
    res.x_max = x_var->Max();
    res.y_min = y_var->Min();
    res.y_max = y_var->Max();
    return res;        
}

//
// Rounded division
//
    
class RDTestState {
public:
	int x_min, x_max;
	int y_min, y_max;
	int z_min, z_max;
};
    
RDTestState test_rounded_div(int x, int y, int z)
{
	/* build the solver */
	Solver solver("Test rounded product constraint");

	/* build variables  */
	IntVar* x_var = solver.MakeIntVar(-100, 100, string("x"));
	IntVar* y_var = solver.MakeIntVar(0, 100, string("y"));
	IntVar* z_var = solver.MakeIntVar(-100, 100, string("z"));
    
	/* set some variables */
	if (x != FREE_VAL) {
	    Constraint* cst_x = solver.MakeEquality(x_var, x);
	    solver.AddConstraint(cst_x);
	    cst_x->PostAndPropagate();
	}
	if (y != FREE_VAL) {
	    Constraint* cst_y = solver.MakeEquality(y_var, y);
	    solver.AddConstraint(cst_y);
	    cst_y->PostAndPropagate();
	}
	if (z != FREE_VAL) {
	    Constraint* cst_z = solver.MakeEquality(z_var, z);
	    solver.AddConstraint(cst_z);
	    cst_z->PostAndPropagate();
	}
    
	/* build rounded division constraint */
	Constraint* cst = MakeRoundedDivisionConstraint(&solver, x_var, y_var, z_var);
	solver.AddConstraint(cst);
	cout << "Initial constraint status: " << cst << endl;
    
	/* propagate the division constraint */
	cst->PostAndPropagate();
	cout << "Constraint after propagation: " << cst << endl;	
    
	/* get variable status */
	RDTestState res;
	res.x_min = x_var->Min();
	res.x_max = x_var->Max();
	res.y_min = y_var->Min();
	res.y_max = y_var->Max();
	res.z_min = z_var->Min();
	res.z_max = z_var->Max();
	return res;
}
    
//
// Average Constraint
//
    
class ACTestState {
public:
    int64 x_min[3], x_max[3];
    int64 y_min, y_max;
	int64 w_min[3], w_max[3];
};

class TestAssign : public Decision {
public:
	TestAssign(IntVar* const v, int64 val,
						bool leq, bool geq, bool fail_on_btk);
	virtual ~TestAssign() {}
	virtual void Apply(Solver* const s);
	virtual void Refute(Solver* const s);
	virtual string DebugString() const;
	virtual void Accept(DecisionVisitor* const visitor) const {
		// visitor->VisitSetVariableValue(var_, value_);
	}

private:
	IntVar* const var_;
	int64 value_;
	bool leq_;
	bool geq_;
	bool fail_on_btk_;
};

TestAssign::TestAssign(IntVar* const v, int64 val,
									bool leq, bool geq, bool fail_on_btk)
    : var_(v), value_(val), leq_(leq), geq_(geq), fail_on_btk_(fail_on_btk){
	assert(leq || geq);
}

string TestAssign::DebugString() const {
	if (leq_ && geq_)
		return StringPrintf("[%s == %ld]", var_->DebugString().c_str(), value_);
	else if (leq_)
		return StringPrintf("[%s <= %ld]", var_->DebugString().c_str(), value_);
	else
		return StringPrintf("[%s >= %ld]", var_->DebugString().c_str(), value_);
}

void TestAssign::Apply(Solver* const s) {
	cout << "posting " << var_;
	if (leq_ && geq_) {
		cout << " = " << value_ << endl;
		var_->SetValue(value_);
	}
	else if (leq_) {
		cout << " <= " << value_ << endl;
		var_->SetMax(value_);
	}
	else {
		cout << " >= " << value_ << endl;
		var_->SetMin(value_);
	}
}

void TestAssign::Refute(Solver* const s) {
	if (fail_on_btk_) {
		cout << "fail!" << endl;
		var_->SetMax(var_->Min()-1);
	}
	else cout << "no-op" << endl;
}

Decision* MakeTestAssign(Solver* s, IntVar* const v, int64 val,
								bool leq, bool geq, bool fail_on_btk) {
  return s->RevAlloc(new TestAssign(v, val, leq, geq, fail_on_btk));
}

//
// Search by performing a single assignemt. This is however preceded by a
// single, fake dive which always fails, so as to allow testing of the
// incremental filtering algorithm.
//
class SingleAssignmentSearch : public DecisionBuilder  {
public:
	SingleAssignmentSearch(Solver* solver,
									const std::vector<IntVar*>& vars,
									std::vector<int64>& values)
	    : vars_(vars), values_(values), solver_(solver),
	 		first_attempt(true), branched(false) {}
	virtual ~SingleAssignmentSearch() {}

	Decision* Next(Solver* const s) {
		if (!branched)	 {		
			for (int i = 0; i < vars_.size(); ++i)
				if (!vars_[i]->Bound()) {
					int64 val = vars_[i]->Min();
					if (val == values_[i]) val++;
					branched = true;
					return MakeTestAssign(s, vars_[i], val, true, true, false);
				}
		}
		else {
			if (first_attempt) {
				for (int i = 0; i < vars_.size(); ++i)
					if (!vars_[i]->Bound()) {
						return MakeTestAssign(s, vars_[i], vars_[i]->Min(),
													true, true, true);
					}
			}
			else { 
				for (int i = 0; i < vars_.size(); ++i) {
					if (vars_[i]->Min() < values_[i]) {
						return MakeTestAssign(s, vars_[i], values_[i],
													false, true, true);
					}
					else if (vars_[i]->Max() > values_[i]) {
						return MakeTestAssign(s, vars_[i], values_[i],
													true, false, true);
					}
				}
			}
		}
				
		if (first_attempt) {
			cout << "END OF FIRST ATTEMPT" << endl;
			first_attempt = false;
			return s->MakeFailDecision();
		}
		return NULL;
	}
private:
	const std::vector<IntVar*>& vars_;
	std::vector<int64> const values_;
	Solver* solver_;
	bool first_attempt;
	bool branched;
};
	
//
// Print the value of a array of variables
//
class PrintDecisionBuilder : public DecisionBuilder  {
public:
	PrintDecisionBuilder(Solver* solver, const std::vector<IntVar*>& vars)
	    : vars_(vars) {}
	virtual ~PrintDecisionBuilder() {}

	Decision* Next(Solver* const s) {
		for (int i = 0; i < vars_.size(); ++i) {
			if (i > 0) cout << " ";
			cout << vars_[i];
		}
		cout << endl;
		return NULL;
	}
private:
	const std::vector<IntVar*>& vars_;
};
	
//
// Print the value of a array of variables
//
class PrintDecisionBuilder2 : public DecisionBuilder  {
public:
	PrintDecisionBuilder2(Solver* solver, IntVar* var)
	    : var_(var) {}
	virtual ~PrintDecisionBuilder2() {}

	Decision* Next(Solver* const s) {
		cout << var_ << endl;
		return NULL;
	}
private:
	const IntVar* var_;
};
	
//
// Print the value of a array of variables
//
class NOOPDecisionBuilder : public DecisionBuilder  {
public:
	NOOPDecisionBuilder(Solver* solver) {}
	virtual ~NOOPDecisionBuilder() {}

	Decision* Next(Solver* const s) {
		return NULL;
	}
};
	
    
ACTestState test_average(int x_val[], int64 k_val[], int y_val)
{
	/* build the solver */
	Solver solver("Test average constraint");

	/* build variables  */
	vector<IntVar*> x;
	x.push_back(solver.MakeIntVar(0, 10, string("x0")));
	x.push_back(solver.MakeIntVar(0, 10, string("x1")));
	x.push_back(solver.MakeIntVar(0, 10, string("x2")));        
	IntVar* y = solver.MakeIntVar(0, 10, string("y"));

	/* prepare weights vector */
	vector<int64> k;
	k.push_back(k_val[0]);
	k.push_back(k_val[1]);
	k.push_back(k_val[2]);
 
	/* set variables */
	vector<IntVar*> search_var;
	vector<int64> search_val;
	for (int i = 0; i < 3; ++i)
	if (x_val[i] != FREE_VAL) {
		// Constraint* cst_x = solver.MakeEquality(x[i], x_val[i]);
		// solver.AddConstraint(cst_x);
		// cst_x->PostAndPropagate();

		search_var.push_back(x[i]);
		search_val.push_back(x_val[i]);
	}
	if (y_val != FREE_VAL) {
		// Constraint* cst_y = solver.MakeEquality(y, y_val);
		// solver.AddConstraint(cst_y);
		// cst_y->PostAndPropagate();

		search_var.push_back(y);
		search_val.push_back(y_val);
	}

	/* build average constraint */
	Constraint* cst = MakeAverageCst(&solver, x, y, k);
	solver.AddConstraint(cst);
	cout << "Initial constraint status: " << cst << endl;

	/* propagate the neuron constraint */
	// cst->PostAndPropagate();
	// cout << "Constraint after propagation: " << cst << endl;	

	/* search */
	SingleAssignmentSearch search_strategy(&solver, search_var, search_val);

	solver.NewSearch(&search_strategy);
	bool solved = solver.NextSolution();

	ACTestState res;
	for(int i = 0; i < 3; ++i) {
		res.x_min[i] = x[i]->Min();
		res.x_max[i] = x[i]->Max();
	}
	res.y_min = y->Min();
	res.y_max = y->Max();

	solver.EndSearch();

	return res;        
}
	
ACTestState test_averageVTV(int64 x_min[], int64 x_max[],
								int64 w_min[], int64 w_max[],
								int64 y_min, int64 y_max)
{
	/* build the solver */
	Solver solver("Test average constraint with variabl terma values");

	/* build variables  */
	vector<IntVar*> x;
	x.push_back(solver.MakeIntVar(x_min[0], x_max[0], string("x0")));
	x.push_back(solver.MakeIntVar(x_min[1], x_max[1], string("x1")));
	x.push_back(solver.MakeIntVar(x_min[2], x_max[2], string("x2")));        

	IntVar* y = solver.MakeIntVar(y_min, y_max, string("y"));

	vector<IntVar*> w;
	w.push_back(solver.MakeIntVar(w_min[0], w_max[0], string("w0")));
	w.push_back(solver.MakeIntVar(w_min[1], w_max[1], string("w1")));
	w.push_back(solver.MakeIntVar(w_min[2], w_max[2], string("w2"))); 

	/* build average constraint */
	Constraint* cst = MakeAverageCst(&solver, x, y, w);
	solver.AddConstraint(cst);
	cout << "Initial constraint status: " << cst << endl;

	/* propagate the neuron constraint */
	// cst->PostAndPropagate();
	// cout << "Constraint after propagation: " << cst << endl;	

	/* search */
	// PrintDecisionBuilder db1(&solver, x);
	// PrintDecisionBuilder db2(&solver, w);
	// PrintDecisionBuilder2 db3(&solver, y);
	// DecisionBuilder* db = solver.Compose(&db1, &db2, &db3);
	NOOPDecisionBuilder db(&solver);

	solver.NewSearch(&db);
	bool solved = solver.NextSolution();

	ACTestState res;
	for(int i = 0; i < 3; ++i) {
		res.x_min[i] = x[i]->Min();
		res.x_max[i] = x[i]->Max();
		res.w_min[i] = w[i]->Min();
		res.w_max[i] = w[i]->Max();
	}
	res.y_min = y->Min();
	res.y_max = y->Max();

	solver.EndSearch();

	return res;        
}

// ================================================
// = Test the 2-Layer, Feed Forward NN constraint =
// ================================================

void test_Tansig2LFFCst()
{
    // build the solver
    Solver s("Tester for Tansig2LFFCst");
	
	// define the weight vectors
	vector<double> wgt0 = {0.4, 0.2, 0.4,
						   0.4, 0.4, 0.2};
	vector<double> wgt1 = {0.4, 0.1, 0.5};
	
	/* define the finite precision */
	int64 prec = 1000;
	    
    // build variables
	int n = 2;
	vector<IntVar*> x;
    s.MakeIntVarArray(n, -1, 1, "x", &x);
    IntVar* out = s.MakeIntVar(-2*prec, 2*prec, string("out"));
	
	// define the input expressions
	vector<IntExpr*> netin;
	for (int i = 0; i < n; ++i)
		netin.push_back(s.MakeProd(x[i], prec));
        
    // build the network constraint
    Constraint* netcst = MakeTansig2LFFCst(&s, netin, out, wgt0, wgt1, prec, 3);
    s.AddConstraint(netcst);
	
	// define an objective (to have frequente updates of the out var)
	vector<SearchMonitor*> monitors;
	monitors.push_back(s.MakeMinimize(out, prec / 10));
    
	// Define the search strategy
    DecisionBuilder* const db = s.MakePhase(x, Solver::CHOOSE_FIRST_UNBOUND,
                                        Solver::ASSIGN_MAX_VALUE);
	
	// start search
    s.NewSearch(db, monitors);
	int cnt = 0;
	while (s.NextSolution()) {
		// check the solution
		cout << "SOLUTION  " << cnt << " *****************************" << endl;
		cout << "out: " << out->Value() << endl;
		cout << "x:";
		for (int i = 0; i < n; ++i) cout << " " << x[i]->Value();
		cout << endl;
		
		// check the correctness of all solutions
		switch (cnt) {
			case 0:
				CHECK(x[0]->Value() == 1 && x[1]->Value() == 1);
				CHECK(out->Value() == 695);
				break;
			case 1:
				CHECK(x[0]->Value() == 0 && x[1]->Value() == 0);
				CHECK(out->Value() == 557);
				break;
			case 2:
				CHECK(x[0]->Value() == -1 && x[1]->Value() == 0);
				CHECK(out->Value() == 397);
				break;
			case 3:
				CHECK(x[0]->Value() == -1 && x[1]->Value() == -1);
				CHECK(out->Value() == 274);
				break;
		}
		
		// update the solution counter
		++cnt;
	}
	s.EndSearch();
	CHECK(cnt == 4);
}

void test_Tansig2LFFCst2()
{
    // build the solver
    Solver s("Tester for Tansig2LFFCst (#2)");
	
	// define the weight vectors
	vector<double> wgt0 = {0.0, 20.0, -20.0,
						   0.4, 0.4, 0.2};
	vector<double> wgt1 = {0.4, 0.5, -0.1};
	
	/* define the finite precision */
	int64 prec = 1000;
	    
    // build variables
	int n = 2;
	vector<IntVar*> x;
    s.MakeIntVarArray(n, -1, 1, "x", &x);
    IntVar* out = s.MakeIntVar(-2*prec, 2*prec, string("out"));
	
	// define the input expressions
	vector<IntExpr*> netin;
	for (int i = 0; i < n; ++i)
		netin.push_back(s.MakeProd(x[i], prec));
        
    // build the network constraint
    Constraint* netcst = MakeTansig2LFFCst(&s, netin, out, wgt0, wgt1, prec, 3);
    s.AddConstraint(netcst);
	
	// define an objective (to have frequent updates of the out var)
	vector<SearchMonitor*> monitors;
	monitors.push_back(s.MakeMaximize(out, prec / 10));
    
	// Define the search strategy
    DecisionBuilder* const db = s.MakePhase(x, Solver::CHOOSE_FIRST_UNBOUND,
                                        Solver::ASSIGN_MIN_VALUE);
	
	// start search
    s.NewSearch(db, monitors);
	int cnt = 0;
	while (s.NextSolution()) {
		// check the solution
		cout << "SOLUTION  " << cnt << " *****************************" << endl;
		cout << "out: " << out->Value() << endl;
		cout << "x:";
		for (int i = 0; i < n; ++i) cout << " " << x[i]->Value();
		cout << endl;
		
		// check the correctness of all solutions
		switch (cnt) {
			case 0:
				CHECK(x[0]->Value() == -1 && x[1]->Value() == -1);
				CHECK(out->Value() == 397);
				break;
			case 1:
				CHECK(x[0]->Value() == 0 && x[1]->Value() == -1);
				CHECK(out->Value() == 707);
				break;
		}
		
		// update the solution counter
		++cnt;
	}
	s.EndSearch();
	CHECK(cnt == 2);
	
	// start search (second iteration)
    DecisionBuilder* const db2 = s.MakePhase(x, Solver::CHOOSE_FIRST_UNBOUND,
                                        Solver::ASSIGN_MAX_VALUE);
	
    s.NewSearch(db2, monitors);
	cnt = 0;
	cout << "************ SECOND ATTEMPT *************" << endl;
	while (s.NextSolution()) {
		// check the solution
		cout << "SOLUTION  " << cnt << " *****************************" << endl;
		cout << "out: " << out->Value() << endl;
		cout << "x:";
		for (int i = 0; i < n; ++i) cout << " " << x[i]->Value();
		cout << endl;
		
		// check the correctness of all solutions
		switch (cnt) {
			case 0:
				CHECK(x[0]->Value() == 1 && x[1]->Value() == 1);
				CHECK(out->Value() == 313);
				break;
			case 1:
				CHECK(x[0]->Value() == 1 && x[1]->Value() == 0);
				CHECK(out->Value() == 682);
				break;
		}
		
		// update the solution counter
		++cnt;
	}
	s.EndSearch();
	CHECK(cnt == 2);
}



// #define TEST_HARDLIM_
// #define TEST_TANSIG_
// #define TEST_ROUNDEDPRODUCT_
// #define TEST_AVERAGE_
// #define TEST_AVERAGE_VTV_
// #define TEST_ROUNDEDDIVISION_
// #define TEST_2LFF_
#define TEST_2LFFCST_

int main(int argc, char **argv)
{
	NCTestState res;
	
	//--------------------------------------------------------------------------
	// Stage 1: basic tests (on the Hardlim neuron); precision is fixed to 1
	//--------------------------------------------------------------------------
	
#ifdef TEST_HARDLIM_
	/* test 0 */
	reset();
	cout << "TEST 0" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == 1);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 1);
	CHECK(res.out_lb == 0 && res.out_ub == 1);
	
	/* test 1: fix an input and test in-to-out propagation */
	reset();
	in_defaults[0] = 0;
	cout << "TEST 1" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == 0);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 1);
	CHECK(res.out_lb == 0 && res.out_ub == 0);
	
	/* test 2: fix an input, change the bias and test in-to-out propagation */
	reset();
	in_defaults[0] = 1;
	bias = -0.4;
	cout << "TEST 2" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 1 && res.in_ub[0] == 1);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 1);
	CHECK(res.out_lb == 1 && res.out_ub == 1);
	
	/* test 3: fix the output and test out-to-in propagation */
	reset();
	out_default = 1;
	cout << "TEST 3" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 1 && res.in_ub[0] == 1);
	CHECK(res.in_lb[1] == 1 && res.in_ub[1] == 1);
	CHECK(res.out_lb == 1 && res.out_ub == 1);
	
	/* test 4: fix the output and one input and test out-to-in propagation */
	reset();
	in_defaults[0] = 1;
	out_default = 0;
	bias = -0.6;
	cout << "TEST 4" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 1 && res.in_ub[0] == 1);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 0);
	CHECK(res.out_lb == 0 && res.out_ub == 0);
	
	/* test 5: fix the output and one input and test out-to-in propagation */
	reset();
	in_defaults[0] = 1;
	out_default = 1;
	bias = -0.6;
	cout << "TEST 5" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 1 && res.in_ub[0] == 1);
	CHECK(res.in_lb[1] == 1 && res.in_ub[1] == 1);
	CHECK(res.out_lb == 1 && res.out_ub == 1);
	
	//--------------------------------------------------------------------------
	// Stage 2: play with different precision value
	//--------------------------------------------------------------------------
	
	/* test 6: like test 5, with higher precision */
	reset();
	prec = 100;
	in_defaults[0] = prec;
	out_default = prec;
	bias = -0.6;
	cout << "TEST 6" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == prec && res.in_ub[0] == prec);
	CHECK(res.in_lb[1] == 0.1 * prec / weights[1] && res.in_ub[1] == prec);
	CHECK(res.out_lb == prec && res.out_ub == prec);
	
	/* test 7: like test 4, with higher precision */
	reset();
	prec = 100;
	in_defaults[0] = prec;
	out_default = 0;
	bias = -0.6;
	cout << "TEST 7" << endl;
	res = test_hardlim(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == prec && res.in_ub[0] == prec);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 0.1 * prec / weights[1] - 1);
	CHECK(res.out_lb == 0 && res.out_ub == 0);
#endif
	
	//--------------------------------------------------------------------------
	// Stage 3: Test tansig constraint
	//--------------------------------------------------------------------------
	
#ifdef TEST_TANSIG_
	/* test 8, basic propagation of the tansign constraint (all variables are
	       left free) */
	reset();
	cout << "TEST 8" << endl;
	res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == prec);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == prec);
	CHECK(res.out_lb == -prec && res.out_ub == 0);
	    
	/* test 9, basic propagation of the tansign constraint (all variables are
	     left free) */
	reset();
	    bias = 0;
	cout << "TEST 9" << endl;
	res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == prec);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == prec);
	CHECK(res.out_lb == 0 && res.out_ub == prec);
	
	/* test 10, basic propagation of the tansign constraint (all variables are
	     left free) */
	reset();
	    out_default = -1;
	    weights[0] = 20;
	    weights[1] = 20;
	cout << "TEST 10" << endl;
	res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == 0);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 0);
	CHECK(res.out_lb == -1 && res.out_ub == -1);
	
	/* test 11, basic propagation of the tansig constraint (all variables are
	     left free) */
	reset();
	    out_default = 0; // this assignment is impossible with the given weights
	    weights[0] = 20; // and precision = 1
	    weights[1] = 20;
	    prec = 1000;
	cout << "TEST 11" << endl;
	res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == 30);
	CHECK(res.in_lb[1] == 0 && res.in_ub[1] == 30);
	CHECK(res.out_lb == 0 && res.out_ub == 0);
	
	/* test 11b, basic propagation of the tansign constraint (all variables are
	     left free) */
	reset();
	    out_default = 0; // this assignment is impossible with the given weights
	    weights[0] = 20; // and precision = 1
	    weights[1] = -20;
	    in_defaults[0] = 0;
	    bias = 0.6;
	    prec = 1000;
	cout << "TEST 11b" << endl;
	res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	CHECK(res.in_lb[0] == 0 && res.in_ub[0] == 0);
	CHECK(res.in_lb[1] == 30 && res.in_ub[1] == 30);
	CHECK(res.out_lb == 0 && res.out_ub == 0);
#endif
	    
	//--------------------------------------------------------------------------
	// Stage 4: Test rounded product constraint
	//--------------------------------------------------------------------------
	   
#ifdef TEST_ROUNDEDPRODUCT_ 
	RPTestState res2;

	// test 12: free constraint, y = x
	cout << "TEST 12: Rounded product constraint" << endl;
	res2 = test_rounded_prod(FREE_VAL, FREE_VAL, 0, 1);
	CHECK(res2.x_min == -100 && res2.x_max == 100);
	CHECK(res2.y_min == -100 && res2.y_max == 100);
	    
	// test 13: free constraints, but non unary coefficient
	cout << "TEST 13: Rounded product constraint" << endl;
	res2 = test_rounded_prod(FREE_VAL, FREE_VAL, 0, 2);
	CHECK(res2.x_min == -50 && res2.x_max == 50);
	CHECK(res2.y_min == -100 && res2.y_max == 100); 
	
   // test 14: free constraints, but fractional coefficient
   cout << "TEST 13: Rounded product constraint" << endl;
   res2 = test_rounded_prod(FREE_VAL, FREE_VAL, 0, 0.335);
	CHECK(res2.x_min == -100 && res2.x_max == 100);
	CHECK(res2.y_min == -34 && res2.y_max == 34);
	    
   // test 14: free constraints, but negative coefficient
   cout << "TEST 14: Rounded product constraint" << endl;
   res2 = test_rounded_prod(FREE_VAL, FREE_VAL, 0, -0.335);
	CHECK(res2.x_min == -100 && res2.x_max == 100);
   CHECK(res2.y_min == -34 && res2.y_max == 34);
	    
   // test 15: free constraints, but negative coefficient
   cout << "TEST 15: Rounded product constraint" << endl;
   res2 = test_rounded_prod(FREE_VAL, FREE_VAL, 3, 0.335);
	CHECK(res2.x_min == -100 && res2.x_max == 100);
   CHECK(res2.y_min == -32 && res2.y_max == 35); 
#endif
	    
	//--------------------------------------------------------------------------
	// Stage 3 bis: tansig again (negative weights)
	//--------------------------------------------------------------------------
	
#ifdef TEST_TANSIG_
	/* test 16, tansig, negative weights */
	// reset();
	//    prec = 1000;
	//    out_default = 0; // this assignment is impossible with the given weights
	//    weights[0] = 20; // and precision = 1
	//    weights[1] = -20;
	//    in_defaults[0] = prec;
	// cout << "TEST 16" << endl;
	// res = test_tansig(prec, ninputs, weights, bias, in_defaults, out_default);
	// CHECK(res.in_lb[0] == prec && res.in_ub[0] == prec);
	// CHECK(res.in_lb[1] == 970 && res.in_ub[1] == 970);
	// CHECK(res.out_lb == 0 && res.out_ub == 0);
#endif

	//--------------------------------------------------------------------------
	// Stage 5: Test average constraint
	//--------------------------------------------------------------------------

#ifdef TEST_AVERAGE_
	ACTestState res3;
	int x_val[] = {FREE_VAL, FREE_VAL, FREE_VAL};
	int64 k_val[] = {1, 1, 1};
    
	/* test 17 */
	cout << "TEST 17: Average constraint (free)" << endl;
	res3 = test_average(x_val, k_val, FREE_VAL);
	CHECK(res3.x_min[0] == 0 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 0 && res3.y_max == 1);
	    
	/* test 18 */
	cout << "TEST 18: Average constraint (free, non uniform weights)" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	res3 = test_average(x_val, k_val, FREE_VAL);
	CHECK(res3.x_min[0] == 0 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 0 && res3.y_max == 4);
	
	/* test 19 */
	cout << "TEST 19: Average constraint (high weight, min propagation)" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	res3 = test_average(x_val, k_val, 3);
	CHECK(res3.x_min[0] == 1 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 3 && res3.y_max == 3);
	    
	/* test 20 */
	cout << "TEST 20: Average constraint (low weight, max propagation)" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	x_val[0] = 9, x_val[1] = FREE_VAL, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, 3);
	CHECK(res3.x_min[0] == 9 && res3.x_max[0] == 9);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 9);
	CHECK(res3.y_min == 3 && res3.y_max == 3);
	    
	/* test 21 */
	cout << "TEST 21: Average constraint (high weight, max propagation" << endl;
	k_val[0] = 9, k_val[1] = 2, k_val[2] = 1;
	x_val[0] = FREE_VAL, x_val[1] = FREE_VAL, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, 3);
	CHECK(res3.x_min[0] == 1 && res3.x_max[0] == 7);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 3 && res3.y_max == 3);
	    
	/* test 22 */
	cout << "TEST 22: Average constraint (low weight, min propagation)" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	x_val[0] = FREE_VAL, x_val[1] = 0, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, 3);
	CHECK(res3.x_min[0] == 1 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 0);
	CHECK(res3.x_min[2] == 1 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 3 && res3.y_max == 3);
	    
	/* test 23 */
	cout << "TEST 22: in to out" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	x_val[0] = FREE_VAL, x_val[1] = 1, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, FREE_VAL);
	CHECK(res3.x_min[0] == 0 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 1 && res3.x_max[1] == 1);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 1 && res3.y_max == 4);
	    
	/* test 24 */
	cout << "TEST 23: in to out /2" << endl;
	k_val[0] = 4, k_val[1] = 2, k_val[2] = 1;
	x_val[0] = 2, x_val[1] = FREE_VAL, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, FREE_VAL);
	CHECK(res3.x_min[0] == 2 && res3.x_max[0] == 2);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
	CHECK(res3.y_min == 2 && res3.y_max == 4);
	
	/* test 23ter */
	cout << "TEST 23-ter: stop condition" << endl;
	k_val[0] = 4, k_val[1] = 3, k_val[2] = 2;
	x_val[0] = FREE_VAL, x_val[1] = FREE_VAL, x_val[2] = FREE_VAL;
	res3 = test_average(x_val, k_val, 3);
	CHECK(res3.x_min[0] == 0 && res3.x_max[0] == 10);
	CHECK(res3.x_min[1] == 0 && res3.x_max[1] == 10);
	CHECK(res3.x_min[2] == 0 && res3.x_max[2] == 10);
#endif

	//--------------------------------------------------------------------------
	// Stage 6: Test rounded division constraint
	//--------------------------------------------------------------------------

#ifdef TEST_ROUNDEDDIVISION_
	RDTestState res4;
	
	// test 25: free constraint
	cout << "TEST 24: Rounded division constraint (free)" << endl;
	res4 = test_rounded_div(FREE_VAL, FREE_VAL, FREE_VAL);
	CHECK(res4.x_min == -100 && res4.x_max == 100);
	CHECK(res4.y_min == 0 && res4.y_max == 100);
	CHECK(res4.z_min == -100 && res4.z_max == 100);
	
	// test 26:
	cout << "TEST 25: Rounded division constraint" << endl;
	res4 = test_rounded_div(40, FREE_VAL, FREE_VAL);
	CHECK(res4.x_min == 40 && res4.x_max == 40);
	CHECK(res4.y_min == 1 && res4.y_max == 100);
	CHECK(res4.z_min == 0 && res4.z_max == 40);
	
	// test 27:
	cout << "TEST 26: Rounded division constraint" << endl;
	res4 = test_rounded_div(FREE_VAL, 2, FREE_VAL);
	CHECK(res4.x_min == -100 && res4.x_max == 100);
	CHECK(res4.y_min == 2 && res4.y_max == 2);
	CHECK(res4.z_min == -50 && res4.z_max == 50);
	
	// test 28:
	cout << "TEST 27: Rounded division constraint" << endl;
	res4 = test_rounded_div(FREE_VAL, FREE_VAL, 10);
	CHECK(res4.x_min == 10 && res4.x_max == 100);
	CHECK(res4.y_min == 1 && res4.y_max == 10);
	CHECK(res4.z_min == 10 && res4.z_max == 10);
	
	cout << "TEST 27b: Rounded division constraint" << endl;
	res4 = test_rounded_div(FREE_VAL, FREE_VAL, -10);
	CHECK(res4.x_min == -100 && res4.x_max == -10);
	CHECK(res4.y_min == 1 && res4.y_max == 10);
	CHECK(res4.z_min == -10 && res4.z_max == -10);
	
	cout << "TEST 27c: Rounded division constraint" << endl;
	res4 = test_rounded_div(FREE_VAL, FREE_VAL, 0);
	CHECK(res4.x_min == 0 && res4.x_max == 0);
	CHECK(res4.y_min == 0 && res4.y_max == 100);
	CHECK(res4.z_min == 0 && res4.z_max == 0);
	
	cout << "TEST 27d: Rounded division constraint" << endl;
	res4 = test_rounded_div(0, 0, FREE_VAL);
	CHECK(res4.x_min == 0 && res4.x_max == 0);
	CHECK(res4.y_min == 0 && res4.y_max == 0);
	CHECK(res4.z_min == 0 && res4.z_max == 0);
	
	cout << "TEST 27e: Rounded division constraint" << endl;
	res4 = test_rounded_div(0, FREE_VAL, 0);
	CHECK(res4.x_min == 0 && res4.x_max == 0);
	CHECK(res4.y_min == 0 && res4.y_max == 100);
	CHECK(res4.z_min == 0 && res4.z_max == 0);
	
	cout << "TEST 27f: Rounded division constraint" << endl;
	res4 = test_rounded_div(FREE_VAL, 0, 0);
	CHECK(res4.x_min == 0 && res4.x_max == 0);
	CHECK(res4.y_min == 0 && res4.y_max == 0);
	CHECK(res4.z_min == 0 && res4.z_max == 0);
	
	// // test 29:
	// cout << "TEST 28: Rounded division constraint" << endl;
	// res4 = test_rounded_div(FREE_VAL, 0, FREE_VAL);
	// // CHECK(res4.x_min == 10 && res4.x_max == 100);
	// // CHECK(res4.y_min == 1 && res4.y_max == 10);
	// // CHECK(res4.z_min == 10 && res4.z_max == 10);
#endif

	//-------------------------------------------------------------------------
	// Test Average Constraint with Variable Term Weights
	//-------------------------------------------------------------------------
	
#ifdef TEST_AVERAGE_VTV_
	ACTestState res5;
	int64 x_min[] = {0, 0, 0};
	int64 x_max[] = {10, 10, 10};
	int64 w_min[] = {-10, -10, -10};
	int64 w_max[] = {10, 10, 10};
	int64 y_min = -10, y_max = 10;
	
	cout << "basic in-to-out propagation #1" << endl;
	w_min[0] = 0, w_min[1] = 0, w_min[2] = 0;
	w_max[0] = 0, w_max[1] = 0, w_max[2] = 0;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.y_min == 0);	CHECK(res5.y_max == 0);
	CHECK(res5.x_min[0] == 0); CHECK(res5.x_max[0] == 10);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 10);
	CHECK(res5.x_min[2] == 0); CHECK(res5.x_max[2] == 10);
	
	cout << "basic in-to-out propagation #2" << endl;
	w_min[0] = -3, w_min[1] = 0, w_min[2] = 0;
	w_max[0] = 0, w_max[1] = 5, w_max[2] = 0;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.y_min == -3);	CHECK(res5.y_max == 5);
	CHECK(res5.x_min[0] == 0); CHECK(res5.x_max[0] == 10);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 10);
	CHECK(res5.x_min[2] == 0); CHECK(res5.x_max[2] == 10);
	
	cout << "basic in-to-out propagation #3" << endl;
	// w_min[0] = 1, w_min[1] = 3, w_min[2] = 5;
	// w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	w_min[0] = 1, w_min[1] = 3, w_min[2] = 5;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	x_min[0] = 1, x_min[1] = 1, x_min[2] = 1;
	x_max[0] = 10, x_max[1] = 10, x_max[2] = 2;
	y_min = -10, y_max = 10;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.y_min == 2);	CHECK(res5.y_max == 5);
	
	cout << "out-to-in propagation #1" << endl;
	w_min[0] = 1, w_min[1] = 3, w_min[2] = 5;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	x_min[0] = 0, x_min[1] = 0, x_min[2] = 0;
	x_max[0] = 1, x_max[1] = 1, x_max[2] = 10;
	y_min = -10, y_max = 4;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 0); CHECK(res5.x_max[0] == 1);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 1);
	CHECK(res5.x_min[2] == 0); CHECK(res5.x_max[2] == 9);
	CHECK(res5.w_min[0] == 1); CHECK(res5.w_max[0] == 2);
	CHECK(res5.w_min[1] == 3); CHECK(res5.w_max[1] == 4);
	CHECK(res5.w_min[2] == 5); CHECK(res5.w_max[2] == 6);
	CHECK(res5.y_min == 0); 	CHECK(res5.y_max == 4);
	
	cout << "out-to-in propagation #2" << endl;
	w_min[0] = 1, w_min[1] = 3, w_min[2] = 6;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 7;
	x_min[0] = 0, x_min[1] = 0, x_min[2] = 2;
	x_max[0] = 1, x_max[1] = 1, x_max[2] = 10;
	y_min = -10, y_max = 4;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 1); CHECK(res5.x_max[0] == 1);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 1);
	CHECK(res5.x_min[2] == 2); CHECK(res5.x_max[2] == 3);
	CHECK(res5.w_min[0] == 1); CHECK(res5.w_max[0] == 2);
	CHECK(res5.w_min[1] == 3); CHECK(res5.w_max[1] == 4);
	CHECK(res5.w_min[2] == 6); CHECK(res5.w_max[2] == 6);
	CHECK(res5.y_min == 4); 	CHECK(res5.y_max == 4);
	
	cout << "out-to-in propagation #3" << endl;
	w_min[0] = 1, w_min[1] = 3, w_min[2] = 8;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 9;
	x_min[0] = 0, x_min[1] = 0, x_min[2] = 1;
	x_max[0] = 1, x_max[1] = 1, x_max[2] = 10;
	y_min = -10, y_max = 4;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 1); CHECK(res5.x_max[0] == 1);
	CHECK(res5.x_min[1] == 1); CHECK(res5.x_max[1] == 1);
	CHECK(res5.x_min[2] == 1); CHECK(res5.x_max[2] == 1);
	CHECK(res5.w_min[0] == 1); CHECK(res5.w_max[0] == 2);
	CHECK(res5.w_min[1] == 3); CHECK(res5.w_max[1] == 4);
	CHECK(res5.w_min[2] == 8); CHECK(res5.w_max[2] == 9);
	CHECK(res5.y_min == 4); 	CHECK(res5.y_max == 4);
	
	cout << "out-to-in propagation #3.1" << endl;
	w_min[0] = -6, w_min[1] = 5, w_min[2] = 8;
	w_max[0] = -1, w_max[1] = 6, w_max[2] = 9;
	x_min[0] = 0, x_min[1] = 0, x_min[2] = 0;
	x_max[0] = 10, x_max[1] = 10, x_max[2] = 10;
	y_min = -10, y_max = -4;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 1); CHECK(res5.x_max[0] == 10);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 2);
	CHECK(res5.x_min[2] == 0); CHECK(res5.x_max[2] == 2);
	CHECK(res5.w_min[0] == -6); CHECK(res5.w_max[0] == -4);
	CHECK(res5.w_min[1] == 5); CHECK(res5.w_max[1] == 6);
	CHECK(res5.w_min[2] == 8); CHECK(res5.w_max[2] == 9);
	CHECK(res5.y_min == -6); 	CHECK(res5.y_max == -4);
	
	cout << "out-to-in propagation #4 (avg min)" << endl;
	w_min[0] = 1, w_min[1] = 3, w_min[2] = 3;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	x_min[0] = 0, x_min[1] = 0, x_min[2] = 0;
	x_max[0] = 10, x_max[1] = 10, x_max[2] = 10;
	y_min = 5, y_max = 10;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 0); CHECK(res5.x_max[0] == 6);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 10);
	CHECK(res5.x_min[2] == 1); CHECK(res5.x_max[2] == 10);
	CHECK(res5.w_min[0] == 1); CHECK(res5.w_max[0] == 2);
	CHECK(res5.w_min[1] == 3); CHECK(res5.w_max[1] == 4);
	CHECK(res5.w_min[2] == 5); CHECK(res5.w_max[2] == 6);
	CHECK(res5.y_min == 5); 	CHECK(res5.y_max == 6);
	
	cout << "out-to-in propagation #5 (avg min)" << endl;
	w_min[0] = -50, w_min[1] = 3, w_min[2] = 5;
	w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	x_min[0] = 1, x_min[1] = 0, x_min[2] = 0;
	x_max[0] = 10, x_max[1] = 10, x_max[2] = 10;
	y_min = 5, y_max = 10;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.x_min[0] == 1); CHECK(res5.x_max[0] == 6);
	CHECK(res5.x_min[1] == 0); CHECK(res5.x_max[1] == 10);
	CHECK(res5.x_min[2] == 2); CHECK(res5.x_max[2] == 10);
	CHECK(res5.w_min[0] == -10); CHECK(res5.w_max[0] == 2);
	CHECK(res5.w_min[1] == 3); CHECK(res5.w_max[1] == 4);
	CHECK(res5.w_min[2] == 5); CHECK(res5.w_max[2] == 6);
	CHECK(res5.y_min == 5); 	CHECK(res5.y_max == 6);
	
	cout << "advanced in-to-out propagation" << endl;
	// w_min[0] = 1, w_min[1] = 3, w_min[2] = 5;
	// w_max[0] = 2, w_max[1] = 4, w_max[2] = 6;
	w_min[0] = 3, w_min[1] = 1, w_min[2] = 7;
	w_max[0] = 3, w_max[1] = 8, w_max[2] = 7;
	x_min[0] = 1, x_min[1] = 1, x_min[2] = 1;
	x_max[0] = 10, x_max[1] = 18, x_max[2] = 10;
	y_min = -10, y_max = 10;
	res5 = test_averageVTV(x_min, x_max, w_min, w_max, y_min, y_max);
	CHECK(res5.y_min == 1);	CHECK(res5.y_max == 8);
#endif
	
// #ifdef TEST_2LFF_
// 	// prepare return data structure
// 	NNTestState nnres;
// 	// prepare input data
// 	int prec = 100; // precision
// 	int i_min[] = {-prec, -prec}; // input min
// 	int i_max[] = {+prec, +prec/2}; // input max
// 	int o_min[] = {-100, -100}; // output min
// 	int o_max[] = {+100, +100}; // output max
// 	// define hidden neuron weights
// 	int isize = 2, hsize = 2, osize = 2;
// 	double wgt0[] = {0, -1, -1,	0, 1, -1};
// 	double wgt1[] = {0, 1, 1, 0, 1, 1};
// 	nnres = test_2lff_network(isize, hsize, osize,
// 							  i_min, i_max, o_min, o_max,
// 							  wgt0, wgt1, prec);
// #endif
							  
#ifdef TEST_2LFFCST_
	// test_Tansig2LFFCst();
	test_Tansig2LFFCst2();
#endif
	    
	return 0;
}
