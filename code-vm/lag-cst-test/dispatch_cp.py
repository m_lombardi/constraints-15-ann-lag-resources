#!/usr/bin/env python
# encoding: utf-8

import pywrapcp_eml as pycp
import numpy as np
import sys
import getopt
from numpy import ceil
import random
#from numpy import floor

# =====================
# = Global parameters =
# =====================

ptf_h = 8 # platform height
ptf_w = 6 # platform width

cpi_vmin = 0 # minimum CPI value (for normalization)
cpi_vmax = 35 # maximum CPI value (for normalization)

# for predicting MEAN EFFICIENCY
eff_vmin = 0.0 # minimum efficiency value (for normalization)
eff_vmax = 1.0 # maximum efficiency value (for normalization)

# normalization factors
# NOTE: those must match the values used during network training
eff_n_sub = 0.5 * (eff_vmax+0.2 - eff_vmin)
eff_n_div = 0.5 * (eff_vmax+0.2 - eff_vmin)
eff_range = (eff_vmax+0.2 - eff_vmin)

cpi_n_sub = 0.5*(cpi_vmax-cpi_vmin)
cpi_n_div = 0.5*(cpi_vmax-cpi_vmin)

# ============================================
# = Normalization/de-normalization functions =
# ============================================

def normalize_cpi(V):
    return [(v - cpi_n_sub)/cpi_n_div for v in V]
    
def de_normalize_cpi(V):
    return [(v * cpi_n_div + cpi_n_sub) for v in V]
    
def normalize_eff(V):
    return [(v - eff_n_sub)/eff_n_div for v in V]
    
def de_normalize_eff(V):
    return [(v * eff_n_div + eff_n_sub) for v in V]

# =================================
# = Neural network modeling stuff =
# =================================

def tansig(x):
    return 2 / (1 + np.e**(-2*x)) - 1

class ANN2L:
    def __init__(self, b, w, bcap, wcap):
        self.b = b
        self.w = w
        self.bcap = bcap
        self.wcap = wcap
        self.n = len(w[0])
        self.m = len(b)
        
    def eval(self, x, prec = None):
        assert len(x) == self.n
        # ATTN the type of neurons must match the real network
        if prec is None:  
            y = [self.b[j] + sum(self.w[j][i] * x[i] for i in xrange(self.n))
                                                     for j in xrange(self.m)]
            y = [tansig(v) for v in y]
            z = self.bcap + sum(self.wcap[j] * y[j] for j in xrange(self.m))
            z = tansig(z)
        else:
            ndigits = int(np.log10(prec))
            xr = [round(v, ndigits) for v in x]
            # print 'x', xr
            y = [self.b[j] + sum(self.w[j][i] * xr[i] for i in xrange(self.n))
                                                      for j in xrange(self.m)]
            y = [round(tansig(v), ndigits) for v in y]
            # print 'y', y
            z = self.bcap + sum(self.wcap[j] * y[j] for j in xrange(self.m))
            z = round(tansig(z), ndigits)
            # print 'z', z
        return z
        
    def __repr__(self):
        s = '%d,%d,%d' % (self.n, self.m, 1)
        for j in xrange(self.m):
            s += ',' + str(','.join(str(v) for v in self.w[j]))
            s += ',' + str(self.b[j])
        s += ',' + str(','.join(str(v) for v in self.wcap))
        s += ',' + str(self.bcap)
        return s

# ==============================================================
# = Functions to read workload/mapping and network information =
# ==============================================================

def read_networks(fname):
    # prepare the list of networks
    nets = []
    # parse the input file
    with open(fname) as f:
        for l in f:
            if len(l.strip()) > 0 and not l.startswith('#'):
                flds = l[l.find(':')+1:].split(',')
                
                # read number of inputs and outputs
                n = int(flds[0]) # number of inputs
                m = int(flds[1]) # number of hidden layers
                # there is always a single output, so nothing to read here
            
                # this is the data for layer 1
                flds_w = flds[3:3+m*(n+1)] # bias/weights for the first layer
                # this is the data for layer 2
                flds_wcap = flds[3+m*(n+1):] # bias/weights for the output layer
            
                # separate weights and bias data (bias info comes last)
                w = [[float(v) for v in flds_w[j*(n+1):j*(n+1)+n]]
                     for j in xrange(m)]
                b = [float(flds_w[n+j*(n+1)]) for j in xrange(m)]
            
                # separate weights and bias data (bias info comes last)
                bcap = float(flds_wcap[m])
                wcap = [float(v) for v in flds_wcap[:m]]

                # build and return a network
                nets.append(ANN2L(b, w, bcap, wcap))
    # return all the networks
    assert len(nets) == ptf_w * ptf_h
    return nets
                
def read_workloads(fname):
    wlds = []
    with open(fname) as f:
        for l in f:
            if len(l.strip()) > 0 and not l.startswith('#'):
                wlds.append([float(v) for v in l.split(',')])
    return wlds
    
def read_reservations(fname):
    wlds = []
    with open(fname) as f:
        for l in f:
            if len(l.strip()) > 0 and not l.startswith('#'):
                wlds.append([int(v) for v in l.split(',')])
    return wlds
    
# ========================================
# = Functions to build and the ANN model =
# ========================================
        
def h1_norm(c0, c1):
    '''H1 norm for pairs (i.e. x,y positions)'''
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return abs(c0_x - c1_x) + abs(c0_y - c1_y)
    
def hinf_norm(c0, c1):
    '''H infinite norm for pairs (i.e. x,y positions)'''
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return max(abs(c0_x - c1_x), abs(c0_y - c1_y))
    
def is_ur(c0, c1):
    """Check if c0 is on the upper right of c1"""
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return c0_x > c1_x and c0_y > c1_y

def is_br(c0, c1):
    """Check if c0 is on the bottom right of c1"""
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return c0_x > c1_x and c0_y < c1_y
    
def is_bl(c0, c1):
    """Check if c0 is on the bottom left of c1"""
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return c0_x < c1_x and c0_y < c1_y
    
def is_ul(c0, c1):
    """Check if c0 is on the upper left of c1"""
    c0_x, c0_y = c0 / ptf_w, c0 % ptf_w
    c1_x, c1_y = c1 / ptf_w, c1 % ptf_w
    return c0_x < c1_x and c0_y > c1_y

#------------------------------------------

def build_network(s, target, ann, AVG, MIN, MIN2, OUTv, params, ann_type):
    """
    s - the solver
    ann - the neural network
    AVG - the average cpi variables (for all cores)
    MAX - the maximum cpi variables (for all cores)
    MIN - the minimum cpi variables (for all cores)
    MIN2 - the "second minimum" cpi variables (for all cores)
    OUTv - the output variable for the target core
    params - the option pack for the dispacthing subproblem
    """
    # define some ranges and auxiliary variables (for convenience)
    cores = range(ptf_w * ptf_h)
    
    # prepare the network input
    inputs = []
    # - the first input is the avg cpi of the target core: nothing to do here
    inputs.append(AVG[target])

    # - the second input is the min cpi of the target core: nothing to do here  
    if (ann_type == 1):
        inputs.append(MIN[target])
    
    # - the third input is the second min cpi of the target core
    # inputs.append(MIN2[target])

    # - the fourth input is the avg of the avg cpi of the neigborhing cores
    neighbors = [k for k in cores if hinf_norm(k,target) == 1]
    N_MEAN = s.IntVar(-sys.maxint-1, sys.maxint, "N_MEAN[%d]" % target)
    s.Add(pycp.RoundedProductConstraint(s,
                        s.Sum([AVG[k] for k in neighbors]), # input
                        N_MEAN, # output
                        1.0 / len(neighbors))) # coefficient
    inputs.append(N_MEAN)
                        
    
    # - the fifth input is the avg of the avg cpi of all the other cores
    others = [k for k in cores if hinf_norm(k,target) > 1]
    O_MEAN = s.IntVar(-sys.maxint-1, sys.maxint, "O_MEAN[%d]" % target)
    s.Add(pycp.RoundedProductConstraint(s,
                        s.Sum([AVG[k] for k in others]), # input
                        O_MEAN, # output
                        1.0 / len(others))) # coefficient
    inputs.append(O_MEAN)
    
    
    # # TODO for the bound evaluation only
    # if ann_type == 1:
    #     inputs = [s.IntVar(-params.prec, params.prec) for k in xrange(4)]
    # else:
    #     inputs = [s.IntVar(-params.prec, params.prec) for k in xrange(3)]
    
    # BUILD THE CONSTRAINTS TO ENCODE THE NETWORK
    net = [] # all the neuron constraints for this network (legacy stuff)
    # flatten L0 weights
    wgt0 = []
    for b, w in zip(ann.b, ann.w):
        wgt0 += [b] + w
    # flatten L1 weights
    wgt1 = [ann.bcap] + ann.wcap
    # build the ANN constraint
    cst = pycp.Tansig2LFFCst(s, # the solver
                             inputs, # network input
                             OUTv, # network output
                             wgt0, # flattened L0 weights
                             wgt1, # flattened L1 weights
                             params.prec, # precision
                             # 'NET%d' % target, # constraint name
                             params.net_prop_level)
    # add the constraint to the model
    s.Add(cst)
    net.append(cst)
    
    
    # DE-COMMENT THIS JUST FOR DEBUG PURPOSE
    # else:
    #     # define the output variables for the hidden layer
    #     H_OUT = [s.IntVar(-sys.maxint-1, sys.maxint, 'H_OUT[%d]' % j)
    #                                                         for j in xrange(ann.m)]
    #     # build the hidden layer
    #     for j in xrange(ann.m):
    #         # build the neuron constraints
    #         cst = pycp.TansigNeuron(s, # the solver
    #                                 inputs, # neuron inputs
    #                                 H_OUT[j], # output variable
    #                                 ann.b[j], # bias
    #                                 ann.w[j], # weights
    #                                 params.prec, # precision
    #                                 'HN[%d][%d]' % (target,j)) # name
    #         # add the new constraint
    #         s.Add(cst)
    #         net.append(cst)
    #
    #     # build the output layer (this is always a single neuron)
    #     # ATTN: check that the neuron types match those used during learning
    #     cst = pycp.TansigNeuron(s, # the solver
    #                             H_OUT, # the output of the hidden neurons
    #                             OUTv, # the output variable
    #                             ann.bcap, # the bias
    #                             ann.wcap, # the weights
    #                             params.prec, # the precision factor
    #                             'ON[%d]' % target) # name
    #     # add the new constraint
    #     s.Add(cst)
    #     net.append(cst)
    
    # return the network of neuron constraints
    return net

# ===========================================================
# = Function to check the correctness of the network output =
# ===========================================================

def get_network_inputs(target, cpi_on_core, ann_type):
    '''
    target - the target core
    ann - the corresponding neural network
    cpi_on_core - a matrix (list of list) with the CPI values for each core
    NOTE: you can use both scaled and non-scaled CPIs
    '''
    # get useful stuff
    cores = range(ptf_w * ptf_h)
    
    # evaluate the considered network
    # synhtesize the nework input
    inputs = []
    
    # TODO: be sure that the input parameters match the real ones
    
    # the first input is the average CPI on the target core
    inputs.append(np.mean(cpi_on_core[target]))
    
    # the second input is the min CPI on the target core
    if (ann_type == 1):
        inputs.append(np.min(cpi_on_core[target]))
    
    # the third input is the second min CPI on the target core
    # inputs.append(sorted(cpi_on_core[target])[1])
    
    # the fourth input is the average of the average CPI on the neighbors
    neighbors = [k for k in cores if hinf_norm(k,target) == 1]
    inputs.append(np.mean([np.mean(cpi_on_core[k]) for k in neighbors]))
    
    # the fifth input is the average of the average CPI on the other cores
    others = [k for k in cores if hinf_norm(k,target) > 1]
    inputs.append(np.mean([np.mean(cpi_on_core[k]) for k in others]))
    
    return inputs
    

def check_network(target, ann, cpi_on_core, OUTv, params, ann_type):
    '''
    Check if the value of OUTv is reasonably close the correct ANN prediction
    for this mapping.
    '''
    # get useful stuff
    # cores = range(ptf_w * ptf_h)
    
    # print 'Checking network for core %d' % target
    
    # scale down the network input
    inputs = get_network_inputs(target, cpi_on_core, ann_type)
    inputs = [float(v) / params.prec for v in inputs]
    
    # compute the estimated prediction (this takes into account rounding issues)
    est_pred = ann.eval(inputs, prec = params.prec) 
    # compute the 'real' prediction (no unnecessary rounding)
    real_pred = ann.eval(inputs)
    
    # the estimated prediction must match
    comp_err = OUTv.Value() - int(round(est_pred * params.prec))
    if abs(comp_err) > 5:
        print 'XXX Rounding error: %d' % abs(comp_err)
    # assert abs(comp_err) <= 10
    
    # return the error w.r.t. the 'real' prediction
    return real_pred - OUTv.Value() / float(params.prec)

        
# =========================================
# = Solving an LNS dispatching subproblem =
# =========================================

# restart types
restart_constant, restart_luby = 0,     1
search_random, search_custom, search_naive, search_custom2 = 0, 1, 2, 3

class solve_dispatching_subproblem_type:
    def __init__(self, prec,
                       step,
                       tlim,
                       max_sol_num,
                       restart_base,
                       restart_type,
                       optimize,
                       search_strat,
                       reserved_tasks,
                       res_level,
                       log_branch_count,
                       time_offset,
                       branch_offset,
                       fail_offset,
                       net_prop_level,
                       seed):
        self.prec = prec
        self.step = step
        self.tlim = tlim
        self.max_sol_num = max_sol_num
        self.restart_base = restart_base
        self.log_branch_count = log_branch_count
        self.time_offset = time_offset
        self.branch_offset = branch_offset
        self.fail_offset = fail_offset
        self.restart_type = restart_type
        self.optimize = optimize
        self.search_strat = search_strat
        self.reserved_tasks = reserved_tasks
        self.res_level = res_level
        self.net_prop_level = net_prop_level
        self.seed = seed

def solve_dispatching_subproblem(wld_original, anns, pre_mapping, params,
                                 lbound, ann_type):
    """
    - wld is the workload (a list of CPI values)
    - anns is the platform description (a list of ANN2L objects)
    - pre_mapping is a list such that pre_mapping[i] = j if task i is mapped on
      core j, while pre_mapping[i] is None if the task needs to be mapped by
      the solver
    - params is an instance of solve_dispatching_subproblem_type
    - lbound is a lower bound on the cost of the acceptable solution. This is a
      non-normalized, non-scaled value (hence, something in [0,1])
    - ann_type is an integer that specifies the type of network inputs
    """
    # get copies/pointers to all input params
    prec = params.prec
    step = params.step
    tlim = params.tlim
    restart_base = params.restart_base
    restart_type = params.restart_type
    log_branch_count = params.log_branch_count
        
    # pre_mapping[pre_mapping.index(None)] = 19 # DEBUG (to have a partially filled core)
 
    # list of all cores
    cores = range(ptf_w * ptf_h)
    tasks = range(len(wld_original))
    
    # number of tasks per cores
    assert len(wld_original) % len(cores) == 0
    ntpc = len(wld_original) / len(cores)
    
    # number of pre-mapped tasks for each core
    pre_map_num = [len([v for v in pre_mapping if v == k]) for k in cores]
    for k in cores: # debug
        assert pre_map_num[k] <= ntpc
    
    # list of the partially free cores (with less than "ntpc" tasks)
    pf_cores = [k for k in cores if pre_map_num[k] < ntpc]
    
    # list of non-mapped tasks
    free_tasks = [i for i in tasks if pre_mapping[i] is None]
    
    # check: reserved efficiency tasks can only be used with ANN modes
    assert params.reserved_tasks is None or ann_type != -1
    
    # if there are free tasks with efficiency guarantees, keep a list of them
    if params.reserved_tasks is not None:
        res_free_tasks = [i for i in params.reserved_tasks if pre_mapping[i] is None]
        
    # if there are non free tasks with efficiency guarantees, memorize the core
    # where they are mapped
    if params.reserved_tasks is not None:
        res_cores = [k for k in cores 
                     if any(pre_mapping[i] == k for i in params.reserved_tasks)]
    
    # normalize and scale the workload
    wld = [int(round(v*prec)) for v in normalize_cpi(wld_original)]
    # print 'NORMALIZED AND SCALED WORKLOAD:', wld
    
    # BUILD MODEL **************************************************************
    # build a solver
    s = pycp.Solver('The Solver')
    
    # reseed the solver
    if params.seed is not None:
        s.ReSeed(params.seed)    
    
    # PREPARE VARIABLES
    # mapping variables (WARNING: those are indirect mapping variables), meaning
    # that their value is not the index of a core, but an index for the list of
    # the partially free cores. This trick simplyfies the use of the MapDomain
    # constraint in or-tools
    C = {i:s.IntVar(0, len(pf_cores)-1, 'C[%d]' % i) for i in free_tasks}
    
    # binary mapping variables (to compute the average)
    X = {(k,i) : s.BoolVar('X[%d,%d]' % (k, i)) for i in free_tasks for k in pf_cores}
    
    # average CPI variables (for the ANN)
    AVG = [s.IntVar(-prec, prec, "AVG[%d]" % k) for k in cores]
    
    # min CPI variables (for the ANN, only for some network types)
    if (ann_type == 1):
        MIN = [s.IntVar(-prec, prec, "MIN[%d]" % k) for k in cores]
    else:
        MIN = None
    
    # second min CPI variables
    # ATTN no network is using this anymore
    MIN2 = None #[s.IntVar(-prec, prec, "MIN2[%d]" % k) for k in cores]
        
    # network output variables
    if ann_type != -1:
        OUT = {k:s.IntVar(-sys.maxint-1, sys.maxint, "OUT[%d]" % k) for k in pf_cores}
        # if non-free tasks with guaranteed performance exist, we need to
        # predict the efficiency also for the cores where they are running
        if params.reserved_tasks is not None:
            # add a new variable for each core in res_cores that does not appear
            # in pf_cores
            for k in res_cores:
                if k not in OUT:
                    OUT[k] = s.IntVar(-sys.maxint-1, sys.maxint, "OUT[%d]" % k)
    else:
        OUT = None
        
    # cost variable
    OBJ = s.IntVar(-sys.maxint-1, sys.maxint, "OBJ")
    
    # BUILD THE CONSTRAINTS
    # build the chaining constraint between the C and the X variables
    # WARNING: the "pf_cores" array is used as an index for the values of C
    for i in free_tasks:
        # identify the binary variables referring to the task
        V = [X[k,i] for k in pf_cores]
        # s.Add(s.MapDomain(C[i], V))
        s.Add(C[i].MapTo(V))
        
    # an equal number of tasks on all cores
    # (the list comprehension builds the cardinality variables)
    pf_cards = [ntpc - pre_map_num[k] for k in pf_cores]
    s.Add(s.Distribute([C[i] for i in free_tasks], [s.IntVar(v,v) for v in pf_cards]))
    
    # build the constraint to compute the average, max and min CPI values
    for k in cores:
        # compute pre-filled part
        wld_on_core = [cpi for i, cpi in enumerate(wld) if pre_mapping[i] == k]
        avg_num = float(np.sum(wld_on_core))
        avg_den = float(ntpc)
        # in case the core is totally filled
        if len(wld_on_core) == ntpc:
            s.Add(AVG[k] == int(round(avg_num / avg_den)))
            if (ann_type == 1):
                s.Add(MIN[k] == int(np.min(wld_on_core)))
            #s.Add(MIN2[k] == int(sorted(wld_on_core)[1]))
        # in case the core is partially filled or empty
        elif len(wld_on_core) > 0:
            V = [X[k,i] for i in free_tasks]
            cpi = [wld[i] for i in free_tasks]
            # the average is a division
            s.Add(pycp.RoundedProductConstraint(s,
                    int(avg_num) + s.ScalProd(V, cpi), # input
                    AVG[k], # output
                    1.0 / avg_den )) # coefficient
            # the min must be computed on both the pre-mapped and the free tasks
            # -- old constraint (min only)
            if (ann_type == 1):
                s.Add(MIN[k] == s.Min([s.IntConst(int(np.min(wld_on_core)))] +
                                       [cpi[i]*v for i,v in enumerate(V)]))
            # --- new constraint (min and second min)
            # s.Add(pycp.RegretConstraint(s,
            #   [s.IntConst(int(np.min(wld_on_core)))] + [cpi[i]*v for i,v in enumerate(V)],
            #   MIN[k],
            #   MIN2[k]))
        
        # in case the core is totally free
        else:
            V = [X[k,i] for i in free_tasks]
            cpi = [wld[i] for i in free_tasks]
            # the average is a division
            s.Add(pycp.RoundedProductConstraint(s,
                    s.ScalProd(V, cpi), # input
                    AVG[k], # output
                    1.0 / avg_den )) # coefficient
            # the min must be computed only for the the free tasks
            # --- old constraint (min only)
            if (ann_type == 1):
                s.Add(MIN[k] == s.Min([cpi[i]*v for i,v in enumerate(V)]))
            # --- new constraint (min and second min)
            #s.Add(pycp.RegretConstraint(s,
            #        [cpi[i]*v for i,v in enumerate(V)],
            #        MIN[k],
            #        MIN2[k]))
            
    # build all the Neuron Constraints
    if ann_type != -1:
        net_csts = {} 
        for k in OUT:
            net_csts[k] = build_network(s, k, anns[k], AVG, MIN, MIN2, OUT[k],
                                        params, ann_type)        
                                        
    # define the reservation requirements
    if params.reserved_tasks is not None:
        # the efficiency threshold, scaled and normalized
        th = normalize_eff([params.res_level])[0]
        th = int(round(prec * th))
        # the cores that are not partially free and contain a taks with
        # guaranteed quality are subject to a required efficiency constraints
        for k in res_cores:
            s.Add(OUT[k] >= th)
        # the efficiency of the (partially free) cores where reserved tasks can
        # be mapped must satisfy the required efficiency level
        pf_core_OUT = [OUT[k] for k in pf_cores]
        for i in res_free_tasks:
            s.Add(s.Element(pf_core_OUT, C[i]) >= th)
       
    # define the objective variable (min among all network outputs or among all
    # average CPI values, depending on the platform model)
    if ann_type != -1:
        s.Add(OBJ == s.Min([OUT[k] for k in pf_cores]))
    else:
        s.Add(OBJ == s.Min([anns[k] * AVG[k] for k in cores]))
        
    # lower bound on the objective
    if lbound is not None:
        if ann_type != -1:
            # compute the normalized and incremented bound
            # note: the "normalize_eff" function is designed to work with lists
            n_lbound = normalize_eff([lbound + step])[0]
        else:
            n_lbound = normalize_cpi([lbound + step])[0]
        # add the constraint and print the normalized bound value
        s.Add(OBJ >= int(ceil(n_lbound * prec)))
        print '- normalized lower bound', int(ceil(n_lbound * prec))
    
    # DEFINE THE OBJECTIVE
    if params.optimize:
        objective = s.Maximize(OBJ, int(ceil(step*eff_range*prec)) )
    
    # SOLVE THE PROBLEM ********************************************************
    # compute the time spent in building the model
    model_time = s.WallTime()
    
    # define the search strategy
    search_vars = [C[i] for i in free_tasks]
    
    if params.search_strat == search_random:
        search_db = s.Phase(search_vars,
                            s.CHOOSE_MIN_SIZE_LOWEST_MIN,
                            s.ASSIGN_RANDOM_VALUE)
    elif params.search_strat == search_custom:
        if params.reserved_tasks is None:
            search_db = pycp.TAWDSearch(s, search_vars,
                                           [wld[i] for i in free_tasks])
        else:
            # TODO this works only for searching the full problem!!!
            search_vars_res = [C[i] for i in res_free_tasks]
            wld_res = [wld[i] for i in res_free_tasks]
            
            other_free_tasks = [i for i in free_tasks if i not in res_free_tasks]
            search_vars_otr = [C[i] for i in other_free_tasks]
            wld_otr = [wld[i] for i in other_free_tasks]
            
            search_db = pycp.TAWDResSearch(s, search_vars_res, search_vars_otr,
                                              wld_res, wld_otr)
    elif params.search_strat == search_custom2:
        print 'search_custom2'
        # simplified version of the custom search (min size domain for var
        # assignment, lowest workload core for the val assignment)
        search_db = pycp.TAWDSearch2(s, search_vars,
                                        [wld[i] for i in free_tasks])
    elif params.search_strat == search_naive:
        search_db = s.Phase(search_vars,
                            s.CHOOSE_FIRST_UNBOUND,
                            s.ASSIGN_MIN_VALUE)
    else:
        if len(free_tasks) != len(wld):
            print >> sys.err, 'ERROR: unknown search strategy'
            sys.exit(1)
    
    # prepare the list of the search monitors (this initially contains only
    # the problem objective, or nothing at all - if optimization within an
    # iteration was not requested)
    if params.optimize:
        search_monitors = [objective]
    else:
        search_monitors = []
    
    # set a time limit
    if tlim is not None:
        search_monitors.append(s.TimeLimit(tlim))
        
    # configure restarts
    if restart_base is not None:
        if restart_type == restart_luby:
            search_monitors.append(s.LubyRestart(restart_base))
        else:
            search_monitors.append(s.ConstantRestart(restart_base))
    
    # search log
    if log_branch_count is not None:
        search_monitors.append(s.SearchLog(log_branch_count, OBJ))
        
    # start search!
    s.NewSearch(search_db, search_monitors)
    sol = None # best solution
    nsol = 0 # number of solution
    check_time = 0 # time to check the solution validity
    while s.NextSolution():        
        # one more solution
        nsol += 1
             
        # store (partial) solution
        sol = {k:pf_cores[C[k].Value()] for k in C}
                
        # collect stats
        time, branches, fails = s.WallTime() - model_time - check_time, s.Branches(), s.Failures()
        if ann_type != -1:
            de_normalized_obj = de_normalize_eff([OBJ.Value() / float(prec)])[0]
        else:
            de_normalized_obj = de_normalize_cpi([OBJ.Value() / float(prec)])[0]
            
        # check solution
        check_time_start = s.WallTime()
        check_solution(C, AVG, MIN, MIN2, OUT, pre_mapping, wld, anns, pf_cores, params, ann_type)
        
        # print stats
        print '- solution found, obj var is: %d' % OBJ.Value()
        print '- de-normalized obj var: %f' % de_normalized_obj
        print '- time (local/global): %.3f/%.3f' % (time/1000.0, (params.time_offset+time)/1000.0)
        print '- branches (local/global): %d/%d' % (branches, params.branch_offset+branches)
        print '- fails (local/global): %d/%d' % (fails, params.fail_offset+fails)
        
        # record the time to process the solution 
        check_time += s.WallTime() - check_time_start
        
        # stop if optimization was not requested or if the maximum number of
        # solutions has beenn found
        if not params.optimize or (nsol == params.max_sol_num):
            break
        
    # terminate search
    s.EndSearch()
    
    # return results
    # tot_time = max(10, s.WallTime() - model_time - check_time) # at least 10 ms
    tot_time = max(1, s.WallTime() - check_time)    
    return sol, tot_time, s.Branches(), s.Failures()
    
def check_solution(C, AVG, MIN, MIN2, OUT, pre_mapping, wld, anns, pf_cores, params, ann_type):
    '''
    C - the core variables
    AVG - the average CPI variables
    MIN - the minimum CPI variables
    pre_mapping - the array with the partial mapping
    wld - the NORMALIZED workload
    params - the parameters for solve_dispatching_subproblem
    '''
    # build useful ranges
    cores = range(ptf_w * ptf_h)
    
    # start solution cheking
    print '--- checking solution'
    
    # collect CPI values on each core
    cpi_on_core = [[] for k in cores]
    for i, v in enumerate(pre_mapping):
        core_idx = v if v is not None else pf_cores[C[i].Value()]
        cpi_on_core[core_idx].append(wld[i])

    # check distribution constraints
    for k in cores:
        assert len(cpi_on_core[k]) == len(wld) /len(cores)
    
    # check average values
    for k in cores:
        assert AVG[k].Value() == int(round(np.mean(cpi_on_core[k])))
        
    # check minimum values
    if ann_type == 1:
        for k in cores:
            assert MIN[k].Value() == int(np.min(cpi_on_core[k]))
        
    # check second minimum values
    #for k in cores:
    #    assert MIN2[k] == int(sorted(cpi_on_core[k])[1])
    
    # check the required efficiency constraints
    if params.reserved_tasks is not None:
        th = normalize_eff([params.res_level])[0]
        th = int(round(params.prec * th))
        for i in params.reserved_tasks:
            k = pre_mapping[i] if pre_mapping[i] is not None else pf_cores[C[i].Value()]
            assert OUT[k].Value() >= th
        
    # check all networks
    if ann_type != -1:
        max_rouding_err = 0
        for k in pf_cores:
            # this function checks if the network evaluation is done correctly,
            # taking into account the need to do rounding in the model. The
            # check is done via assertions
            err = check_network(k, anns[k], cpi_on_core, OUT[k], params, ann_type)
            # update worst case rounding error
            max_rouding_err = max(err, max_rouding_err)
            # this assertion checks how big is the error due to rounding
            # assert err < 10*(1.0 / params.prec)
        
        # if we got here, everything was OK
        print '--- solution ok! Maximum rounding error: %f' % max_rouding_err   
    else:
        print '--- solution ok!'
        
# def check_solution(C, AVG, MIN, MIN2, OUT, pre_mapping, wld, anns, pf_cores, params, ann_type):
#     '''
#     C - the core variables
#     AVG - the average CPI variables
#     MIN - the minimum CPI variables
#     pre_mapping - the array with the partial mapping
#     wld - the NORMALIZED workload
#     params - the parameters for solve_dispatching_subproblem
#     '''
#     # build useful ranges
#     cores = range(ptf_w * ptf_h)
#     
#     # start solution cheking
#     print '--- checking solution'
#     
#     # collect CPI values on each core
#     cpi_on_core = [[] for k in cores]
#     for i, v in enumerate(pre_mapping):
#         core_idx = v if v is not None else pf_cores[C[i].Value()]
#         cpi_on_core[core_idx].append(wld[i])
# 
#     # check distribution constraints
#     for k in cores:
#         assert len(cpi_on_core[k]) == len(wld) /len(cores)
#     
#     # check average values
#     for k in cores:
#         assert AVG[k] == int(round(np.mean(cpi_on_core[k])))
#         
#     # check minimum values
#     if ann_type == 1:
#         for k in cores:
#             assert MIN[k] == int(np.min(cpi_on_core[k]))
#         
#     # check second minimum values
#     #for k in cores:
#     #    assert MIN2[k] == int(sorted(cpi_on_core[k])[1])
#         
#     # check all networks
#     if ann_type != -1:
#         max_rouding_err = 0
#         for idx, k in enumerate(pf_cores):
#             # this function checks if the network evaluation is done correctly,
#             # taking into account the need to do rounding in the model. The check
#             # is done via assertions
#             err = check_network(k, anns[k], cpi_on_core, OUT[idx], params, ann_type)
#             # update worst case rounding error
#             max_rouding_err = max(err, max_rouding_err)
#             # this assertion checks how big is the error due to rounding
#             # assert err < 10*(1.0 / params.prec)
#         
#         # if we got here, everything was OK
#         print '--- solution ok! Maximum rounding error: %f' % max_rouding_err   
#     else:
#         print '--- solution ok!'
    
# =================================================
# = Functions to handle Large Neighborhood Search =
# =================================================

class improve_via_lns_type:
    def __init__(self, prec,
                       step,
                       tlim,
                       iterlim,
                       local_tlim,
                       local_snum,
                       restart_base,
                       restart_type,
                       reserved_tasks,
                       res_level,
                       net_prop_level,
                       strategy,
                       neighborhood,
                       seed):
        self.prec = prec
        self.step = step
        self.tlim = tlim
        self.iterlim = iterlim
        self.local_tlim = local_tlim
        self.local_snum = local_snum
        self.restart_base = restart_base
        self.restart_type = restart_type
        self.reserved_tasks = reserved_tasks
        self.res_level = res_level
        self.net_prop_level = net_prop_level
        self.strategy = strategy
        self.neighborhood = neighborhood
        self.seed = seed

def improve_via_lns(wld, anns, fsol, opts, ann_type):
    '''
    wdl: a list of task CPIs, representing the workload
    anns: a list of ANN2L objects, representing the platform
    fsol: a list of core indices (one per task), representing the initial sol
    opts: an instance of "improve_via_lns_type"
    '''
    # get some useful ranges, parameters, etc.
    cores = range(ptf_w * ptf_h)

    # get normalized CPI values
    nwld = normalize_cpi(wld)
    
    # make the first solution the current incumbent.
    sol = [v for v in fsol]
    
    # prepare global stats
    time, branches, fails = 0, 0, 0
    
    # loop until the time budget is gone
    cnt = 0
    while time < opts.tlim:
        # translate the solution to a "cpi on core" format
        sol_cpi = [[nwld[i] for i, k2 in enumerate(sol) if k2 == k] for k in cores]
        
        # de-normalized avergae CPI per core
        avgcpi = [np.mean(t) for t in sol_cpi]
        avgcpi = de_normalize_cpi(avgcpi)

        # evaluate efficiency on the whole platform
        if ann_type != -1:
            eff = [anns[k].eval(get_network_inputs(k, sol_cpi, ann_type)) for k in cores]
            eff = de_normalize_eff(eff)
        
        # store the initial worst case efficiency and average CPI
        if cnt == 0:
            initial_avgcpi = avgcpi # initial w.c. average CPI
            if ann_type != -1:
                initial_eff = eff # initial w.c. efficiency
            
        # stop the optimization process if the maximum number of iterations has
        # been performed
        # NOTE: this check is not performed in the while loop to allow the
        # initialization of a few fields
        if cnt >= opts.iterlim:
            break
            
        # start the actual iteration
        print ('LNS iteration #%d ' % cnt) + '='*40
        print '- worst case average CPI: %.3f' % min(avgcpi)
        if ann_type != -1:
            print '- worst case efficiency: %.3f' % min(eff)
        
        # identify the fragment to relax
        if ann_type != -1:
            # identify the fragment to relax
            if opts.neighborhood is not None:
                pre_mapping, lbound = select_fixed_neighborhood(sol, opts)
            else:
                pre_mapping, lbound = select_fragment_3(sol, sol_cpi, eff, opts)
            
            # # pre_mapping, lbound = select_fragment_1(sol, sol_cpi, eff, opts)
            # pre_mapping, lbound = select_fragment_3(sol, sol_cpi, eff, opts)
        else:
            # identify the fragment to relax
            if opts.neighborhood is not None:
                pre_mapping, lbound = select_fixed_neighborhood(sol, opts)
            else:
                pre_mapping, lbound = select_fragment_avgcpi(sol, sol_cpi, opts)
            
            # pre_mapping, lbound = select_fragment_avgcpi(sol, sol_cpi, opts)

        # print '- relaxed tasks: %s' % str([i for i, k in enumerate(pre_mapping) if k is None])
        if ann_type != -1:
            print '- efficiency to beat: %.3f' % lbound
        else:
            print '- average CPI to beat: %.3f' % lbound
        
        # compute the local time limit
        local_tlim = min(int(opts.local_tlim), opts.tlim-time)
        #local_tlim = max(500, local_tlim) # capping (0.5 sec minimum)
    
        # build option pack for the subproblem
        sp_opts = solve_dispatching_subproblem_type( 
                            opts.prec, # precision factor
                            opts.step, # step (a double, typically < 1)
                            local_tlim, # time limit for each attempt
                            opts.local_snum, # maximum number of solutions
                            opts.restart_base, # base fails for restarts
                            opts.restart_type, # restart strategy
                            True, # optimize within a single iteration
                            opts.strategy, # the search strategy to be used
                            opts.reserved_tasks, # tasks with quality guarantees
                            opts.res_level, # req. eff in that case
                            None, # log every N branches (None = no log)
                            time, # time offset (for printing stuff)
                            branches, # branch offset (for printing stuff)
                            fails, # fail offset (for printing stuff)
                            opts.net_prop_level, # trigger Lagrangian prop.
                            opts.seed) # seed for the RNGs
        
        # solve the dispatching subproblem and collect stats
        lns_sol, lns_time, lns_branches, lns_fails = \
           solve_dispatching_subproblem(wld, anns, pre_mapping, sp_opts,
                                        lbound, ann_type)

        print '- lns time: %f' % (lns_time/1000.0)
        print '- lns branches: %d' % lns_branches
        print '- lns fails: %d' % lns_fails
        
        # collect stats
        time += lns_time
        branches += lns_branches
        fails += lns_fails
        
        # replace the current incumbent (if any was found)
        if lns_sol is not None:
            for i in lns_sol:
                sol[i] = int(lns_sol[i])
                    
        # one more iteration is completed
        cnt += 1
        
    # collect the final values for the performance metrics
    final_sol_cpi = [[nwld[i] for i, k2 in enumerate(sol) if k2 == k] for k in cores]
    final_avgcpi =  [np.mean(t) for t in final_sol_cpi]
    final_avgcpi = de_normalize_cpi(final_avgcpi)
    if ann_type != -1:
        final_eff = [anns[k].eval(get_network_inputs(k, final_sol_cpi, ann_type)) for k in cores]
        final_eff = de_normalize_eff(final_eff)    
    
    # here the LNS process is over

    print '\nLNS optimization is over' + '='*40
    print 'initial worst case average CPI: %.3f' % min(initial_avgcpi)
    print 'final worst case average CPI: %.3f' % min(final_avgcpi)
    if ann_type != -1:
        print 'initial worst case efficiency: %.3f' % min(initial_eff)
        print 'final worst case efficiency: %.3f' % min(final_eff)
    print 'number of iterations: %d' % cnt
    print 'total time: %f' % time
    print 'total branches: %d' % branches
    print 'total fails: %d' % fails
    
    # print the solution
    print '\nFinal Solution: ' + ', '.join('%d->%d' % (i, k)
                                           for i,k in enumerate(sol))
                                           
    # print the average CPI on each core, following the platform layout
    print '\nAverage CPI map:'
    for i in xrange(ptf_h):
        row_avgcpi = final_avgcpi[i*ptf_w:(i+1)*ptf_w]
        print ','.join('\t%.2f' % v for v in row_avgcpi)
    
    # print the efficiency map
    if ann_type != -1:
        print '\nEfficiency map:'
        for i in xrange(ptf_h):
            row_eff = final_eff[i*ptf_w:(i+1)*ptf_w]
            print ','.join('\t%.3f' % v for v in row_eff)
    
# ====================================
# = Neighborhood selection functions =
# ====================================

def select_fragment_1(sol, sol_cpi, eff, opts):
    # build a sorted list of core indices with their efficiency
    ranked = sorted([(k,e) for k, e in enumerate(eff)], key = lambda t: t[1])
    
    # select N/2 of the best cores and N/2 of the worst ones
    nlow, nhigh, nrand = 1, 2, 3
    best_cores = [k for (k,e) in ranked[:nlow] ] # low efficiency cores
    worst_cores = [k for (k,e) in ranked[-nhigh:] ] # high efficiency cores
    random_cores = np.random.choice([k for k,e in ranked[nlow:-nhigh]], nrand)
    
    # relax the best and the worst cores
    relaxed = best_cores + worst_cores + list(random_cores)
    pre_mapping = [(None if k in relaxed else k) for k in sol]
    
    # the bound to be beated is the worst case efficiency among the relaxed cores
    lbound = min(eff[k] for k in relaxed)
    
    # return the required stuff
    return pre_mapping, lbound
    
def select_fragment_2(sol, sol_cpi, eff, opts):
    # compute the minimum efficiency
    min_eff = min(eff)
    
    # split cores into "critical" and "slack" ones
    crit_cores, slack_cores = [], []
    for k, e in enumerate(eff):
        if e == min_eff:
            crit_cores.append(k)
        else:
            slack_cores.append(k)
        
    # select the cores to relax
    ncrit, nslack = 1, 3
    relaxed = list(np.random.choice(crit_cores, ncrit))
    relaxed += list(np.random.choice(slack_cores, nslack))
    
    # relax
    pre_mapping = [(None if k in relaxed else k) for k in sol]
    
    # the bound to be beated is the worst case efficiency among the relaxed cores
    lbound = min(eff[k] for k in relaxed)
    
    # return the required stuff
    return pre_mapping, lbound
    
def select_fragment_3(sol, sol_cpi, eff, opts):
    # compute mean CPI for each core
    avgcpi = [np.mean(t) for t in sol_cpi]
    
    # compute the minimum efficiency
    min_eff = min(eff)
    
    # split cores into "critical" and "slack" ones
    crit_cores, slack_cores = [], []
    # selection probability for the slack cores
    slack_selp = []
    for k, e in enumerate(eff):
        if e == min_eff:
            crit_cores.append(k)
        else:
            slack_cores.append(k)
            selp = (1+eff[k]) / (1+avgcpi[k]) # selection probability
            slack_selp.append(selp) # store the selection probability
        
    # select the cores to relax
    ncrit, nslack = 1, 3
    relaxed = list(np.random.choice(crit_cores, ncrit))
    for cnt in xrange(nslack):
        # draw a (scaled) random number
        r = np.random.rand() * np.sum(slack_selp)
        # find the correponding index
        for i, k in enumerate(slack_cores):
            if slack_selp[i] > 0:
                if r <= 0:
                    relaxed.append(k)
                    slack_selp[i] = 0
                    break
                else:
                    r -= slack_selp[i]
    
    # relax
    pre_mapping = [(None if k in relaxed else k) for k in sol]
    
    # the bound to be beated is the worst case efficiency among the relaxed cores
    lbound = min(eff[k] for k in relaxed)
    
    # return the required stuff
    return pre_mapping, lbound

def select_fixed_neighborhood(sol, opts):
    # relax a fixed set of cores
    relaxed = opts.neighborhood
            
    # relax
    pre_mapping = [(None if k in relaxed else k) for k in sol]
    
    # the bound to be beated is the worst case pred among the relaxed cores
    lbound = -1
    
    # return the required stuff
    return pre_mapping, lbound
    
def select_fragment_avgcpi(sol, sol_cpi, opts):
    # compute mean CPI for each core (normalized)
    avgcpi = [np.mean(t) for t in sol_cpi]
    
    # compute the worst case average CPI
    min_avgcpi = min(avgcpi)
    
    # split cores into "critical" and "slack" ones
    crit_cores = []
    # selection probability for the slack cores
    for k, v in enumerate(avgcpi):
        if v == min_avgcpi:
            crit_cores.append(k)
    # print 'crit_cores', crit_cores

    # number of critical cores to relax (completely)
    ncrit = 1
    
    # number of tasks to be relaxed at random
    nrandom = 4 * len(sol) / (ptf_w * ptf_h)
    
    # pick a critical core at random
    relaxed = list(np.random.choice(crit_cores, ncrit))
    # print 'relaxed', relaxed
    
    # compute the lower bound for LNS optimization
    lbound = min(avgcpi[k] for k in relaxed)
    lbound = de_normalize_cpi([lbound])[0] # de-normalize
    # print 'lbound', lbound
    
    # relax the tasks on the critical cores
    pre_mapping = [(None if k in relaxed else k) for k in sol]
    # print 'pre_mapping', pre_mapping
    
    # pick the other tasks to relax at random
    the_others = [i for i, k in enumerate(sol) if k is not None]
    # print 'the_others', the_others
    the_others_relaxed = list(np.random.choice(the_others, nrandom))
    # print 'the_others_relaxed', the_others_relaxed
    
    # relax the freshly selected cores
    for i in the_others_relaxed:
        pre_mapping[i] = None
    # print 'pre_mapping', pre_mapping
        
    # return the required stuff
    return pre_mapping, lbound

# ======================================
# = Functions to find a first solution =
# ======================================

def find_first_solution(wld, nets):
    # get number of tasks
    n = len(wld)
    assert n % 2 == 0
    
    # get number of cores
    m = ptf_w * ptf_h
    assert n % m == 0
    
    # define the range of cores
    cores = range(ptf_w * ptf_h)
    
    # build a list with task indices and CPIs
    tasks = [(i,cpi) for i, cpi in enumerate(wld)]
    
    # sort the list by increasing CPI
    tasks.sort(key = lambda t: t[1])
        
    # use the inverse order on the second half of the list
    tasks = tasks[:n/2] + tasks[n/2:][::-1]
    
    # compute the mapping by distributing tasks to cores by incresing core index
    mapping = [None for cpi in wld]
    for j, t in enumerate(tasks):
        mapping[t[0]] = j % m
        
    # check the balancing constraints
    ntpc = n / m
    for k in cores:
        assert len([k2 for k2 in mapping if k2 == k]) == ntpc
        
    # # for debug: use a stupid mapping
    # for i in xrange(n):
    #     mapping[i] =  i / (n/m)
    
    # return the mapping
    return mapping, 0


def find_first_solution_adv(wld, anns, opt_pack, ann_type):
    # build option pack for the subproblem
    sp_opts = solve_dispatching_subproblem_type( 
                        opt_pack.prec, # precision factor
                        0, # step (a double, typically < 1)
                        opt_pack.global_tlim*1000, # time limit for each attempt
                        1, # maximum number of solutions
                        None, #opt_pack.restart_base, # base fails for restarts
                        None, #opt_pack.restart_strat, # restart strategy
                        False, # optimize within a single iteration
                        search_custom, # the search strategy to be used
                        reserved_tasks, # tasks with a quality guarantee
                        opt_pack.res_level, # req. eff. for them
                        None, # log every N branches (None = no log)
                        0, # time offset (for printing stuff)
                        0, # branch offset (for printing stuff)
                        0) # fail offset (for printing stuff)
    
    # build a totally empty pre-mapping
    pre_mapping = [None for cpi in wld]
        
    # solve the dispatching subproblem and collect stats
    sol, time, branches, fails = \
       solve_dispatching_subproblem(wld, anns, pre_mapping, sp_opts,
                                    None, # lower bound
                                    ann_type)

    # return the solution
    return [sol[i] for i in xrange(len(wld))], time

# ===============
# = Main script =
# ===============

# usage message
help_message = """
options:
--help                                      show this help message
--wld-idx (int)                             solve the i-th workload in the input file
--prec (int)                                precision factor
--step (in ]0,1])                           required (relative) improvement for accepting a solution
--global-tlim (int)                         time limit for the whole search process (in millisec)
--global-iter (int)                         iteration limit for the whole search process
--seed  (int)                               seed for the RNG
--local-tlim (int)                          time limit for each iteration (in millisec)
--local-nsol (int)                          stop an iteration when N solutions are found (<= 0 for no limitation)
--restart-base (int)                        base fail limit for the restarts (0 for no restarts)
--restart-strat [luby|constant]             strategy to update the restart fail limit
                                            (this option is ignored if --restart-base is 0)
--use-acpi-model ???                        use simplified model based on the ACPI values TODO: check this
--reservation-file (string)                  file containing the job number with a minimum efficiency requirement
--reservation-level (in [0,1])              minimum efficiency requirement
--search-strategy  [random|naive|ad_hoc]    search strategy within each iteration
--explore-neighborhood (int{,int})          forced choice of the cores to relax
--net-prop-level [0|1|2|3]                 0 - basic neuron constraint propagation, non-incremental
                                            1 - basic neuron constraint propagation, incremental
                                            2 - basic neuron constraint propagation + lagrangian z propagation
                                            3 - basic neuron constraint propagation + lagrangian x and z propagation
"""

# Usage exception class
class Usage(Exception):
    def __init__(self, msg):
        self.msg = "ERROR: " + str(msg)

# options
short_opts = 'h'
long_opts = ['help', 'wld-idx=', 'prec=', 'step=',
             'global-tlim=', 'global-iter=', 'seed=',
             'local-tlim=', 'local-nsol=', 'restart-base=', 'restart-strat=',
             'use-acpi-model', 'reservation-file=', 'reservation-level=',
             'search-strategy=', 'explore-neighborhood=',
             'net-prop-level=']

# stardard option pack
class OptPack:
    def __init__(self):
        self.wld_idx = 0 # index of the workload tuple in the workload file
        self.prec = 10000 # precision factor
        self.step = 0.005 # optimization step
        self.global_tlim = 30 # time limit on the overall process
        self.global_iter = 1000 # maximum number of LNS iterations
        self.local_tlim = 3 # time limit for each LNS iteration
        self.local_nsol = 1 # maximum number of solutions for each LNS iter.
        self.restart_base = 100 # base number of fails for the restart strategy
        self.restart_strat = restart_luby # the restart strategy
        self.seed = None
        self.acpi_model = False
        self.res_file = None
        self.res_level = 0.90 # at least X%  eff. for the reserved tasks
        self.net_prop_level = 0 # by default, use simplest propagation
        self.strategy = search_random # by default, use randomized search
        self.neighborhood = None
                
    def __repr__(self):
        s  = '--wld-idx %d' % self.wld_idx
        s += ' --prec %d' % self.prec
        s += ' --step %d' % self.step
        s += ' --global-tlim %d' % self.global_tlim
        s += ' --global-iter %d' % self.global_iter
        s += ' --local-tlim %d' % self.local_tlim
        s += ' --local-nsol %d' % self.local_nsol
        s += ' --net-prop-level %d' % self.net_prop_level
        s += ' --restart-base %d' % (self.restart_base if self.restart_base is not None else 0)
        s += ' --restart-strat %s' % ('luby' if self.restart_strat == restart_luby
                                      else 'constant')
        s += ' --search-strategy %s' % ('random' if self.strategy == search_random else
                                        ('naive' if self.strategy == search_naive else 'ad_hoc'))
        if self.neighborhood is not None:
            neigh_strings = [str(v) for v in self.neighborhood]
            s += ' --explore-neighborhood ' + ','.join(neigh_strings)
        if self.seed is not None:
            s += ' --seed %d' % self.seed
        if self.acpi_model:
            s += ' --use-acpi-model'
        if self.res_file is not None:
            s += ' --reservation-file %s' % self.res_file
            s += ' --reservation-level %f' % self.res_level
        return s

# apply option
def apply_option(opt_pack, option, value):
    if option == '--wld-idx':
        opt_pack.wld_idx = int(value)
    elif option == '--prec':
        opt_pack.prec = int(value)
    elif option == '--seed':
        opt_pack.seed = int(value)
    elif option == '--step':
        opt_pack.step = float(value)
    elif option == '--global-tlim':
        opt_pack.global_tlim = int(value)
    elif option == '--global-iter':
        opt_pack.global_iter = int(value)
    elif option == '--local-tlim':
        opt_pack.local_tlim = float(value)
    elif option == '--local-nsol':
        opt_pack.local_nsol = float(value)
    elif option == '--restart-base':
        opt_pack.restart_base = float(value)
        if opt_pack.restart_base <= 0:
            opt_pack.restart_base = None
    elif option == '--reservation-file':
        opt_pack.res_file = value
    elif option == '--reservation-level':
        opt_pack.res_level = float(value)
    elif option == '--use-acpi-model':
        opt_pack.acpi_model = True
    elif option == '--net-prop-level':
        opt_pack.net_prop_level = int(value)
        assert 0 <= opt_pack.net_prop_level <= 3
    elif option == '--search-strategy':
        if value == 'random':
            opt_pack.strategy = search_random
        elif value == 'naive':
            opt_pack.strategy = search_naive
        elif value == 'ad_hoc':
            opt_pack.strategy = search_custom
        else:
            raise Usage('invalid search strategy')
    elif option == '--explore-neighborhood':
        opt_pack.neighborhood = [int(v) for v in value.split(',')]
    elif option == '--restart-strat':
        if value == 'luby':
            opt_pack.restart_strat = restart_luby
        elif value == 'constant':
            opt_pack.restart_strat = restart_constant
        else:
            raise Usage('invalid restart strategy')

# the actual main script
if __name__ == '__main__':
    # check arguments
    if len(sys.argv) < 3:
        print >> sys.stderr, 'USAGE:'
        print >> sys.stderr, sys.argv[0].split("/")[-1] + " {options} <net file> <wld file>"
        print help_message
        sys.exit()
        
    # get network and workload file name
    net_fname = sys.argv[-2]
    wld_fname = sys.argv[-1]
        
    # parse arguments  
    opt_pack = OptPack()
    try:  
        opts, args = getopt.getopt(sys.argv[1:-2], short_opts, long_opts)
        
        # parse options
        for option, value in opts:
            if option in ('h', 'help'):
                print >> sys.stderr, 'USAGE:'
                print >> sys.stderr, sys.argv[0].split("/")[-1] + " {options} <ptf file> <wld file>"
                print help_message
            else:
                apply_option(opt_pack, option, value)
            
    except getopt.error, msg:
        # raise Usage(msg)
        print >> sys.stderr
        print >> sys.stderr, 'ERROR: %s' % msg
        print >> sys.stderr
        print >> sys.stderr, 'USAGE:'
        print >> sys.stderr, sys.argv[0].split("/")[-1] + " {options} <net file> <wld file>"
        print help_message
        sys.exit()
        
    # check the network name and determine the neural network type
    ann_name = net_fname[net_fname.rfind('/')+1:]
    if not opt_pack.acpi_model:
        if ann_name == 'scc_ann1.txt':
            ann_type = 1
        elif ann_name == 'scc_ann2.txt':
            ann_type = 2
        else:
            print >> sys.stderr, 'The specified ANN is not supported'
            sys.exit()
    else:
        if ann_name == 'scc_acpi0.txt':
            ann_type = -1
        else:
            print >> sys.stderr, 'The specified ANN is not supported'
            sys.exit()
        
    # print the command line
    print '=' * 78
    print 'LNS optimizer for EML demonstration'
    print '=' * 78
    print '\nOption values: %s' % opt_pack
    print '\nStarting the Optimization Process'
    
    # get the networks
    if not opt_pack.acpi_model:
        anns = read_networks(net_fname)
    else:
        # otherwise, instead of ANNs, we read weights
        with open(net_fname) as f:
            anns = [int(v) for l in f for v in l.split()]
    
    # get the workloads
    wlds = read_workloads(wld_fname)
    
    # get the target workload
    wld = wlds[opt_pack.wld_idx]
    
    # get the reservations (if specified)
    if opt_pack.res_file is not None:
        reserved_tasks = read_reservations(opt_pack.res_file)[opt_pack.wld_idx]
    else:
        reserved_tasks = None
        
    print 'reserved_tasks', reserved_tasks
    
    # find the first solution
    print 'Finding a first solution'
    if opt_pack.res_file is not None:
        print 'using advanced method to find a first solution'
        fsol, fsol_time = find_first_solution_adv(wld, anns, opt_pack, ann_type)
    else:
        fsol, fsol_time = find_first_solution(wld, anns)
            
    # print 'First solution'
    # for k in range(ptf_h * ptf_w): # DEBUG
    #    l = [i for i, k2 in enumerate(fsol) if k2 == k]
    #    print l, np.mean([wld[i] for i in l])
    
    # seed the RNG
    if opt_pack.seed is not None:
        random.seed(opt_pack.seed)
        np.random.seed(opt_pack.seed)
        
    # build an option pack for the LNS procedure
    lns_opts = improve_via_lns_type(
                       opt_pack.prec,
                       opt_pack.step,
                       opt_pack.global_tlim*1000 - fsol_time,
                       opt_pack.global_iter,
                       opt_pack.local_tlim*1000,
                       opt_pack.local_nsol,
                       opt_pack.restart_base,
                       opt_pack.restart_strat,
                       reserved_tasks,
                       opt_pack.res_level,
                       opt_pack.net_prop_level,
                       opt_pack.strategy,
                       opt_pack.neighborhood,
                       opt_pack.seed)
                       
    # run the LNS procedure
    improve_via_lns(wld, anns, fsol, lns_opts, ann_type)