========================
= Network Descriptions =
========================

*** SCC_ANN1:

- average CPI on the target core
- min CPI on the target core
- average of the average CPI of the neighbor cores (adjacent + corners)
- average of the average CPI of all the other cores

- hidden layer: 2 tansig neurons
- output layer: 1 tansig neuron

*** SCC_ANN2:

- average CPI on the target core
- average of the average CPI of the neighbor cores (adjacent + corners)
- average of the average CPI of all the other cores

- hidden layer: 2 tansig neurons
- output layer: 1 tansig neuron
