#!/usr/bin/env bash

# configuration
options="--global-tlim 300 --local-tlim 300 --search-strategy ad_hoc --global-iter 1 --local-nsol 0 --restart-base 0"

# Library with the desired version of the propagators
# NOTE: this is necessary since many of the propagator parameters are defined
# at compilation time
# "i" = number of initial subgradient iterations
# "s" = number of subgradient iterations after the first activation
# "b" = activation budget
lib_path="pre-compiled-libs/i1000-s5-b18"

# Option string (this is used to tag the log files)
opt_string="i1000_s5_budget18"

# Redefine PYTHONPATH variable to include the new folder
export PYTHONPATH=$lib_path

# core sets (3-tuples randomly picked from range(48))
# NOTE: instances similar to those are referenced as "unbiased" in the CP
# conference paper. For this reason the log files contain the suffix "ub"
# core_sets_100="20,10,4 5,40,22 14,1,7 32,24,15 34,41,39 25,24,0 32,41,45 13,46,11 12,13,25 4,7,3 42,38,1 11,8,5 7,17,32 7,47,26 45,39,6 38,6,41 14,3,19 10,42,17 44,12,17 12,23,40 20,5,25 35,4,41 2,37,3 37,43,23 18,7,37 19,11,5 12,17,25 18,8,30 35,19,27 21,22,25 29,9,37 20,0,42 14,16,5 44,28,15 43,47,22 15,22,26 43,35,23 42,40,25 34,8,43 18,20,31 22,39,37 31,38,3 41,33,0 16,41,9 22,6,40 33,16,19 14,2,15 10,40,44 39,1,24 28,6,16 34,11,29 18,5,16 6,33,30 39,35,21 38,47,42 30,46,20 30,41,11 8,13,21 4,34,32 29,4,13 26,10,11 4,13,5 8,41,9 44,4,32 17,45,26 42,29,45 38,12,32 18,44,46 26,30,34 47,1,42 44,23,32 23,27,47 17,10,40 32,33,6 7,41,3 10,35,25 43,0,40 15,19,6 40,37,32 23,7,20 28,36,17 44,24,12 32,12,47 35,38,12 41,25,16 36,31,41 0,38,13 38,37,45 46,16,34 26,24,33 16,32,13 41,12,47 4,32,44 32,24,20 26,18,34 32,21,30 26,32,28 30,47,16 38,16,5 47,7,9"
core_sets_200="36,9,41 46,25,36 36,19,5 22,29,41 11,35,36 19,25,9 38,28,36 40,35,8 9,43,42 26,33,12 26,30,34 0,7,47 31,43,5 42,2,9 15,2,39 7,9,11 23,11,37 11,38,42 35,12,0 18,7,44 8,44,39 6,36,26 37,33,0 47,13,1 2,42,43 24,37,25 26,3,17 6,20,43 22,41,43 23,38,12 20,26,11 0,9,3 25,6,0 6,14,10 2,37,42 1,18,10 4,25,21 29,17,1 9,13,8 19,18,7 44,47,23 20,34,13 19,38,31 27,0,4 13,10,26 15,42,23 29,41,0 26,27,30 21,10,3 46,42,0 27,11,38 44,18,15 46,12,27 34,2,19 11,25,16 18,27,28 7,27,34 20,19,16 20,18,27 4,22,10 8,2,18 26,17,10 31,45,23 23,6,8 17,33,2 22,47,39 14,34,16 30,41,37 18,13,6 2,7,40 1,21,43 12,14,35 45,24,35 38,2,32 26,5,27 17,28,46 11,22,3 21,7,1 42,44,2 44,17,3 32,28,12 37,10,22 0,2,9 31,35,45 33,35,21 18,8,26 11,33,17 5,42,39 21,0,35 30,5,12 43,12,8 24,33,44 16,38,32 38,5,17 28,37,43 45,3,15 18,24,9 23,26,17 43,46,20 18,31,23 2,44,28 11,8,15 19,38,1 38,35,1 9,47,11 30,23,26 26,34,38 35,20,30 44,1,11 35,30,26 38,10,44 25,30,45 16,38,25 28,46,31 14,41,29 24,28,36 16,27,34 44,21,47 10,28,9 45,24,44 42,41,36 45,34,36 30,26,43 33,25,43 18,44,27 3,29,46 17,4,39 39,0,16 22,39,41 5,47,14 17,20,38 4,28,35 29,26,24 34,39,2 42,11,3 35,6,28 25,26,37 36,17,29 32,18,8 19,23,40 8,30,7 32,8,21 29,27,11 16,4,34 16,28,13 16,10,15 20,0,10 9,25,36 38,7,39 16,11,41 10,8,21 29,18,40 46,28,47 9,34,44 42,36,0 13,47,40 11,33,18 8,46,30 46,0,12 4,37,21 31,6,4 19,13,38 41,21,31 16,21,33 2,46,44 47,29,15 40,3,24 33,42,29 2,10,29 24,35,9 45,42,15 3,25,15 12,25,16 11,2,7 9,18,25 43,36,32 36,42,2 38,1,25 1,13,42 30,18,45 13,30,17 38,16,25 43,26,34 30,19,43 12,21,39 13,30,4 12,26,22 42,37,31 33,44,20 28,47,21 35,28,23 15,23,1 41,25,29 43,18,12 32,38,44 35,3,32 37,4,30 6,29,19 20,8,43 29,22,19"

cnt=0
for core_set in ${core_sets_200}; do

	# This piece of code allows to skip solving several instances. This allows
	# to interrupt the solution of a large batch of instances and then re-start
	# it without havinf to re-solve everything
	last_solved_cnt=-1
	if [ $cnt -le $last_solved_cnt ]; then
		let cnt=$cnt+1
		continue
	fi

	for ann in ann1 ann2 ; do
		netfile="networks/scc_${ann}.txt"
		log_fstem="lag_logs/log_${ann}"

		echo "ANN: ${ann}, CORES: ${core_set} (# $cnt), TIME: `date` ======"

		# baseline approach
		# "l1" = propagate individual neuron constraints
		log_fname="${log_fstem}_n${core_set}_l1_ub200.txt"
		echo ${log_fname}
		./dispatch_cp.py $options --explore-neighborhood ${core_set} --net-prop-level 1 $netfile workloads/wld_warm_div20.txt > ${log_fname}

		# lagrangian approaches
		for lvl in 2 3; do #for lvl in `seq 2 3`; do
			# "n" = the subset of cores for this instance
			# "l" = propagation level (2 = output only, 3 = input and output)
			# "ub200" = just the suffix for this benchmark
			log_fname="${log_fstem}_n${core_set}_l${lvl}_${opt_string}_ub200.txt"

			echo ${log_fname}
			python dispatch_cp.py $options --explore-neighborhood ${core_set} --net-prop-level $lvl $netfile workloads/wld_warm_div20.txt > ${log_fname}
		done
	done

	let cnt=$cnt+1
done