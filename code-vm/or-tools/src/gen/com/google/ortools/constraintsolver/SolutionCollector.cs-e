/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace Google.OrTools.ConstraintSolver {

using System;
using System.Runtime.InteropServices;
using System.Collections;

public partial class SolutionCollector : SearchMonitor {
  private HandleRef swigCPtr;

  internal SolutionCollector(IntPtr cPtr, bool cMemoryOwn) : base(operations_research_constraint_solverPINVOKE.SolutionCollector_SWIGUpcast(cPtr), cMemoryOwn) {
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(SolutionCollector obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~SolutionCollector() {
    Dispose();
  }

  public override void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          operations_research_constraint_solverPINVOKE.delete_SolutionCollector(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
      base.Dispose();
    }
  }

  public SolutionCollector(Solver s, Assignment assignment) : this(operations_research_constraint_solverPINVOKE.new_SolutionCollector__SWIG_0(Solver.getCPtr(s), Assignment.getCPtr(assignment)), true) {
  }

  public SolutionCollector(Solver s) : this(operations_research_constraint_solverPINVOKE.new_SolutionCollector__SWIG_1(Solver.getCPtr(s)), true) {
  }

  public void Add(IntVar var) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_0(swigCPtr, IntVar.getCPtr(var));
  }

  public void Add(IntVarVector vars) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_1(swigCPtr, IntVarVector.getCPtr(vars));
    if (operations_research_constraint_solverPINVOKE.SWIGPendingException.Pending) throw operations_research_constraint_solverPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Add(IntervalVar var) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_2(swigCPtr, IntervalVar.getCPtr(var));
  }

  public void Add(IntervalVarVector vars) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_3(swigCPtr, IntervalVarVector.getCPtr(vars));
    if (operations_research_constraint_solverPINVOKE.SWIGPendingException.Pending) throw operations_research_constraint_solverPINVOKE.SWIGPendingException.Retrieve();
  }

  public void Add(SequenceVar var) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_4(swigCPtr, SequenceVar.getCPtr(var));
  }

  public void Add(SequenceVarVector vars) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_Add__SWIG_5(swigCPtr, SequenceVarVector.getCPtr(vars));
    if (operations_research_constraint_solverPINVOKE.SWIGPendingException.Pending) throw operations_research_constraint_solverPINVOKE.SWIGPendingException.Retrieve();
  }

  public void AddObjective(IntVar objective) {
    operations_research_constraint_solverPINVOKE.SolutionCollector_AddObjective(swigCPtr, IntVar.getCPtr(objective));
  }

  public override void EnterSearch() {
    operations_research_constraint_solverPINVOKE.SolutionCollector_EnterSearch(swigCPtr);
  }

  public int SolutionCount() {
    int ret = operations_research_constraint_solverPINVOKE.SolutionCollector_SolutionCount(swigCPtr);
    return ret;
  }

  public Assignment Solution(int n) {
    IntPtr cPtr = operations_research_constraint_solverPINVOKE.SolutionCollector_Solution(swigCPtr, n);
    Assignment ret = (cPtr == IntPtr.Zero) ? null : new Assignment(cPtr, false);
    return ret;
  }

  public long WallTime(int n) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_WallTime(swigCPtr, n);
    return ret;
  }

  public long Branches(int n) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_Branches(swigCPtr, n);
    return ret;
  }

  public long Failures(int n) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_Failures(swigCPtr, n);
    return ret;
  }

  public long ObjectiveValue(int n) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_ObjectiveValue(swigCPtr, n);
    return ret;
  }

  public long Value(int n, IntVar var) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_Value(swigCPtr, n, IntVar.getCPtr(var));
    return ret;
  }

  public long StartValue(int n, IntervalVar var) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_StartValue(swigCPtr, n, IntervalVar.getCPtr(var));
    return ret;
  }

  public long EndValue(int n, IntervalVar var) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_EndValue(swigCPtr, n, IntervalVar.getCPtr(var));
    return ret;
  }

  public long DurationValue(int n, IntervalVar var) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_DurationValue(swigCPtr, n, IntervalVar.getCPtr(var));
    return ret;
  }

  public long PerformedValue(int n, IntervalVar var) {
    long ret = operations_research_constraint_solverPINVOKE.SolutionCollector_PerformedValue(swigCPtr, n, IntervalVar.getCPtr(var));
    return ret;
  }

  public CpIntVector ForwardSequence(int n, SequenceVar var) {
    CpIntVector ret = new CpIntVector(operations_research_constraint_solverPINVOKE.SolutionCollector_ForwardSequence(swigCPtr, n, SequenceVar.getCPtr(var)), false);
    return ret;
  }

  public CpIntVector BackwardSequence(int n, SequenceVar var) {
    CpIntVector ret = new CpIntVector(operations_research_constraint_solverPINVOKE.SolutionCollector_BackwardSequence(swigCPtr, n, SequenceVar.getCPtr(var)), false);
    return ret;
  }

  public CpIntVector Unperformed(int n, SequenceVar var) {
    CpIntVector ret = new CpIntVector(operations_research_constraint_solverPINVOKE.SolutionCollector_Unperformed(swigCPtr, n, SequenceVar.getCPtr(var)), false);
    return ret;
  }

}

}
