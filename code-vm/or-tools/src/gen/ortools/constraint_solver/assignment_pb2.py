# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ortools/constraint_solver/assignment.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)




DESCRIPTOR = _descriptor.FileDescriptor(
  name='ortools/constraint_solver/assignment.proto',
  package='operations_research',
  serialized_pb='\n*ortools/constraint_solver/assignment.proto\x12\x13operations_research\"W\n\x15IntVarAssignmentProto\x12\x0e\n\x06var_id\x18\x01 \x01(\t\x12\x0b\n\x03min\x18\x02 \x01(\x03\x12\x0b\n\x03max\x18\x03 \x01(\x03\x12\x14\n\x06\x61\x63tive\x18\x04 \x01(\x08:\x04true\"\xe4\x01\n\x1aIntervalVarAssignmentProto\x12\x0e\n\x06var_id\x18\x01 \x01(\t\x12\x11\n\tstart_min\x18\x02 \x01(\x03\x12\x11\n\tstart_max\x18\x03 \x01(\x03\x12\x14\n\x0c\x64uration_min\x18\x04 \x01(\x03\x12\x14\n\x0c\x64uration_max\x18\x05 \x01(\x03\x12\x0f\n\x07\x65nd_min\x18\x06 \x01(\x03\x12\x0f\n\x07\x65nd_max\x18\x07 \x01(\x03\x12\x15\n\rperformed_min\x18\x08 \x01(\x03\x12\x15\n\rperformed_max\x18\t \x01(\x03\x12\x14\n\x06\x61\x63tive\x18\n \x01(\x08:\x04true\"\x8c\x01\n\x1aSequenceVarAssignmentProto\x12\x0e\n\x06var_id\x18\x01 \x01(\t\x12\x18\n\x10\x66orward_sequence\x18\x02 \x03(\x05\x12\x19\n\x11\x62\x61\x63kward_sequence\x18\x03 \x03(\x05\x12\x13\n\x0bunperformed\x18\x04 \x03(\x05\x12\x14\n\x06\x61\x63tive\x18\x05 \x01(\x08:\x04true\",\n\nWorkerInfo\x12\x11\n\tworker_id\x18\x01 \x01(\x05\x12\x0b\n\x03\x62ns\x18\x02 \x01(\t\"\x8a\x03\n\x0f\x41ssignmentProto\x12\x46\n\x12int_var_assignment\x18\x01 \x03(\x0b\x32*.operations_research.IntVarAssignmentProto\x12P\n\x17interval_var_assignment\x18\x02 \x03(\x0b\x32/.operations_research.IntervalVarAssignmentProto\x12P\n\x17sequence_var_assignment\x18\x06 \x03(\x0b\x32/.operations_research.SequenceVarAssignmentProto\x12=\n\tobjective\x18\x03 \x01(\x0b\x32*.operations_research.IntVarAssignmentProto\x12\x34\n\x0bworker_info\x18\x04 \x01(\x0b\x32\x1f.operations_research.WorkerInfo\x12\x16\n\x08is_valid\x18\x05 \x01(\x08:\x04true')




_INTVARASSIGNMENTPROTO = _descriptor.Descriptor(
  name='IntVarAssignmentProto',
  full_name='operations_research.IntVarAssignmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='var_id', full_name='operations_research.IntVarAssignmentProto.var_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='min', full_name='operations_research.IntVarAssignmentProto.min', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='max', full_name='operations_research.IntVarAssignmentProto.max', index=2,
      number=3, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='active', full_name='operations_research.IntVarAssignmentProto.active', index=3,
      number=4, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=67,
  serialized_end=154,
)


_INTERVALVARASSIGNMENTPROTO = _descriptor.Descriptor(
  name='IntervalVarAssignmentProto',
  full_name='operations_research.IntervalVarAssignmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='var_id', full_name='operations_research.IntervalVarAssignmentProto.var_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='start_min', full_name='operations_research.IntervalVarAssignmentProto.start_min', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='start_max', full_name='operations_research.IntervalVarAssignmentProto.start_max', index=2,
      number=3, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='duration_min', full_name='operations_research.IntervalVarAssignmentProto.duration_min', index=3,
      number=4, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='duration_max', full_name='operations_research.IntervalVarAssignmentProto.duration_max', index=4,
      number=5, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='end_min', full_name='operations_research.IntervalVarAssignmentProto.end_min', index=5,
      number=6, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='end_max', full_name='operations_research.IntervalVarAssignmentProto.end_max', index=6,
      number=7, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='performed_min', full_name='operations_research.IntervalVarAssignmentProto.performed_min', index=7,
      number=8, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='performed_max', full_name='operations_research.IntervalVarAssignmentProto.performed_max', index=8,
      number=9, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='active', full_name='operations_research.IntervalVarAssignmentProto.active', index=9,
      number=10, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=157,
  serialized_end=385,
)


_SEQUENCEVARASSIGNMENTPROTO = _descriptor.Descriptor(
  name='SequenceVarAssignmentProto',
  full_name='operations_research.SequenceVarAssignmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='var_id', full_name='operations_research.SequenceVarAssignmentProto.var_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='forward_sequence', full_name='operations_research.SequenceVarAssignmentProto.forward_sequence', index=1,
      number=2, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='backward_sequence', full_name='operations_research.SequenceVarAssignmentProto.backward_sequence', index=2,
      number=3, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='unperformed', full_name='operations_research.SequenceVarAssignmentProto.unperformed', index=3,
      number=4, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='active', full_name='operations_research.SequenceVarAssignmentProto.active', index=4,
      number=5, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=388,
  serialized_end=528,
)


_WORKERINFO = _descriptor.Descriptor(
  name='WorkerInfo',
  full_name='operations_research.WorkerInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='worker_id', full_name='operations_research.WorkerInfo.worker_id', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='bns', full_name='operations_research.WorkerInfo.bns', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=530,
  serialized_end=574,
)


_ASSIGNMENTPROTO = _descriptor.Descriptor(
  name='AssignmentProto',
  full_name='operations_research.AssignmentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='int_var_assignment', full_name='operations_research.AssignmentProto.int_var_assignment', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='interval_var_assignment', full_name='operations_research.AssignmentProto.interval_var_assignment', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sequence_var_assignment', full_name='operations_research.AssignmentProto.sequence_var_assignment', index=2,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='objective', full_name='operations_research.AssignmentProto.objective', index=3,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='worker_info', full_name='operations_research.AssignmentProto.worker_info', index=4,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='is_valid', full_name='operations_research.AssignmentProto.is_valid', index=5,
      number=5, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=577,
  serialized_end=971,
)

_ASSIGNMENTPROTO.fields_by_name['int_var_assignment'].message_type = _INTVARASSIGNMENTPROTO
_ASSIGNMENTPROTO.fields_by_name['interval_var_assignment'].message_type = _INTERVALVARASSIGNMENTPROTO
_ASSIGNMENTPROTO.fields_by_name['sequence_var_assignment'].message_type = _SEQUENCEVARASSIGNMENTPROTO
_ASSIGNMENTPROTO.fields_by_name['objective'].message_type = _INTVARASSIGNMENTPROTO
_ASSIGNMENTPROTO.fields_by_name['worker_info'].message_type = _WORKERINFO
DESCRIPTOR.message_types_by_name['IntVarAssignmentProto'] = _INTVARASSIGNMENTPROTO
DESCRIPTOR.message_types_by_name['IntervalVarAssignmentProto'] = _INTERVALVARASSIGNMENTPROTO
DESCRIPTOR.message_types_by_name['SequenceVarAssignmentProto'] = _SEQUENCEVARASSIGNMENTPROTO
DESCRIPTOR.message_types_by_name['WorkerInfo'] = _WORKERINFO
DESCRIPTOR.message_types_by_name['AssignmentProto'] = _ASSIGNMENTPROTO

class IntVarAssignmentProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _INTVARASSIGNMENTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.IntVarAssignmentProto)

class IntervalVarAssignmentProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _INTERVALVARASSIGNMENTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.IntervalVarAssignmentProto)

class SequenceVarAssignmentProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _SEQUENCEVARASSIGNMENTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.SequenceVarAssignmentProto)

class WorkerInfo(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _WORKERINFO

  # @@protoc_insertion_point(class_scope:operations_research.WorkerInfo)

class AssignmentProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _ASSIGNMENTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.AssignmentProto)


# @@protoc_insertion_point(module_scope)
