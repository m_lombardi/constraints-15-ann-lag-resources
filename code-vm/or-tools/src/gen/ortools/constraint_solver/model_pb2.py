# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: ortools/constraint_solver/model.proto

from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)


import ortools.constraint_solver.search_limit_pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='ortools/constraint_solver/model.proto',
  package='operations_research',
  serialized_pb='\n%ortools/constraint_solver/model.proto\x12\x13operations_research\x1a,ortools/constraint_solver/search_limit.proto\"E\n\x14\x43PIntegerMatrixProto\x12\x0c\n\x04rows\x18\x01 \x01(\x05\x12\x0f\n\x07\x63olumns\x18\x02 \x01(\x05\x12\x0e\n\x06values\x18\x03 \x03(\x03\"\xbe\x02\n\x0f\x43PArgumentProto\x12\x16\n\x0e\x61rgument_index\x18\x01 \x01(\x05\x12\x15\n\rinteger_value\x18\x02 \x01(\x03\x12\x15\n\rinteger_array\x18\x03 \x03(\x03\x12 \n\x18integer_expression_index\x18\x04 \x01(\x05\x12 \n\x18integer_expression_array\x18\x05 \x03(\x05\x12\x16\n\x0einterval_index\x18\x06 \x01(\x05\x12\x16\n\x0einterval_array\x18\x07 \x03(\x05\x12\x16\n\x0esequence_index\x18\x08 \x01(\x05\x12\x16\n\x0esequence_array\x18\t \x03(\x05\x12\x41\n\x0einteger_matrix\x18\n \x01(\x0b\x32).operations_research.CPIntegerMatrixProto\"_\n\x10\x43PExtensionProto\x12\x12\n\ntype_index\x18\x01 \x01(\x05\x12\x37\n\targuments\x18\x02 \x03(\x0b\x32$.operations_research.CPArgumentProto\"\xbf\x01\n\x18\x43PIntegerExpressionProto\x12\r\n\x05index\x18\x01 \x01(\x05\x12\x12\n\ntype_index\x18\x02 \x01(\x05\x12\x0c\n\x04name\x18\x03 \x01(\t\x12\x37\n\targuments\x18\x04 \x03(\x0b\x32$.operations_research.CPArgumentProto\x12\x39\n\nextensions\x18\x05 \x03(\x0b\x32%.operations_research.CPExtensionProto\"\x83\x01\n\x17\x43PIntervalVariableProto\x12\r\n\x05index\x18\x01 \x01(\x05\x12\x12\n\ntype_index\x18\x02 \x01(\x05\x12\x0c\n\x04name\x18\x03 \x01(\t\x12\x37\n\targuments\x18\x04 \x03(\x0b\x32$.operations_research.CPArgumentProto\"\x83\x01\n\x17\x43PSequenceVariableProto\x12\r\n\x05index\x18\x01 \x01(\x05\x12\x12\n\ntype_index\x18\x02 \x01(\x05\x12\x0c\n\x04name\x18\x03 \x01(\t\x12\x37\n\targuments\x18\x04 \x03(\x0b\x32$.operations_research.CPArgumentProto\"\xb8\x01\n\x11\x43PConstraintProto\x12\r\n\x05index\x18\x01 \x01(\x05\x12\x12\n\ntype_index\x18\x02 \x01(\x05\x12\x0c\n\x04name\x18\x03 \x01(\t\x12\x37\n\targuments\x18\x04 \x03(\x0b\x32$.operations_research.CPArgumentProto\x12\x39\n\nextensions\x18\x05 \x03(\x0b\x32%.operations_research.CPExtensionProto\"K\n\x10\x43PObjectiveProto\x12\x10\n\x08maximize\x18\x01 \x01(\x08\x12\x0c\n\x04step\x18\x02 \x01(\x03\x12\x17\n\x0fobjective_index\x18\x03 \x01(\x05\"X\n\x0f\x43PVariableGroup\x12\x37\n\targuments\x18\x01 \x03(\x0b\x32$.operations_research.CPArgumentProto\x12\x0c\n\x04type\x18\x02 \x01(\t\"\x8b\x04\n\x0c\x43PModelProto\x12\r\n\x05model\x18\x01 \x01(\t\x12\x0f\n\x07version\x18\x02 \x01(\x05\x12\x0c\n\x04tags\x18\x03 \x03(\t\x12\x42\n\x0b\x65xpressions\x18\x04 \x03(\x0b\x32-.operations_research.CPIntegerExpressionProto\x12?\n\tintervals\x18\x05 \x03(\x0b\x32,.operations_research.CPIntervalVariableProto\x12?\n\tsequences\x18\x06 \x03(\x0b\x32,.operations_research.CPSequenceVariableProto\x12;\n\x0b\x63onstraints\x18\x07 \x03(\x0b\x32&.operations_research.CPConstraintProto\x12\x38\n\tobjective\x18\x08 \x01(\x0b\x32%.operations_research.CPObjectiveProto\x12;\n\x0csearch_limit\x18\t \x01(\x0b\x32%.operations_research.SearchLimitProto\x12=\n\x0fvariable_groups\x18\n \x03(\x0b\x32$.operations_research.CPVariableGroup\x12\x14\n\x0clicense_text\x18\x0b \x01(\t')




_CPINTEGERMATRIXPROTO = _descriptor.Descriptor(
  name='CPIntegerMatrixProto',
  full_name='operations_research.CPIntegerMatrixProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='rows', full_name='operations_research.CPIntegerMatrixProto.rows', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='columns', full_name='operations_research.CPIntegerMatrixProto.columns', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='values', full_name='operations_research.CPIntegerMatrixProto.values', index=2,
      number=3, type=3, cpp_type=2, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=108,
  serialized_end=177,
)


_CPARGUMENTPROTO = _descriptor.Descriptor(
  name='CPArgumentProto',
  full_name='operations_research.CPArgumentProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='argument_index', full_name='operations_research.CPArgumentProto.argument_index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='integer_value', full_name='operations_research.CPArgumentProto.integer_value', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='integer_array', full_name='operations_research.CPArgumentProto.integer_array', index=2,
      number=3, type=3, cpp_type=2, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='integer_expression_index', full_name='operations_research.CPArgumentProto.integer_expression_index', index=3,
      number=4, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='integer_expression_array', full_name='operations_research.CPArgumentProto.integer_expression_array', index=4,
      number=5, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='interval_index', full_name='operations_research.CPArgumentProto.interval_index', index=5,
      number=6, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='interval_array', full_name='operations_research.CPArgumentProto.interval_array', index=6,
      number=7, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sequence_index', full_name='operations_research.CPArgumentProto.sequence_index', index=7,
      number=8, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sequence_array', full_name='operations_research.CPArgumentProto.sequence_array', index=8,
      number=9, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='integer_matrix', full_name='operations_research.CPArgumentProto.integer_matrix', index=9,
      number=10, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=180,
  serialized_end=498,
)


_CPEXTENSIONPROTO = _descriptor.Descriptor(
  name='CPExtensionProto',
  full_name='operations_research.CPExtensionProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='type_index', full_name='operations_research.CPExtensionProto.type_index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPExtensionProto.arguments', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=500,
  serialized_end=595,
)


_CPINTEGEREXPRESSIONPROTO = _descriptor.Descriptor(
  name='CPIntegerExpressionProto',
  full_name='operations_research.CPIntegerExpressionProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='operations_research.CPIntegerExpressionProto.index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type_index', full_name='operations_research.CPIntegerExpressionProto.type_index', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='operations_research.CPIntegerExpressionProto.name', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPIntegerExpressionProto.arguments', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='extensions', full_name='operations_research.CPIntegerExpressionProto.extensions', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=598,
  serialized_end=789,
)


_CPINTERVALVARIABLEPROTO = _descriptor.Descriptor(
  name='CPIntervalVariableProto',
  full_name='operations_research.CPIntervalVariableProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='operations_research.CPIntervalVariableProto.index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type_index', full_name='operations_research.CPIntervalVariableProto.type_index', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='operations_research.CPIntervalVariableProto.name', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPIntervalVariableProto.arguments', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=792,
  serialized_end=923,
)


_CPSEQUENCEVARIABLEPROTO = _descriptor.Descriptor(
  name='CPSequenceVariableProto',
  full_name='operations_research.CPSequenceVariableProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='operations_research.CPSequenceVariableProto.index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type_index', full_name='operations_research.CPSequenceVariableProto.type_index', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='operations_research.CPSequenceVariableProto.name', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPSequenceVariableProto.arguments', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=926,
  serialized_end=1057,
)


_CPCONSTRAINTPROTO = _descriptor.Descriptor(
  name='CPConstraintProto',
  full_name='operations_research.CPConstraintProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='index', full_name='operations_research.CPConstraintProto.index', index=0,
      number=1, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type_index', full_name='operations_research.CPConstraintProto.type_index', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='operations_research.CPConstraintProto.name', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPConstraintProto.arguments', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='extensions', full_name='operations_research.CPConstraintProto.extensions', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=1060,
  serialized_end=1244,
)


_CPOBJECTIVEPROTO = _descriptor.Descriptor(
  name='CPObjectiveProto',
  full_name='operations_research.CPObjectiveProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='maximize', full_name='operations_research.CPObjectiveProto.maximize', index=0,
      number=1, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='step', full_name='operations_research.CPObjectiveProto.step', index=1,
      number=2, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='objective_index', full_name='operations_research.CPObjectiveProto.objective_index', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=1246,
  serialized_end=1321,
)


_CPVARIABLEGROUP = _descriptor.Descriptor(
  name='CPVariableGroup',
  full_name='operations_research.CPVariableGroup',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='arguments', full_name='operations_research.CPVariableGroup.arguments', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type', full_name='operations_research.CPVariableGroup.type', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=1323,
  serialized_end=1411,
)


_CPMODELPROTO = _descriptor.Descriptor(
  name='CPModelProto',
  full_name='operations_research.CPModelProto',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='model', full_name='operations_research.CPModelProto.model', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='version', full_name='operations_research.CPModelProto.version', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='tags', full_name='operations_research.CPModelProto.tags', index=2,
      number=3, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='expressions', full_name='operations_research.CPModelProto.expressions', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='intervals', full_name='operations_research.CPModelProto.intervals', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='sequences', full_name='operations_research.CPModelProto.sequences', index=5,
      number=6, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='constraints', full_name='operations_research.CPModelProto.constraints', index=6,
      number=7, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='objective', full_name='operations_research.CPModelProto.objective', index=7,
      number=8, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='search_limit', full_name='operations_research.CPModelProto.search_limit', index=8,
      number=9, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='variable_groups', full_name='operations_research.CPModelProto.variable_groups', index=9,
      number=10, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='license_text', full_name='operations_research.CPModelProto.license_text', index=10,
      number=11, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=unicode("", "utf-8"),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  serialized_start=1414,
  serialized_end=1937,
)

_CPARGUMENTPROTO.fields_by_name['integer_matrix'].message_type = _CPINTEGERMATRIXPROTO
_CPEXTENSIONPROTO.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPINTEGEREXPRESSIONPROTO.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPINTEGEREXPRESSIONPROTO.fields_by_name['extensions'].message_type = _CPEXTENSIONPROTO
_CPINTERVALVARIABLEPROTO.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPSEQUENCEVARIABLEPROTO.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPCONSTRAINTPROTO.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPCONSTRAINTPROTO.fields_by_name['extensions'].message_type = _CPEXTENSIONPROTO
_CPVARIABLEGROUP.fields_by_name['arguments'].message_type = _CPARGUMENTPROTO
_CPMODELPROTO.fields_by_name['expressions'].message_type = _CPINTEGEREXPRESSIONPROTO
_CPMODELPROTO.fields_by_name['intervals'].message_type = _CPINTERVALVARIABLEPROTO
_CPMODELPROTO.fields_by_name['sequences'].message_type = _CPSEQUENCEVARIABLEPROTO
_CPMODELPROTO.fields_by_name['constraints'].message_type = _CPCONSTRAINTPROTO
_CPMODELPROTO.fields_by_name['objective'].message_type = _CPOBJECTIVEPROTO
_CPMODELPROTO.fields_by_name['search_limit'].message_type = ortools.constraint_solver.search_limit_pb2._SEARCHLIMITPROTO
_CPMODELPROTO.fields_by_name['variable_groups'].message_type = _CPVARIABLEGROUP
DESCRIPTOR.message_types_by_name['CPIntegerMatrixProto'] = _CPINTEGERMATRIXPROTO
DESCRIPTOR.message_types_by_name['CPArgumentProto'] = _CPARGUMENTPROTO
DESCRIPTOR.message_types_by_name['CPExtensionProto'] = _CPEXTENSIONPROTO
DESCRIPTOR.message_types_by_name['CPIntegerExpressionProto'] = _CPINTEGEREXPRESSIONPROTO
DESCRIPTOR.message_types_by_name['CPIntervalVariableProto'] = _CPINTERVALVARIABLEPROTO
DESCRIPTOR.message_types_by_name['CPSequenceVariableProto'] = _CPSEQUENCEVARIABLEPROTO
DESCRIPTOR.message_types_by_name['CPConstraintProto'] = _CPCONSTRAINTPROTO
DESCRIPTOR.message_types_by_name['CPObjectiveProto'] = _CPOBJECTIVEPROTO
DESCRIPTOR.message_types_by_name['CPVariableGroup'] = _CPVARIABLEGROUP
DESCRIPTOR.message_types_by_name['CPModelProto'] = _CPMODELPROTO

class CPIntegerMatrixProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPINTEGERMATRIXPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPIntegerMatrixProto)

class CPArgumentProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPARGUMENTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPArgumentProto)

class CPExtensionProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPEXTENSIONPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPExtensionProto)

class CPIntegerExpressionProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPINTEGEREXPRESSIONPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPIntegerExpressionProto)

class CPIntervalVariableProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPINTERVALVARIABLEPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPIntervalVariableProto)

class CPSequenceVariableProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPSEQUENCEVARIABLEPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPSequenceVariableProto)

class CPConstraintProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPCONSTRAINTPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPConstraintProto)

class CPObjectiveProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPOBJECTIVEPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPObjectiveProto)

class CPVariableGroup(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPVARIABLEGROUP

  # @@protoc_insertion_point(class_scope:operations_research.CPVariableGroup)

class CPModelProto(_message.Message):
  __metaclass__ = _reflection.GeneratedProtocolMessageType
  DESCRIPTOR = _CPMODELPROTO

  # @@protoc_insertion_point(class_scope:operations_research.CPModelProto)


# @@protoc_insertion_point(module_scope)
