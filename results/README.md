# Results

This folder contains the results of all the experiments we presented in [1], plus a few more that are not reported in the paper.

The results come as raw log files from our solvers, that can be found in the compressed archive "lag_logs.zip".

Each file name is is in the format:

```
log_<ann>_n<cores>_l<propagation level>[_i<initial subg. iter>_s<subg. iter. during search>_<budget kword>][_<benchmark kword>]
```

where:

* "ann" is the considered set of ANNs, i.e. either ANN1 or ANN2.
* "cores" is a list of integer representing the indices of the subet of cores that should be re-optimized
* The possible values for "propagation level" are:
    - 1: for the baseline Neuron Constraint propagation
    - 2: baseline + Lagrangian bounds on the network output
    - 3: baseline + Lagrangian bounds on the network output and input
* "initial subg. iter." is the number of initial subgradient iterations
* "subg. iter. during search" is the number of subgradient iterations during search.
* "budget kword" can be:
    - "always", in case no activation budget is employed
    - "budgetXX", for an activation budget with value XX
* "benchmark kword" can be:
    - nothing, for the original benchmark from [2], which was not discussed in [1]
    - "ub46", for the first (i.e. small) benchmark from [1]
    - "ub200", for the second (i.e. large) benchmark from [1]

Each (reduced-size) instance has been solved to completeness. Technically, this has been done by running a single iteration of a more complex Large Neighborhood Search method.

The important information in each file can be found under the line:

```
LNS optimization is over========================================
```

In detail, we report the solution time (in millisecond), the number branches, and the number of fails.

For the interested reader, immediately above the `LNS optimization is over` line, we report in a more complicated format some interesting information. In particular:

- Under the label `cnt_l_act`, we report the number of activations of each of the three Lagrangian propagators associated to the three cores considered in the instance
- Under the label `cnt_l_filter`, we report the number of times each of the Lagrangian propagators managed to prune the network _output_
- Under the label `cnt_lx_filter`, we report the number of times each of the Lagrangian propagators managed to prune the network _output_

The values of `cnt_l_filter` and `cnt_lx_filter` are meaningful only if the corresponding propagation technique was actually employed when solving the instance.

## References

[1] Lombardi, Michele, and Stefano Gualandi. "A lagrangian propagator for artificial neural networks in constraint programming." Constraints (2015): 1-28.

[2] Stefano Gualandi, Federico Malucelli: "Resource Constrained Shortest Paths with a Super Additive Objective Function". CP 2012: 299-315