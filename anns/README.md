# Artificial Neural Networks

This folder contains the Artificial Neural Networks employed in [1]. In particular:

* The ANN1 model corresponds to the file "scc_ann1.txt"
* The ANN2 model corresponds to the file "scc_ann2.txt"

## File Format

Each line in the files describes the ANN used to predict the efficiency of a core. The first line is for core 0, the second line for core 1 and so on.

Each line is formatted as follows:

```
<n. inputs>,<n. hidden neurons>,<n. output neurons><hidden layer weights><output layer weights>
```

where:

```
<hidden layer weights> ::= {{,<weight of input i for hidden neuron j>},<bias of hidden neuron j>},
<output layer weights> ::= {{,<weight of hidden neurong i for output neuron j>},<bias of output neuron j>},
```

In other words, the lines start with the size of each network layer, then report the weights for the hidden and the output layer. Weights are given neuron-by-neuron, and the bias for each neuron is reported after its weights.

The components of the input of the ANN1 networks for core $k$ are, in order:

* Average CPI of jobs mapped on core $k$
* Minimum CPI of the jobs mapped on core $k$
* Average of the average CPI of the neighbor cores of $k$
* Average of the average CPI of all the other cores on the platform

The ANN2 networks use the same order, but they lack the minimum CPI input.

## References

[1] Lombardi, Michele, and Stefano Gualandi. "A lagrangian propagator for artificial neural networks in constraint programming." Constraints (2015): 1-28.